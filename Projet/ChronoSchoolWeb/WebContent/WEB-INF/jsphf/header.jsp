<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="header" class="clear">
	<div class="fl_left">
		<h1><a href="index.html">ChronoSchool</a></h1>
		<p>Planning En Ligne</p> 
	</div>
	<c:if test="${sessionScope.user==null }">
		<form action="index.html" method="post" id="login">
			<p class="erreur">${message}</p>
			<input type="text" id="email" name="email"/>
			<input type="password" id="mdp" name="mdp"/>
			<input class="buton" type="submit" value="Connexion"/><br/>
			<div id="forgot">Besoin d'<a href="mailto:chronoschoolplatform@gmail.com">aide</a> ? - <a href="mdpOublie.html">Mot de passe oubli�</a> ?</div>
		</form>
	</c:if>
	<c:if test="${sessionScope.user!=null }">
	<span class="fl_right" id="login">
		<form action="monCompte.html" method="get" id="compte">	
			<input type="submit" id="compte" value="Mon Compte" />
		</form>				
		<form action="deconnexion.html" method="post" id="deconnexion">	
			<input type="submit" id="deconnexion" value="Deconnexion" />
		</form>	
	</span>
	</c:if>
</div>

	<div id="menumain">
		<div id="menutop">
			<ul>
				<c:choose>
					<c:when test="${sessionScope.user==null}">
						<li><a href="index.html">Home</a></li>
						<li class="last"><a href="inscription.html">Inscription</a></li>
					</c:when>
					<c:when test="${sessionScope.user!=null and sessionScope.user.status eq 'A'.charAt(0)}">
						<li><a href="afficherHoraire.html">Mon Horaire</a></li>
						<li><a href="#">Gestion</a>
							<ul>
								<li><a href="listeHoraires">Horaires</a></li>
								<li><a href="listeUtilisateurs.html">Utilisateurs</a></li>
								<li><a href="listeSection.html">Ann�es</a></li>
								<li><a href="listeCours.html">Cours</a></li>
								<li><a href="listeLocaux.html">Locaux</a></li>
								<li><a href="listeEquipement.html">Equipements</a></li>
							</ul>
						</li>
						<li class="last"><a href="etatMails.html">Envoi des mails</a></li>
					</c:when>
					<c:when test="${sessionScope.user!=null and sessionScope.user.status eq 'P'.charAt(0)}">
						<li><a href="afficherHoraire.html">Mon Horaire</a></li>
						<li class="last"><a href="listeCours.html">Liste cours</a></li>
					</c:when>
					<c:when test="${sessionScope.user!=null and sessionScope.user.status eq 'U'.charAt(0)}">
						<li><a href="afficherHoraire.html">Mon Horaire</a></li>
						<li class="last"><a href="listeCours.html">Mes cours</a></li>
					</c:when>
				</c:choose>
			</ul>
			<div  class="clear"></div>
		</div>
	</div>