<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Horaire - Liste</title>   

<link rel="stylesheet" type="text/css" href="css/style.css" />  
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="js/listeHoraires.js"></script>

	 

</head>
<body>

	<div id="contentmain"> 
		<div id="content" class="clear">
				<h2>Liste des horaires</h2>
				<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:url var="ajouter" value="infosHoraire.html"></c:url>
				<form action="${ajouter}" method="get" id="ajout">	
					<input type="submit" id="Ajouter" value="Ajouter un horaire" />
				</form>
				<br/>
				<c:choose>
					<c:when test="${liste==null || liste.size()==0}">
						Il n'y a aucun horaire
					</c:when>
					<c:otherwise>
						
						<table id="listeHoraires">
							<thead>
								<tr>
									<th>Année</th>
									<th>Date de début</th>
									<th>Date de fin</th>
									<th width="20px"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${liste}" var="horaire">
								<tr>
									<td>${horaire.annee.numClasse } - ${horaire.annee.section }</td>
									<td><fmt:formatDate pattern="dd-MM-yyyy" value="${horaire.dateDebut.time}"/></td>
									<td><fmt:formatDate pattern="dd-MM-yyyy" value="${horaire.dateFin.time}"/></td>
									<td width="20px">
										<c:url var="modifier" value="infosHoraire.html"></c:url>
										<form action="${modifier}" method="get"> 
											<input type="hidden" name="idHoraire" value="${horaire.idHoraire}"/>
											<input type="submit" class="ui-icon ui-icon-pencil" /> 
										</form>
										<c:url var="supprimer" value="listeHoraires.html"></c:url>
										<form action="${supprimer}" method="post"> 
											<input type="hidden" name="supprimer" value="${horaire.idHoraire}"/>
											<input type="submit" class="ui-icon ui-icon-closethick" /> 
										</form>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:otherwise>
				</c:choose>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>