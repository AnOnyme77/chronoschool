<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Evenement</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<link rel="stylesheet" type="text/css"
	href="js/jquery-timepicker-master/jquery.timepicker.css">
<link rel="stylesheet" type="text/css"
	href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript"
	src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript"
	src="js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
<script type="text/javascript"
	src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" 
	src="js/jquery-timepicker-master/jquery.timepicker.js"></script>
<script type="text/javascript" src="js/infosEvenement.js"></script>
</head>
<body>

	<div id="contentmain">
		<div id="content" class="clear">
			<h2>Information sur l'événement</h2>
			<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
			<form action="#" method="post">
				<input type="hidden" name="intit" value="${objet.intitule}"/>
				<input type="hidden" name="desc" value="${objet.description}"/>
				<input type="hidden" name="hDeb" value='<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${objet.heureDebut.time}"/>'/>
				<input type="hidden" name="hFin" value='<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${objet.heureFin.time}"/>'/>	
				<c:if test="${objet.intitule != null }">
					<input type="hidden" name="interne" value="1"/>
				</c:if>

				
				<label for="intitule">Intitule</label><br /> 
				<input name="intitule" type="text" id="intitule" value="${objet.intitule }"/><br> <br> 
				
				<label for="description">Description</label><br /> 
				<textarea name="description"  id="description"  style="resize:none;" cols="100%" rows="5">${objet.description}</textarea>
				<br> <br>

				<label for="heureDebut">Heure de début (HH:MM)</label><br /> 
				<input name="heureDebut" type="text" class="timepicker" id="heureDebut"  value='<fmt:formatDate pattern="H:m"  value="${objet.heureDebut.time}"/>'/><br> <br>
				
				<label for="heureFin">Heure de fin (HH:MM)</label><br /> 
				<input name="heureFin" type="text" class="timepicker" id="heureFin" value='<fmt:formatDate pattern="H:m"  value="${objet.heureFin.time}"/>'/><br> <br>
				
				<fmt:setLocale value="fr_FR"/>
				<c:set var="jourSel" ><fmt:formatDate pattern="EEEEEEEE"  value="${objet.heureDebut.time}"/></c:set>
				<label for="jour">Jour de l'événement</label><br />
				<select name="jour" id="jour">
				<c:forEach var="jour" items="${jours}" varStatus="vs">
				<c:choose>
					<c:when test="${jour == jourSel}">
						<option value="${jour }" selected="selected">${jour }</option>
					</c:when>
					<c:otherwise>
						<option value="${jour }">${jour }</option>
					</c:otherwise>
				</c:choose>
				</c:forEach>
				</select> 
				<br> <br>
				
				<div id="detailDialog">
				<h2>Choix du local</h2>
				<c:if test="${liste.size()>0}">
					<table id="listeLocaux">
						<thead>
							<tr>
								<th>Intitule</th>
								<th>Nombre de places</th>
								<th width="20px"></th>
							</tr>
						</thead>
						<tbody>
							<!-- generer les lignes -->
							<c:forEach var="local" items="${liste}" varStatus="vs">
								<tr>
									<td>
										${local.intitule}
										<span class="ui-icon ui-icon-info" style="float:left;" onClick="createDialog('Equipements du local', '<c:forEach var="equip" items="${local.equipements}">- ${equip.intitule}<br></c:forEach>')"></span>	
									</td>
									<td>${local.nbPlaces }</td>
									<td width="20px">
										<input type="checkbox" name="locaux" value="${local.idLocal}" <c:if test="${objet.locaux.contains(local)}">checked="checked"</c:if>  />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${liste.size() == 0 }">
					<p>Aucun local</p>
				</c:if>
				<br><br>
				<h2>Choix du cours</h2>
				<c:choose>
					<c:when test="${listeCours.size()>0}">
						<table id="listeCours">
							<thead>
								<tr>
									<th>Intitulé</th>
									<th>Classe</th>
									<th width="20px"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="cours" items="${listeCours}">
									<tr>
										<td>
											${cours.intitule }
											<span class="ui-icon ui-icon-info" style="float:left;" onClick="createDialog('Equipements du cours', '<c:forEach var="equip" items="${cours.equipements}">- ${equip.intitule}<br></c:forEach>')"></span>	
										</td>
										<td><c:if test="${cours.annee==null}">Aucune classe</c:if>
											<c:if test="${cours.annee!=null}">${cours.annee.numClasse} - ${cours.annee.section}</c:if>
										</td>
										<td width="20px">
											<input type="radio" name="cours" value="${cours.idCours}" <c:if test="${objet.cours.equals(cours)}"> checked="checked"</c:if> />
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<p>Aucun cours enregistré</p>
					</c:otherwise>
				</c:choose>
				</div>
				<br> <input type="submit" id="submit" value="Enregistrer" />
			</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>