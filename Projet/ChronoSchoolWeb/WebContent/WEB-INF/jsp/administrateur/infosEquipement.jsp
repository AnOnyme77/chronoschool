<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">  

<title>Equipement</title> 
<link href="css/style.css" rel="stylesheet" type="text/css"/> 
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/infosEquipement.js"></script>
</head> 
<body> 

	<div id="contentmain">   
		<div id="content" class="clear">
				<h2>Ajout d'un équipement</h2>
				<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<form action="#" method="post">
					<input type="hidden" name="id" value="${objet.idEquipement}"/>
					<label for="intitule">Intitule</label>
					<br /> <input maxlength="255" type="text" name="intitule" id="intitule" size="22" required="required"
						title="Veuillez saisir l'intitule" value="${objet.intitule}"/><br />
					<br /> 
					<label for="description">Description</label>
					<br /> <textarea  maxlength="255" style="resize: none;" draggable="false" rows="5" cols="15" type="text" name="description" id="description" size="22">${objet.description}</textarea> <br />
					<br />
					<input name="submit" type="submit" id="submit" value="Enregistrer" />
					<input name="reset" type="reset" id="reset" value="Reinitialiser les champs" />
				</form>
				
				<c:if test="${objet!=null && objet.locaux.size()>0 }">
					<br /><br />
					<table id="listeLocaux">
						<thead>
							<tr>
								<th>Locaux équipés</th>
								<th width="20px"></th>
							</tr>
						</thead>
						<tbody>
							<c:url value="infosLocal.html" var="url"/>
							<c:forEach items="${objet.locaux}" var="local">
								<tr>
									<td>${local.intitule}</td>
									<td width="20px">
										<form action="${url}" method="get"> 
											<input type="hidden" name="idLocal" value="${local.idLocal}"/>
											<input type="submit" class="ui-icon ui-icon-pencil" /> 
										</form>
										<form action="#" method="post"> 
											<input type="hidden" name="supprimer" value="${local.idLocal}"/>
											<input type="hidden" name="id" value="${objet.idEquipement}"/>
											<input type="hidden" name="intitule" value="${objet.intitule}"/>
											<input type="submit" class="ui-icon ui-icon-closethick" />
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>