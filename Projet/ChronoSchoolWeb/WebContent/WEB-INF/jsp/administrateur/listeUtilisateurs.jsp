<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Utilisateur - Liste</title>  
 
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="js/listeUtilisateurs.js"></script> 
 
	 

</head>
<body>

	<div id="contentmain"> 
		<div id="content" class="clear">
				<h2>Liste des utilisateurs</h2>
				<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty wrongId}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${wrongId}</p></div><br/></div>
					<c:remove var="wrongId"></c:remove>
				</c:if>
				<c:if test="${not empty msgModif}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${msgModif}</p></div><br/></div>
					<c:remove var="msgModif"></c:remove>
				</c:if>
				<c:if test="${liste.size()>0}">
					<table id="listeUtilisateurs">
						<thead>
							<tr>
								<th>ID</th>
								<th>E-mail</th>
								<th>Nom</th>
								<th>Prénom</th>
								<th>Classe</th>
								<th>Statut</th>
								<th width="20px"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="utilisateur" items="${liste}">
								<tr>
									<td>${utilisateur.idUtilisateur}</td>
									<td>${utilisateur.email}</td>
									<td>${utilisateur.nom}</td>
									<td>${utilisateur.prenom}</td>
									<td>
										<c:if test="${utilisateur.annee!=null}">${utilisateur.annee.numClasse}${utilisateur.annee.section}</c:if>
										<c:if test="${utilisateur.annee==null}">Non défini</c:if>
									</td>
									<td>
										<c:forEach var="role" items="${roles}">
											<c:if test="${role.idRole==utilisateur.status}">
												${role.nomRole}
											</c:if>
										</c:forEach>
									</td>
									<td width="20px">
										<c:url var="modifier" value="infosUtilisateur.html"></c:url>
										<form action="${modifier}" method="get"> 
											<input type="hidden" name="idUtilisateur" value="${utilisateur.idUtilisateur}"/>
											<input type="submit" class="ui-icon ui-icon-pencil" /> 
										</form>
										<c:url var="supprimer" value="listeUtilisateurs.html"></c:url>
										<form action="${supprimer}" method="post"> 
											<input type="hidden" name="supprimer" value="${utilisateur.idUtilisateur}"/>
											<input type="submit" class="ui-icon ui-icon-closethick" /> 
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${liste.size()<=0}">
					<p>Aucun utilisateur</p>
				</c:if>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>