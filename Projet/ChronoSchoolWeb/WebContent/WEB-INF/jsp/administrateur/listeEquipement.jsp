<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Equipement - Liste</title>
<link href="css/style.css" rel="stylesheet" type="text/css"/> 
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/listeEquipement.js"></script>
</head> 
<body> 

	<div id="contentmain">  
		<div id="content" class="clear">  
			<h2>Liste des équipements</h2>
				<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
			<c:url var="ajouter" value="infosEquipement.html"></c:url>
			<form action="${ajouter}" method="get" id="ajout">	
				<input type="submit" id="Ajouter" value="Ajouter un équipement" />
			</form>
			<br />
			<c:choose >
				<c:when test="${liste.size()>0}">
					<table id="listeEquipement">
						<thead>
							<tr>
								<th>Intitule</th>
								<th>Description</th>
								<th width="20px"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${liste}" var="equipement">
								<tr>
									<td>${equipement.intitule}</td>
									<td>
										<c:if test="${equipement.description=='' }">Non définie</c:if>
										${equipement.description}
									</td>
									<td width="20px">
										<c:url var="modifier" value="infosEquipement.html"></c:url>
										<form action="${modifier}" method="get"> 
											<input type="hidden" name="idEquipement" value="${equipement.idEquipement}"/>
											<input type="submit" class="ui-icon ui-icon-pencil" /> 
										</form>
										<c:url var="supprimer" value="listeEquipement.html"></c:url>
										<form action="${supprimer}" method="post"> 
											<input type="hidden" name="supprimer" value="${equipement.idEquipement}"/>
											<input type="submit" class="ui-icon ui-icon-closethick" /> 
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<p>Il n'y a aucun équipement</p>
				</c:otherwise>
			</c:choose>
			
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>