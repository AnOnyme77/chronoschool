<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Section</title>
	<link href="css/style.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
	<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
	
	<script type="text/javascript" src="js/infosSections.js"></script> 
</head>  
<body> 
	<div id="contentmain">  
		<div id="content" class="clear"> 
			<h2>Infos Section</h2>
			<c:if test="${not empty message}">
				<div id="tempAlert"><div class="ui-state-error ui-corner-all">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Erreur: </strong>${message}</p></div><br/></div>
				<c:remove var="message"></c:remove>			
			</c:if>
			<form action="#" method="post">
				<label for="section">Section</label>
				<br /> <input type="text" name="section" id="section" size="22" value="${annee.section}" required="required"
					title="Veuillez renseigner le nom de la section"/><br />
				<br /> 
				<label for="annee">Année</label><br />
				<input type="text" name="annee" id="annee" size="22" value="${annee.numClasse }" required="required"
					title="Veuillez renseigner l'année de la section"/><br />
				<br /> 
			
					<label for="etudiant">Etudiant</label><br/>
					<c:if test="${liste.size()>0}">
					<table id="listeUtilisateurs">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nom</th>
								<th>Prénom</th>
								<th>E-mail</th>
								<th>Section</th>
								<th width="20px"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="utilisateur" items="${liste}">
								<tr>
									<td>${utilisateur.idUtilisateur}</td>
									<td>${utilisateur.nom}</td>
									<td>${utilisateur.prenom}</td>
									<td>${utilisateur.email}</td>
									<td>${utilisateur.annee.numClasse}${utilisateur.annee.section}</td>
									<td width="20px">
										<c:if test="${utilisateur.annee.idAnnee==annee.idAnnee}"><input type="checkbox" name="utilisateur" value="${utilisateur.idUtilisateur}" checked></c:if>
										<c:if test="${utilisateur.annee.idAnnee!=annee.idAnnee}"><input type="checkbox" name="utilisateur" value="${utilisateur.idUtilisateur}"></c:if>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${liste.size()<=0}">
					<p>Aucun utilisateur</p>
				</c:if>
					<br/>
				<input type="hidden" name="idUser" value="${annee.idAnnee}"/>
				<input name="submit" type="submit" id="submit" value="Enregistrer les modifications" />
				<input name="reset" type="reset" id="reset" value="Reinitialiser les champs" />
			</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>