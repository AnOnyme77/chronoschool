<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Local - Liste</title>   
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
	<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="js/listeLocaux.js"></script>
	<script type="text/javascript"
		src="js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
</head> 
<body>

	<div id="contentmain"> 
		<div id="content" class="clear"> 
				<h2>Liste des locaux</h2>
				<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:url var="ajout" value="infosLocal.html"/>
				<form action="${ajout }" method="get">
				<input type="submit" value="Ajouter un local" />
				</form>
				<br/>
				<c:if test="${listeLocaux.size()>0}">
				<table id="listeLocaux">
					<thead>
						<tr>
							<th>Intitule</th>
							<th>Nombre de places</th>
							<%-- <th>Nombre d'équipements</th>--%>
							<th width="20px"></th>
						</tr>
					</thead>
					<tbody>
					<!-- generer les lignes -->
						<c:forEach var="local" items="${listeLocaux}" varStatus="vs">
						<tr>
							<td>
								${local.intitule }
								<span class="ui-icon ui-icon-info" style="float:left;" 
									onClick="createDialog('Equipements du local', '<c:forEach var="equip" items="${local.equipements}">- ${equip.intitule}<br></c:forEach>')">
								</span>
							</td>
							<td>${local.nbPlaces }</td>
							<%-- <td>${local.equipements.size() }</td>--%>
							<th width="20px">
								<c:url var="modifier" value="infosLocal.html"/>
								<form action="${modifier}" method="GET">
									<input type="hidden" name="idLocal" value="${local.idLocal }"/>
									<input type="submit" class="ui-icon ui-icon-pencil" /> 
								</form>
								<c:url var="supprimer" value="listeLocaux.html"/>
								<form action="${supprimer}" method="POST">
									<input type="hidden" name="idLocal" value="${local.idLocal}"/>
									<input type="submit" class="ui-icon ui-icon-closethick"/>
								</form>
							</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				</c:if>
				<c:if test="${listeLocaux.size() == 0 }">
				<p>Aucun local</p>
				</c:if>
				<br/>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>