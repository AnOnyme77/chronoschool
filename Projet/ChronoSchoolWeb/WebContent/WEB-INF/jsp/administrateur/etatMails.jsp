<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">  

<title>Etat Mail</title> 
<link href="css/style.css" rel="stylesheet" type="text/css"/> 

</head> 
<body> 

	<div id="contentmain">   
		<div id="content" class="clear">
				<h2>Etat d'envoi des mails</h2>
				<c:choose>
				<c:when test="${liste.size() > 0}">
				<table>
					<thead>
						<tr>
							<th>Destinataire</th>
							<th>Sujet</th>
							<th>Texte</th>
							<th>Date ajout</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${liste}" var="mail">
							<tr>
								<td><a href="infosUtilisateurs.html">${mail.destinataire.nom} ${mail.destinataire.prenom}</a></td>
								<td>${mail.email.titre }</td>
								<td>
									<c:if test="${fn:length(mail.email.contenu) > 50}" >
										<c:set var="contenu" value="${fn:substring(mail.email.contenu, 0, 60)} ..." /> 
									</c:if>
									<c:if test="${fn:length(mail.email.contenu) <= 50}" >
										<c:set var="contenu" value="${mail.email.contenu}" />
									</c:if>
									<c:out value="${contenu}"  />
								</td>
								<td><fmt:formatDate type="both" value="${mail.email.dateAjout.time}"/></td>
								<td>
									<c:choose>
										<c:when test="${mail.status eq 'U'.charAt(0) }">Non-envoyé</c:when>
										<c:when test="${mail.status eq 'S'.charAt(0) }">Envoyé</c:when>
										<c:when test="${mail.status eq 'E'.charAt(0) }">En erreur</c:when>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				</c:when>
				<c:otherwise>
					<p>Il n'y a aucun mail</p>
				</c:otherwise>
				</c:choose>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>