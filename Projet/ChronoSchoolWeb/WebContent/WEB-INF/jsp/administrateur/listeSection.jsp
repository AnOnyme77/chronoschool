<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Section - Liste</title>  
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="js/listeSections.js"></script> 

</head>
<body>
	<div id="contentmain"> 
		<div id="content" class="clear">
				<h2>Liste des sections</h2>
				<c:if test="${not empty message}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${message}</p></div><br/></div>
					<c:remove var="message"></c:remove>
				</c:if>
				<c:if test="${not empty wrongId}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${wrongId}</p></div><br/></div>
					<c:remove var="wrongId"></c:remove>
				</c:if>
				<c:if test="${not empty msgModif}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${msgModif}</p></div><br/></div>
					<c:remove var="msgModif"></c:remove>
				</c:if>
				<c:url value="infosSection.html" var="ajouter"></c:url>
				<form action="${ajouter}" method="get" id="ajout">	
					<input type="submit" id="Ajouter" value="Nouvelle section" />
				</form>	
				<br/>
				<c:if test="${liste.size()>0}">
					<table id="listeSections">
						<thead>
							<tr>
								<th>ID</th>
								<th>Section</th>
								<th>Année</th>
								<th width="20px"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="annee" items="${liste}">
								<tr>
									<td>${annee.idAnnee}</td>
									<td>${annee.section}</td>
									<td>${annee.numClasse}</td>
									<td width="20px">
										<c:url var="modifier" value="infosSection.html"></c:url>
										<form action="${modifier}" method="get"> 
											<input type="hidden" name="idAnnee" value="${annee.idAnnee}"/>
											<input type="submit" class="ui-icon ui-icon-pencil" /> 
										</form>
										<c:url var="supprimer" value="listeSection.html"></c:url>
										<form action="${supprimer}" method="post"> 
											<input type="hidden" name="supprimer" value="${annee.idAnnee}"/>
											<input type="submit" class="ui-icon ui-icon-closethick" /> 
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${liste.size()<=0}">
					<p>Aucune section</p>
				</c:if>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>