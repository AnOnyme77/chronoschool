<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Horaire</title> 
	<link href="css/style.css" rel="stylesheet" type="text/css"/> 
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
	<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
	<script type="text/javascript" src="js/infosHoraire.js"></script>
</head> 
<body> 

	<div id="contentmain">  
		<div id="content" class="clear"> 
				<h2>Information sur l'horaire</h2>
				<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${horaire.idHoraire!=0}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Attention: </strong>La modification d'un horaire entraine la perte des descriptions des événements et des présences des professeurs à ces derniers</p></div><br/></div>
				</c:if>
					<form action="#" method="post">
						<label for="dateDebut">Date de début de validité</label><br />
						<input name="dateDebut" type="date" id="dateDebut" value='<fmt:formatDate pattern="yyyy-MM-dd"  value="${horaire.dateDebut.time}"/>' /><br>
						<br>

						<label for="dateFin">Date de fin de validité</label><br />
						<input name="dateFin" type="date" id="dateFin" value='<fmt:formatDate pattern="yyyy-MM-dd"  value="${horaire.dateFin.time}"/>' /><br>
						<br>

						<label for="annee">Annee</label><br />
						<select name="annee">
							<c:choose>
								<c:when test="${liste!=null && liste.size()>0}">
									<c:forEach items="${liste}" var="annee">
										<c:choose>
											<c:when test="${horaire.annee.idAnnee==annee.idAnnee}">
												<option value="${annee.idAnnee}" selected="selected">${annee.numClasse} - ${annee.section}</option>
											</c:when>
											<c:otherwise>
												<option value="${annee.idAnnee}">${annee.numClasse} - ${annee.section}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<option>Aucune année enregistrée</option>
								</c:otherwise>
							</c:choose>
						</select>
						<br><br>

						<input type="submit" id="ajouter" name="ajouter" value="Ajouter un événement"/>
						<input type="submit" id="submit" name="enregistrer" value="Enregistrer l'horaire"/>
					</form>
				<h2>Evénements de l'horaire</h2>
					<table class="tableauHoraire">
						<thead> 
							<tr>
								<th></th>
								<th>Lundi</th>
								<th>Mardi</th>
								<th>Mercredi</th>
								<th>Jeudi</th>
								<th>Vendredi</th>
								<th>Samedi</th>
								<th>Dimanche</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${semaine.tranchesHoraire}" var="tranche" varStatus="vsTranche">
								<tr>
								<td>
									${tranche.getHeures()}
								</td>
								<c:forEach items="${tranche.jours}" var="jour" varStatus="vsJour">
									<c:choose>
										<c:when test="${vsTranche.index-1<0}">
											<c:if test="${jour.idJour!=-1}">
												<td rowspan="${jour.getRowCount()}" class="coursActif">
													<center>
													${jour.evenement.intitule}<br/>
													<c:url var="modifier" value="infosEvenement.html"></c:url>
													<form action="${modifier}" method="post" class="ui-state-default ui-corner-all"> 
														<input type="hidden" name="fromHoraire" value="2"/>
														<input type="hidden" name="intit" value="${jour.evenement.intitule}"/>
														<input type="hidden" name="desc" value="${jour.evenement.description}"/>
														<input type="hidden" name="hDeb" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureDebut.time}"/>"/>
														<input type="hidden" name="hFin" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureFin.time}"/>"/>
														<input type="submit" class="ui-icon ui-icon-pencil" /> 
													</form>
													<c:url var="supprimer" value="infosHoraire.html"></c:url>
													<form action="${supprimer}" method="post" class="ui-state-default ui-corner-all"> 
														<input type="hidden" name="supprimer" value="supprimer"/>
														<input type="hidden" name="intit" value="${jour.evenement.intitule}"/>
														<input type="hidden" name="desc" value="${jour.evenement.description}"/>
														<input type="hidden" name="hDeb" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureDebut.time}"/>"/>
														<input type="hidden" name="hFin" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureFin.time}"/>"/>
														<input type="submit" class="ui-icon ui-icon-closethick" /> 
													</form></center>
												</td>
											</c:if>
											<c:if test="${jour.idJour==-1}">
												<td></td>
											</c:if>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${jour.idJour==-1 }">
												 <td></td>
												</c:when>
												<c:when test="${jour.idJour!=-1 && 
												semaine.tranchesHoraire.get(vsTranche.index-1).jours.get(vsJour.index).idJour!=jour.idJour}">
													<td rowspan="${jour.getRowCount()}" class="coursActif">
														<center>
														${jour.evenement.intitule}<br/>
														<c:url var="modifier" value="infosEvenement.html"></c:url>
														<form action="${modifier}" method="post" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="fromHoraire" value="2"/>
															<input type="hidden" name="intit" value="${jour.evenement.intitule}"/>
															<input type="hidden" name="desc" value="${jour.evenement.description}"/>
															<input type="hidden" name="hDeb" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureDebut.time}"/>"/>
															<input type="hidden" name="hFin" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureFin.time}"/>"/>
															<input type="submit" class="ui-icon ui-icon-pencil" /> 
														</form>
														<c:url var="supprimer" value="infosHoraire.html"></c:url>
														<form action="${supprimer}" method="post" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="supprimer" value="supprimer"/>
															<input type="hidden" name="intit" value="${jour.evenement.intitule}"/>
															<input type="hidden" name="desc" value="${jour.evenement.description}"/>
															<input type="hidden" name="hDeb" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureDebut.time}"/>"/>
															<input type="hidden" name="hFin" value="<fmt:formatDate pattern="yyyy-MM-dd-HH-mm"  value="${jour.evenement.heureFin.time}"/>"/>
															<input type="submit" class="ui-icon ui-icon-closethick" /> 
														</form></center>
													</td>
												</c:when>
												<c:otherwise>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>