<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Affichage d'un horaire</title>
<link href="css/style.css" rel="stylesheet" type="text/css"/>  
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
<script type="text/javascript" src="js/afficherHoraire.js"></script>
</head>
<body>

	<div id="contentmain">  
		<div id="content" class="clear">  
			<h2>Horaire : ${annee.numClasse } ${annee.section }</h2>
			<c:if test="${not empty message}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${message}</p></div><br/></div>
				</c:if>
			<h3>Semaine du <fmt:formatDate pattern="dd-MM-yyyy" value="${dateDebut.time}"/> au <fmt:formatDate pattern="dd-MM-yyyy" value="${dateFin.time}"/></h3>
			<c:url var="page" value="afficherHoraire.html"></c:url>
			<form action="${page}" method="get"> 
				<input type="hidden" name="date" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${datePrecedente.time}"/>" />
				<input type="submit" class="ui-icon ui-icon-arrowthick-1-w" /> 
			</form>
			<form action="${page}" method="get"> 
				<input type="hidden" name="date" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${dateSuivante.time}"/>" />
				<input type="submit" class="ui-icon ui-icon-arrowthick-1-e" /> 
			</form>
			<table class="tableauHoraire" id="horaire">
				<thead> 
					<tr>
						<th></th>
						<th>Lundi</th>
						<th>Mardi</th>
						<th>Mercredi</th>
						<th>Jeudi</th>
						<th>Vendredi</th>
						<th>Samedi</th>
						<th>Dimanche</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${semaine.tranchesHoraire}" var="tranche" varStatus="vsTranche">
						<tr>
						<td>
							${tranche.getHeures()}
						</td>
						<c:forEach items="${tranche.jours}" var="jour" varStatus="vsJour">
							<c:choose>
								<c:when test="${vsTranche.index-1<0}">
									<c:if test="${jour.idJour!=-1}">
										<c:if test="${jour.evenement.statut eq 'A'.charAt(0)}">
												<td rowspan="${jour.getRowCount()}" class="coursActif">
											</c:if>
											<c:if test="${jour.evenement.statut eq 'S'.charAt(0)}">
												<td rowspan="${jour.getRowCount()}" class="coursSupprime">
											</c:if>
												<center>
												${jour.evenement.intitule}<br/>
												<div class="ui-state-default ui-corner-all">
													<span class="ui-icon ui-icon-info" 
														onClick="createDialog('Description de l événement', 'Cours : ${jour.evenement.cours.intitule} <br>Description : ${jour.evenement.description }')">
													</span>
												</div>
 												<c:if test="${sessionScope.user.status eq 'A'.charAt(0) || user.isTitulaire(jour.evenement.cours)}">
													<c:url var="modifier" value="modifierEvenement.html"></c:url>
													<form action="${modifier}" method="get" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="idEvenement" value="${jour.evenement.idEvenement}"/>
															<input type="submit" class="ui-icon ui-icon-pencil" /> 
													</form>
													<c:if test="${jour.evenement.statut eq 'A'.charAt(0)}">
														<c:url var="annuler" value="afficherHoraire.html"></c:url>
														<form action="${annuler}" method="post" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="date" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${dateDebut.time}"/>" />
															<input type="hidden" name="annuler" value="${jour.evenement.idEvenement}"/>
															<input type="submit" class="ui-icon ui-icon-circle-minus" /> 
														</form>
													</c:if>
													<c:if test="${jour.evenement.statut eq 'S'.charAt(0)}">
														<c:url var="donner" value="afficherHoraire.html"></c:url>
														<form action="${donner}" method="post" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="date" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${dateDebut.time}"/>" />
															<input type="hidden" name="donner" value="${jour.evenement.idEvenement}"/>
															<input type="submit" class="ui-icon ui-icon-circle-check" /> 
														</form>
													</c:if>
												</c:if></center>
											</td>
									</c:if>
									<c:if test="${jour.idJour==-1}">
										<td></td>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${jour.idJour==-1 }">
										 <td></td>
										</c:when>
										<c:when test="${jour.idJour!=-1 && 
										semaine.tranchesHoraire.get(vsTranche.index-1).jours.get(vsJour.index).idJour!=jour.idJour}">
											<c:if test="${jour.evenement.statut eq 'A'.charAt(0)}">
												<td rowspan="${jour.getRowCount()}" class="coursActif">
											</c:if>
											<c:if test="${jour.evenement.statut eq 'S'.charAt(0)}">
												<td rowspan="${jour.getRowCount()}" class="coursSupprime">
											</c:if>
												<center>
												${jour.evenement.intitule}<br/>
												<div class="ui-state-default ui-corner-all">
													<span class="ui-icon ui-icon-info" 
														onClick="createDialog('Description de l événement', 'Cours : ${jour.evenement.cours.intitule} <br>Description : ${jour.evenement.description }')">
													</span>
												</div>
 												<c:if test="${sessionScope.user.status eq 'A'.charAt(0) || user.isTitulaire(jour.evenement.cours)}">
													<c:url var="modifier" value="modifierEvenement.html"></c:url>
													<form action="${modifier}" method="get" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="idEvenement" value="${jour.evenement.idEvenement}"/>
															<input type="submit" class="ui-icon ui-icon-pencil" /> 
													</form>
													<c:if test="${jour.evenement.statut eq 'A'.charAt(0)}">
														<c:url var="annuler" value="afficherHoraire.html"></c:url>
														<form action="${annuler}" method="post" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="date" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${dateDebut.time}"/>" />
															<input type="hidden" name="annuler" value="${jour.evenement.idEvenement}"/>
															<input type="submit" class="ui-icon ui-icon-circle-minus" /> 
														</form>
													</c:if>
													<c:if test="${jour.evenement.statut eq 'S'.charAt(0)}">
														<c:url var="donner" value="afficherHoraire.html"></c:url>
														<form action="${donner}" method="post" class="ui-state-default ui-corner-all"> 
															<input type="hidden" name="date" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${dateDebut.time}"/>" />
															<input type="hidden" name="donner" value="${jour.evenement.idEvenement}"/>
															<input type="submit" class="ui-icon ui-icon-circle-check" /> 
														</form>
													</c:if>
												</c:if></center>
											</td>
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>