<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<title>Mon compte</title>
</head>
<body> 
	<div id="contentmain"> 
		<div id="content" class="clear">
			<h2>Mon Compte</h2>
			<c:if test="${not empty message}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${message}</p></div><br/></div>
				</c:if>
			<c:if test="${not empty msgModif}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${msgModif}</p></div><br/></div>
					<c:remove var="msgModif"></c:remove>
				</c:if>
			<form action="#" method="post">
				<label for="nom">Nom</label> <br /> <input type="text" name="nom"
					id="nom" size="22" value="${user.nom }" required="required"
					title="Veuillez renseinger votre nom" /><br /> <br /> <label
					for="prenom">Prénom</label><br /> <input type="text" name="prenom"
					id="prenom" size="22" value="${user.prenom }" required="required"
					title="Veuillez renseigner votre prénom" /><br /> <br /> <label
					for="email">Email</label><br /> <input type="text" name="email"
					id="email" size="22" value="${user.email }" required="required"
					title="Veuillez renseigner votre email" /><br /> <br /> <label
					for="dateNaiss">Date de naissance</label><br /> <input type="date"
					name="dateNaiss" id="dateNaiss" value="${date }"
					required="required"
					title="Veuillez renseigner votre date de naissance" /><br /> <br />
				<label for="sexe">Sexe</label><br />
				<c:if test="${user.sexe == 'Homme'}">
					<input type="radio" name="sexe" checked="checked" value="M">M
					<input type="radio" name="sexe" value="F">F<br />
				</c:if>
				<c:if test="${user.sexe == 'Femme'}">
					<input type="radio" name="sexe" value="M">M
					<input type="radio" name="sexe" checked="checked" value="F">F<br />
				</c:if>
				<br /> <label for="motDePasse">Nouveau mot de passe(laisser
					blanc pour ne pas modifier)</label><br /> <input type="password"
					name="motDePasse" id="motDePasse" /><br /> <br /> <label
					for="motDePasseConf">Confirmation nouveau mot de passe</label><br />
				<input type="password" name="motDePasseConf" id="motDePasseConf" /><br />
				<br /> <input name="submit" type="submit" id="submit"
					value="Enregistrer les modifications" /> <input name="reset"
					type="reset" id="reset" value="Reinitialiser les champs" />
			</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>