<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Cours</title>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="js/infosCours.js"></script> 

</head> 
<body>

	<div id="contentmain">    
		<div id="content" class="clear">
			<h2>Informations sur le cours</h2> 
			<c:if test="${operation==false}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
				<c:if test="${not empty requestScope.message && operation!=false}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
				</c:if>
			<form action="#" method="post"> 
				<c:if test="${objet!=null}"> 
					<input type="hidden" name="idCours" value="${objet.idCours}"/>
				</c:if>
				
				<label for="intitule">Intitulé</label><br/>
				<input type="textArea" name="intitule" id="intitule" size="22" required="required"
					<c:if test="${objet!=null}"> value="${objet.intitule}"</c:if>
					<c:if test="${userCours.status == 'U'.charAt(0) || (userCours.status == 'P'.charAt(0) && !userCours.isTitulaire(objet))}">disabled="disabled"</c:if> 
				/>
				
				<br/>
				<br/>
				
				<label for="description">Description</label><br/>
				<textarea name="description" id="description" cols="100%" rows="5" style="resize: none;" <c:if test="${userCours.status == 'U'.charAt(0) || (userCours.status == 'P'.charAt(0) && !userCours.isTitulaire(objet))}">disabled="disabled"</c:if>><c:if test="${objet!=null}">${objet.description}</c:if></textarea>
				
				<br/>
				<br/>
				
				<label for="annee">Année</label><br/>
				<select name="annee" id="annee" <c:if test="${userCours.status == 'U'.charAt(0)  || (userCours.status == 'P'.charAt(0) && !userCours.isTitulaire(objet))}">disabled="disabled"</c:if>>
					<c:choose>
						<c:when test="${liste_annee!=null && liste_annee.size()>0}">
							<c:forEach var="annee" items="${liste_annee}">
								<option value="${annee.idAnnee}" 
									<c:if test="${objet!=null && objet.annee.idAnnee==annee.idAnnee}"> selected</c:if>>
									${annee.numClasse} - ${annee.section}
								</option>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<option>Aucune année enregistrée</option>
						</c:otherwise>
					</c:choose>
				</select>
				<br>
				<br>
				<h2>Liste des professeurs</h2> 
				<c:if test="${userCours.status == 'A'.charAt(0) || (userCours.status == 'P'.charAt(0) && userCours.isTitulaire(objet))}">
					<c:choose>
						<c:when test="${liste_professeur.size()>0}">
							<table id="listeProfesseur">
								<thead> 
									<tr>
										<th>Nom</th>
										<th>Prénom</th>
										<th></th>
										<th>Titulaire</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${objet!=null}">
										<c:set var="listeCoursProf" value="${objet.getProfesseurs()}"></c:set>
										<c:forEach var="CoursProf" items="${listeCoursProf}">
											<c:if test="${CoursProf.titulaire}">
												<c:set var="titulaire" value="${CoursProf.professeur.idUtilisateur}"></c:set>
											</c:if>
										</c:forEach>
									</c:if>
									<c:forEach var="professeur" items="${liste_professeur}">
										<tr>
											<td>${professeur.nom}</td>
											<td>${professeur.prenom}</td>
											<td>
												<c:choose>
													<c:when test="${objet!=null && listeCoursProf!=null}">
														<c:set var="checked" value="false"></c:set>
														<c:forEach var="CoursProf" items="${listeCoursProf}">
															<c:if test="${CoursProf.contientUtilisateur(professeur)==true}">
																<c:set var="checked" value="true"></c:set>											
															</c:if>
														</c:forEach>
														<c:if test="${checked=='false'}">
															<input type="checkbox" name="professeur" value="${professeur.idUtilisateur}" onClick="cbChanged(this,${professeur.idUtilisateur});">																								
														</c:if>
														<c:if test="${checked=='true'}">
															<input type="checkbox" name="professeur" value="${professeur.idUtilisateur}" checked onClick="cbChanged(this,${professeur.idUtilisateur});">																								
														</c:if>
													</c:when>
													<c:otherwise>
														<input type="checkbox" name="professeur" value="${professeur.idUtilisateur}" onClick="cbChanged(this,${professeur.idUtilisateur});">
													</c:otherwise> 
												</c:choose>
											</td>
											<td>
												<input type="radio" id="${professeur.idUtilisateur}" value="${professeur.idUtilisateur}" name="titulaire" <c:if test="${professeur.idUtilisateur.equals(titulaire)}"> checked </c:if> <c:if test="${checked==null || checked=='false'}"> disabled </c:if>>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
							<p>Aucun professeur enregistré</p>
						</c:otherwise>
					</c:choose>
				</c:if>
<!-- 			affichage des professeurs pour l'utilisateur -->
				<c:if test="${userCours.status == 'U'.charAt(0) || (userCours.status == 'P'.charAt(0) && !userCours.isTitulaire(objet))}">
					<table id="listeProfesseur">
						<thead> 
							<tr>
								<th>Nom</th>
								<th>Prénom</th>
								<th>Titulaire</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${objet!=null}">
								<c:set var="listeCoursProf" value="${objet.getProfesseurs()}"></c:set>
								<c:forEach var="CoursProf" items="${listeCoursProf}">
									<tr>
										<td>
											${CoursProf.professeur.prenom}
										</td>
										<td>
											${CoursProf.professeur.nom}
										</td>
										<td>
											<input type="radio" disabled <c:if test="${CoursProf.titulaire}">checked="checked"</c:if>>
										</td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</c:if>
				<br>
				<c:if test="${userCours.status == 'A'.charAt(0) || (userCours.status == 'P'.charAt(0) && userCours.isTitulaire(objet))}">
					<h2>Liste des équipements</h2> 
					<c:choose>
						<c:when test="${liste_equipement.size()>0}">
							<table id="listeEquipement">
								<thead> 
									<tr>
										<th>intitulé</th>
										<th>description</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="equipement" items="${liste_equipement}">
										<tr>
											<td>${equipement.intitule}</td>
											<td>${equipement.description}</td>
											<td>
												<c:choose>
													<c:when test="${objet!=null}">
														<input type="checkbox" name="equipement" value="${equipement.idEquipement}" <c:if test="${objet.getEquipements().contains(equipement)}">checked </c:if>>
													</c:when>
													<c:otherwise>
														<input type="checkbox" name="equipement" value="${equipement.idEquipement}">
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
							<p>Aucun équipement enregistré</p>
						</c:otherwise>
					</c:choose>
					<br>		
					<input name="submit" type="submit" id="submit" value="Confirmer" />
					<c:if test="${objet==null}">
						<input name="reset" type="reset" id="reset" value="Reinitialisation" />
					</c:if>
				</c:if>
			</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>
