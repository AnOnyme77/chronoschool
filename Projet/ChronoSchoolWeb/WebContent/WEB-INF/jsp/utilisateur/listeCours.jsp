<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<title>Cours - Liste</title> 

<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript"
		src="js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
<script type="text/javascript" src="js/listeCours.js"></script> 

</head> 
<body> 
	<div id="contentmain">   
		<div id="content" class="clear">
			<h2>Liste des cours</h2>
			<c:if test="${operation==false}">
				<div id="tempAlert"><div class="ui-state-error ui-corner-all">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Erreur: </strong>${requestScope.message}</p></div><br/></div>
			</c:if>
			<c:if test="${not empty requestScope.message && operation!=false}">
				<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Info: </strong>${requestScope.message}</p></div><br/></div>
			</c:if>
			<c:if test="${user.status eq 'A'.charAt(0)}">
				<form action="infosCours.html" method="get" id="ajout">	
					<input type="submit" id="Ajouter" value="Nouveau Cours" />
				</form>
			</c:if>	
			<br/>
			<c:choose>
				<c:when test="${liste.size()>0}">
					<table id="listeCours">
						<thead> 
							<tr>
								<th>Intitulé</th>
								<th>Classe</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="cours" items="${liste}">
								<tr>
									<td>
										${cours.intitule}
										<span class="ui-icon ui-icon-info" style="float:left;" 
											onClick="createDialog('Equipements du cours', '<c:forEach var="equip" items="${cours.equipements}">- ${equip.intitule}<br></c:forEach>')">
										</span>
									</td>
									<td>
										<c:if test="${cours.annee==null}">Aucune classe</c:if>
										<c:if test="${cours.annee!=null}">${cours.annee.numClasse} - ${cours.annee.section}</c:if>
									</td>
									<td width="20px">
									<c:choose>
									<c:when test="${user.status == 'A'.charAt(0) || user.status == 'P'.charAt(0) }">
										<c:url var="modifier" value="infosCours.html"></c:url>
										<form action="${modifier}" method="get"> 
											<input type="hidden" name="idCours" value="${cours.idCours}"/>
											<input type="submit" class="ui-icon ui-icon-pencil" /> 
										</form>
										<c:if test="${user.status == 'A'.charAt(0)}">
											<c:url var="supprimer" value="listeCours.html"></c:url>
											<form action="${supprimer}" method="post"> 
												<input type="hidden" name="supprimer" value="${cours.idCours}"/>
												<input type="submit" class="ui-icon ui-icon-closethick" /> 
											</form>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:url var="afficher" value="infosCours.html" />
										<form action="${afficher }" method="get">
											<input type="hidden" name="idCours" value="${cours.idCours}"/>
											<input type="submit" class="ui-icon ui-icon-info" /> 
										</form>
									</c:otherwise>
									</c:choose>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<p>Aucun cours enregistré</p>
				</c:otherwise>
			</c:choose>				
			<br/>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>