<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="css/style.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
	<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/inscription.js"></script> 
	<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
	<title>
		<c:if test="${user != null}">Informations utilisateur</c:if>
		<c:if test="${user == null}">Inscription</c:if>
	</title>
</head> 
<body>
	<div id="contentmain">   
		<div id="content" class="clear"> 
			<h2>
				<c:if test="${user != null}">Informations utilisateur</c:if>
				<c:if test="${user == null}">Inscription</c:if>
			</h2> 
			<c:if test="${not empty msg}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${msg}</p></div><br/></div>
				</c:if>
			<form action="#" method="post">
				<label for="nom">Nom</label>
				<br /> <input type="text" name="nom" id="nom" size="22" required="required"
					title="Veuillez renseigner votre nom"/><br />
				<br /> 
				<label for="prenom">Prénom</label><br />
				<input type="text" name="prenom" id="prenom" size="22" required="required"
					title="Veuillez renseigner votre prénom"/><br />
				<br /> 
				<label for="email">Email</label><br />
				<input type="text" name="email" id="email" size="22" required="required"
					title="Veuillez renseigner votre email"/><br />
				<br />
				<label for="dateNaiss">Date de naissance (AAAA-MM-JJ)</label><br/>
				<input type="text" name="dateNaiss" id="dateNaiss" required="required"
					title="Veuillez renseigner votre date de naissance"/><br/> 
				<br/>
				<label for="sexe">Sexe</label><br/>
				<input type="radio" name="sexe" checked="checked" value="M">M</input>
				<input type="radio" name="sexe" value="F">F</input><br/> 
				<br/>					
				<label for="motDePasse">Mot de passe</label><br/>
				<input type="password" name="motDePasse" id="motDePasse" required="required"
					title="Veuillez renseigner votre mot de passe"/><br/>
				<br/>
				<label for="motDePasseConf">Confirmation mot de passe</label><br/>
				<input type="password" name="motDePasseConf" id="motDePasseConf" required="required"
					title="Veuillez confirmer votre mot de passe"/><br/>
				<br/>
				<input name="submit" type="submit" id="submit" value="Inscription" />
				<input name="reset" type="reset" id="reset" value="Reinitialiser les champs" />
			</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>
