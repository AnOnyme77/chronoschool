<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
<title>Accueil</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
</head>
<body>
	<div id="contentmain">  
		<div id="content" class="clear">
			<div class="center">
				<c:if test="${not empty msgInscription}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${msgInscription}</p></div><br/></div>
					<c:remove var="msgInscription"></c:remove>
				</c:if>
				<H3>Bienvenue sur le site ChronoSchool</H3>
				<p>Notre objectif ? Vous rendre la vie meilleur !</p>
				<c:url var="urlImage" value="/images/ecole.png" />
				<img src="${urlImage}" width="500" />
			</div>
		</div>
	</div>
</body>
</html>
