<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Mot de passe oublié</title> 
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
</head>
<body>
	<div id="contentmain"> 
		<div id="content" class="clear"> 
				<h2>Récupération de mot de passe</h2>
				<c:if test="${not empty erreurMdpOublie}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${erreurMdpOublie}</p></div><br/></div>
					<c:remove var="erreurMdpOublie"></c:remove>
				</c:if>
				<c:if test="${not empty messageMdpOublie}">
					<div id="tempAlert"><div class="ui-state-highlight ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Info: </strong>${messageMdpOublie}</p></div><br/></div>
					<c:remove var="messageMdpOublie"></c:remove>
				</c:if>
				<form action="mdpOublie.html" method="post">
					<label for="email">Entrez votre Email</label><br/>
					<input type="text" name="email" id="email" size="22" /><br/>
					<br/> 
					<input name="submit" type="submit" id="submit" value="Confirmer" />
				</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>