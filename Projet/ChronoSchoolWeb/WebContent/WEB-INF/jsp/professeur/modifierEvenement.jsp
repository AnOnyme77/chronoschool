<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Evenement</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
</head>
<body>

	<div id="contentmain">
		<div id="content" class="clear">
			<h2>Information sur l'événement</h2>
			<c:set value="style='color:green;'" var="styleMessage"></c:set>
			<c:if test="${operation==false}">
				<c:set value="style='color:red;'" var="styleMessage"></c:set>
			</c:if>
			<h3 ${styleMessage}>${requestScope.message}</h3>
			<form action="#" method="post">
				<input type="hidden" name="idEvenement" value="${objet.idEvenement}"/>
				
				<label for="intitule">Intitule</label><br /> 
				<input name="intitule" type="text" id="intitule" value="${objet.intitule}"/><br> <br> 
				
				<label for="description">Description</label><br /> 
				<textarea name="description"  id="description"  style="resize:none;" cols="100%" rows="5">${objet.description}</textarea><br> <br>
				
				<br> <input type="submit" id="submit" value="Enregistrer" />
			</form>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>