<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Local</title> 
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/DataTables-1.10.4/media/css/jquery.dataTables_themeroller.css" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.11.2.custom/jquery-ui.css" />
<script type="text/javascript" src="js/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="js/infosLocal.js"></script> 
</head>
<body> 
	<div id="contentmain">   
		<div id="content" class="clear">
		
		<c:choose>
		<%-- Ajout d'un local --%>
		<c:when test="${local == null || local.idLocal == 0}">
			<h2>Ajout du local</h2> 
			<c:if test="${not empty message}">
					<div id="tempAlert"><div class="ui-state-error ui-corner-all">
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Erreur: </strong>${message}</p></div><br/></div>
				</c:if>
			<c:url var="ici" value="infosLocal.html"/>
			<form action="${ici }" method="post">
				<label for="intitule">Intitulé</label><br/>
				<input type="text" name="intitule" id="intitule" size="22" value="${local.intitule}" required/><br/>
				<br/>
				<label for="nbPlaces">Nombre  de places</label><br/>
				<c:if test="${local.nbPlaces != 0}">
				<c:set var="nb" value="${local.nbPlaces }"/>
				</c:if>
				<input type="text" name="nbPlaces" id="nbPlaces" size="22" value="${ nb}" required/><br/>
				<br/>
				<h2>Equipements</h2><br/>
				<c:if test="${ listeEquipements.size()>0}">
				<table id="listeEquipements">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Description</th>
							<th width="20px"></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="equipement" items="${listeEquipements }" varStatus="vs">
					<tr>
						<td>${equipement.intitule }</td>
						<td>${equipement.description }</td>
						<td width="20px">
							<c:choose>
							<c:when test="${local.equipements.contains(equipement)}"><input type="checkbox" name="equipements" value="${equipement.idEquipement}" checked="checked"></c:when>
							<c:otherwise><input type="checkbox" name="equipements" value="${equipement.idEquipement}"></c:otherwise>
							</c:choose>
						</td>	
					</tr>				
					</c:forEach>
					</tbody>
				</table>
				</c:if>
				<c:if test="${ listeEquipements.size()<=0}">
					Aucun Equipement enregistré.
				</c:if>
				<br><br>
				<input name="submit" type="submit" id="submit" value="Enregistrer les modifications" />
				<input name="reset" type="reset" id="reset" value="Reinitialiser les champs" />
			</form>
			<div class="clear"></div>
		</c:when>
		<%-- Modification d'un local --%>
		<c:otherwise>
		<h2>Modification du local</h2> 
			<c:if test="${not empty message}">
				<h3 style="color:red;">${message}</h3>
			</c:if>
			<c:url var="ici" value="infosLocal.html"/>
			<form action="${ici }" method="post">
				<label for="intitule">Intitulé</label><br/>
				<input type="text" name="intitule" id="intitule" size="22" value="${local.intitule }"/><br/>
				<br/>
				<label for="nbPlaces">Nombre  de places</label><br/>
				<input type="text" name="nbPlaces" id="nbPlaces" size="22" value="${local.nbPlaces }"/><br/>
				<br/>
				<label for="equipements">Equipements</label><br/>
				<c:if test="${ listeEquipements.size()>0}">
				<table id="listeEquipements">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Description</th>
							<th width="20px"></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="equipement" items="${listeEquipements }" varStatus="vs">
					<tr>
						<td>${equipement.intitule }</td>
						<td>${equipement.description }</td>
						<td width="20px">
							<c:choose>
							<c:when test="${local.equipements.contains(equipement)}"><input type="checkbox" name="equipements" value="${equipement.idEquipement}" checked="checked"></c:when>
							<c:otherwise><input type="checkbox" name="equipements" value="${equipement.idEquipement}"></c:otherwise>
							</c:choose>
						</td>	
					</tr>				
					</c:forEach>
					</tbody>
				</table>
				</c:if>
				<input type="hidden" name="idLocal" value="${local.idLocal }"/>
				<input name="submit" type="submit" id="submit" value="Enregistrer les modifications" />
				<input name="reset" type="reset" id="reset" value="Reinitialiser les champs" />
			</form>
			<div class="clear"></div>
		</c:otherwise>
		</c:choose>
		</div>
	</div>
</body>
</html>