$(document).ready(function() {
    $('#listeLocaux').dataTable( {
		"bJQueryUI": true,
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, -1], [10, 25, "tous les"]],
		"order": [[ 0, 'asc' ]],
		"columnDefs":
            [ {
                "targets": [ 2 ],
                "searchable": false,
                "orderDataType": "dom-checkbox"
            } ],
        "language":
		{
			"decimal": ",",
            "thousands": ".",
		    "sProcessing":     "Traitement en cours...",
		    "sSearch":         "Rechercher&nbsp;:",
		    "sLengthMenu":     "Afficher _MENU_ locaux",
		    "sInfo":           "Affichage de _START_ &agrave; _END_ locaux sur _TOTAL_ locaux",
		    "sInfoEmpty":      "Affichage de 0 local",
		    "sInfoFiltered":   "(filtr&eacute; de _MAX_ locaux au total)",
		    "sInfoPostFix":    "",
		    "sLoadingRecords": "Chargement en cours...",
		    "sZeroRecords":    "Aucun local &agrave; affiché",
		    "sEmptyTable":     "Aucun local disponible dans le tableau",
		    "oPaginate":
		    {
		        "sFirst":      "Premier",
		        "sPrevious":   "Pr&eacute;c&eacute;dent",
		        "sNext":       "Suivant",
		        "sLast":       "Dernier"
		    },
		    "oAria":
		    {
		        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
		    }
		}
    } );
    $('#listeCours').dataTable( {
		"bJQueryUI": true,
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "tous les"]],
		"order": [[ 0, 'asc' ]],
        "columnDefs":
            [ {
                "targets": [ 2 ],
                "searchable": false,
                "orderable" : false
            } ],
        "language":
		{
			"decimal": ",",
            "thousands": ".",
		    "sProcessing":     "Traitement en cours...",
		    "sSearch":         "Rechercher&nbsp;:",
		    "sLengthMenu":     "Afficher _MENU_ cours",
		    "sInfo":           "Affichage de _START_ &agrave; _END_ cours sur _TOTAL_ cours",
		    "sInfoEmpty":      "Affichage de 0 cours",
		    "sInfoFiltered":   "(filtr&eacute; de _MAX_ cours au total)",
		    "sInfoPostFix":    "",
		    "sLoadingRecords": "Chargement en cours...",
		    "sZeroRecords":    "Aucun cours &agrave; affiché",
		    "sEmptyTable":     "Aucun cours disponible dans le tableau",
		    "oPaginate":
		    {
		        "sFirst":      "Premier",
		        "sPrevious":   "Pr&eacute;c&eacute;dent",
		        "sNext":       "Suivant",
		        "sLast":       "Dernier"
		    },
		    "oAria":
		    {
		        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
		    }
		}

    } );

	$(function() {
    	$( ".timepicker" ).timepicker(({
    		'timeFormat': 'H:i',
    		'step': 15,
    		'minTime': '6',
    		'maxTime' : '23'
    	}));
    });
	
//	$('#heureDebut').on('changeTime', function() {
//		$('#heureFin').timepicker({
//		    'minTime': $(this).val(),
//		    'maxTime': '23',
//		    'timeFormat': 'H:i',
//		    'step': 15
//		});
//	});
//	
//	$('#heureFin').on('changeTime', function() {
//		$('#heureDebut').timepicker({
//		    'minTime': '6',
//		    'maxTime': $(this).val(),
//		    'timeFormat': 'H:i',
//		    'step': 15
//		});
//	});
} );

$( "#locauxNon" ).dialog({
	autoOpen: true,
	width: 400,
	buttons: [
		{
			text: "Ok",
			click: function() {
				$( this ).dialog( "close" );
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

function createDialog(title, text){
	if(!text)
		text="Aucun équipements requis";
    return $("<div class='dialog' title='" + title + "'><p>" + text + "</p></div>")
    .dialog({
        buttons: [
          		{
          			text: "Ok",
          			click: function() {
          				$( this ).dialog( "close" );
          			}
          		}
          	],
        position: {
        	my: "center center",
        	at: "center center",
        	of: "#detailDialog"
        }
    });
}
//Déclaration tri pour les checkbox
$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).prop('checked') ? '0' : '1';
    } );
};