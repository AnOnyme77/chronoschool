$(document).ready(function() {
    $('#listeUtilisateurs').dataTable( {
		"bJQueryUI": true,
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "tous les"]],
		"order": [[ 1, 'asc' ], [ 2, 'asc' ], [ 3, 'asc' ], [ 4, 'asc' ]],
        "columnDefs":
        [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 5 ],
            "searchable": false,
            //Déclaration du tri
            "orderDataType": "dom-checkbox"
        } ],
        "language":
		{
			"decimal": ",",
            "thousands": ".",
		    "sProcessing":     "Traitement en cours...",
		    "sSearch":         "Rechercher&nbsp;:",
		    "sLengthMenu":     "Afficher _MENU_ utilisateurs",
		    "sInfo":           "Affichage de _START_ &agrave; _END_ utilisateurs sur _TOTAL_ utilisateurs",
		    "sInfoEmpty":      "Affichage de 0 utilisateur",
		    "sInfoFiltered":   "(filtr&eacute; de _MAX_ utilisateurs au total)",
		    "sInfoPostFix":    "",
		    "sLoadingRecords": "Chargement en cours...",
		    "sZeroRecords":    "Aucun utilisateur &agrave; afficher",
		    "sEmptyTable":     "Aucun utilisateur disponible dans le tableau",
		    "oPaginate":
		    {
		        "sFirst":      "Premier",
		        "sPrevious":   "Pr&eacute;c&eacute;dent",
		        "sNext":       "Suivant",
		        "sLast":       "Dernier"
		    },
		    "oAria":
		    {
		        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
		    }
		}
    } );
} );
//Déclaration tri pour les checkbox
$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).prop('checked') ? '0' : '1';
    } );
};

