$(document).ready(function() {
    
    $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
	$("#dateDebut").datepicker(
	{ 
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
			'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin',
			'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D','L','M','M','J','V','S'],
		weekHeader: 'Sem.',
		firstDay: 1,
		yearRange: "c-100:c+1",
		onClose: function( selectedDate ) {
			 $( "#dateFin" ).datepicker( "option", "minDate", selectedDate );
		}
	});

	$("#dateFin").datepicker(
	{ 
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		closeText: 'Fermer',
		prevText: 'Pr�c�dent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
			'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin',
			'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D','L','M','M','J','V','S'],
		weekHeader: 'Sem.',
		firstDay: 1,
		yearRange: "c-100:c+1",
		onClose: function( selectedDate ) {
			 $( "#dateDebut" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
} );