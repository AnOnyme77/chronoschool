function createDialog(title, text){
    return $("<div class='dialog' title='" + title + "'><p>" + text + "</p></div>")
    .dialog({
        buttons: [
          		{
          			text: "Ok",
          			click: function() {
          				$( this ).dialog( "close" );
          			}
          		}
          	],
        position: {
        	my: "center center",
        	at: "center center",
        	of: "#horaire"
        }
    });
}