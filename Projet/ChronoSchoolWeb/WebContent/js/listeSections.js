$(document).ready(function() {
    $('#listeSections').dataTable( {
		"bJQueryUI": true,
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "toutes les"]],
		"order": [[ 1, 'asc' ], [ 2, 'asc' ]],
        "columnDefs":
        [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 3 ],
            "searchable": false,
            "orderable": false
        } ],
        "language":
		{
			"decimal": ",",
            "thousands": ".",
		    "sProcessing":     "Traitement en cours...",
		    "sSearch":         "Rechercher&nbsp;:",
		    "sLengthMenu":     "Afficher _MENU_ sections",
		    "sInfo":           "Affichage de _START_ &agrave; _END_ sections sur _TOTAL_ sections",
		    "sInfoEmpty":      "Affichage de 0 section",
		    "sInfoFiltered":   "(filtr&eacute; de _MAX_ sections au total)",
		    "sInfoPostFix":    "",
		    "sLoadingRecords": "Chargement en cours...",
		    "sZeroRecords":    "Aucune section &agrave; afficher",
		    "sEmptyTable":     "Aucune section disponible dans le tableau",
		    "oPaginate":
		    {
		        "sFirst":      "Premier",
		        "sPrevious":   "Pr&eacute;c&eacute;dent",
		        "sNext":       "Suivant",
		        "sLast":       "Dernier"
		    },
		    "oAria":
		    {
		        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
		    }
		}
    } );
} );