$(document).ready(function() {
    $('#listeEquipement').dataTable( {
		"bJQueryUI": true,
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "tous les"]],
		"order": [[ 0, 'asc' ], [ 1, 'asc' ]],
        "columnDefs":
        [ {
            "targets": [ 2 ],
            "visible": true,
            "searchable": false,
            "orderable": false
        } ],
        "language":
		{
			"decimal": ",",
            "thousands": ".",
		    "sProcessing":     "Traitement en cours...",
		    "sSearch":         "Rechercher&nbsp;:",
		    "sLengthMenu":     "Afficher _MENU_ équipements",
		    "sInfo":           "Affichage de _START_ &agrave; _END_ équipements sur _TOTAL_ équipements",
		    "sInfoEmpty":      "Affichage de 0 utilisateur",
		    "sInfoFiltered":   "(filtr&eacute; de _MAX_ équipements au total)",
		    "sInfoPostFix":    "",
		    "sLoadingRecords": "Chargement en cours...",
		    "sZeroRecords":    "Aucun équipement &agrave; afficher",
		    "sEmptyTable":     "Aucun équipement disponible dans le tableau",
		    "oPaginate":
		    {
		        "sFirst":      "Premier",
		        "sPrevious":   "Pr&eacute;c&eacute;dent",
		        "sNext":       "Suivant",
		        "sLast":       "Dernier"
		    },
		    "oAria":
		    {
		        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
		    }
		}
    } );
} );