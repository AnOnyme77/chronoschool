$(document).ready(function() {
    $('#listeCours').dataTable( {
		"bJQueryUI": true,
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "tous les"]],
		"order": [[ 0, 'asc' ]],
        "language":
		{
			"decimal": ",",
            "thousands": ".",
		    "sProcessing":     "Traitement en cours...",
		    "sSearch":         "Rechercher&nbsp;:",
		    "sLengthMenu":     "Afficher _MENU_ cours",
		    "sInfo":           "Affichage de _START_ &agrave; _END_ cours sur _TOTAL_ cours",
		    "sInfoEmpty":      "Affichage de 0 cours",
		    "sInfoFiltered":   "(filtr&eacute; de _MAX_ cours au total)",
		    "sInfoPostFix":    "",
		    "sLoadingRecords": "Chargement en cours...",
		    "sZeroRecords":    "Aucun cours &agrave; affiché",
		    "sEmptyTable":     "Aucun cours disponible dans le tableau",
		    "oPaginate":
		    {
		        "sFirst":      "Premier",
		        "sPrevious":   "Pr&eacute;c&eacute;dent",
		        "sNext":       "Suivant",
		        "sLast":       "Dernier"
		    },
		    "oAria":
		    {
		        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
		    }
		}
    } );
} );

function createDialog(title, text){
	if(!text)
		text="Aucun équipements requis";
    return $("<div class='dialog' title='" + title + "'><p>" + text + "</p></div>")
    .dialog({
        buttons: [
          		{
          			text: "Ok",
          			click: function() {
          				$( this ).dialog( "close" );
          			}
          		}
          	],
        position: {
        	my: "center center",
        	at: "center center",
        	of: "#listeCours"
        }
    });
}