package filtres;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.SecurityParser;
import domaine.Constantes;
import domaine.PageRight;
import domaine.Utilisateur;

/**
 * Servlet Filter implementation class FiltreConnexion
 */
@WebFilter()
public class FiltreConnexion implements Filter {

	private Map<String,PageRight> pages;
	private boolean activated;
	
	public void destroy() {
		this.pages=null;
		this.activated = false;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse rep = (HttpServletResponse)response;		
		String url = req.getRequestURI().substring(req.getServletContext().getContextPath().length());
		Utilisateur user = this.getConnectedUser(req);
		boolean redirect = true;

		if(this.pages.containsKey(url)){
			PageRight page = this.pages.get(url);
			if(!page.isConnexion() ||(user!=null && page.containsRole(user.getStatus())))redirect = false;
		}
		
		if(this.activated && redirect && !this.isUnfiltered(url)){
			if(user!=null) rep.sendRedirect(rep.encodeRedirectURL(req.getContextPath()+"/afficherHoraire.html"));
			else rep.sendRedirect(rep.encodeRedirectURL(req.getContextPath()+"/index.html"));
		}
		else{
			chain.doFilter(request, response);
		}
		
	}
	
	public boolean isUnfiltered(String url){
		return(url.contains("/css/") || url.contains("/js/") || url.contains("/images/"));
	}
	
	public Utilisateur getConnectedUser(HttpServletRequest req){
		HttpSession session = req.getSession(true);
		Utilisateur connected = null;
		synchronized(session){
			connected = (Utilisateur)session.getAttribute(Constantes.ATT_SESSION_USER);
		}
		return(connected);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		SecurityParser parser = new SecurityParser();
		this.pages = parser.parser(fConfig.getServletContext().getResourceAsStream("/WEB-INF/security.xml"));
		this.activated = true;
		this.activated = (fConfig.getInitParameter("Active")!=null && !fConfig.getInitParameter("Active").equals("true")?false:true);
	}

}
