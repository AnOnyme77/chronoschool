package domaine;

import java.util.Calendar;


public class Jour {
	private Evenement evenement;
	private int idJour;
	
	public Jour() {
		super();
		this.initialiser();
	}
	
	public void vider(){
		this.initialiser();
	}
	
	private void initialiser(){
		this.idJour = -1;
		this.evenement = new Evenement();
		this.evenement.setIdEvenement(-1);
	}
	
	public Evenement getEvenement() {
		return evenement;
	}

	public void setEvenement(Evenement evenement,int idJour) {
		this.evenement = evenement;
		this.idJour = idJour;
	}

	public Calendar getDebut(){
		return this.evenement.getHeureDebut();
	}
	
	public long getRowCount(){
		long nombreSecondes=((this.evenement.getHeureFin().getTimeInMillis()-this.evenement.getHeureDebut().getTimeInMillis())/1000);
		return((nombreSecondes/60/(60/Semaine.TRANCHE_PAR_HEURE)));
	}
	
	public Calendar getFin(){
		return this.evenement.getHeureFin();
	}
	
	public String getIntitule(){
		return this.evenement.getCours().getIntitule();
	}
	
	public int getIdEvenement(){
		return this.evenement.getIdEvenement();
	}
	
	public void supprimerEvenement(){
		this.evenement.setIdEvenement(-1);
	}

	public int getIdJour() {
		return idJour;
	}

	public void setIdJour(int idJour) {
		this.idJour = idJour;
	}
	
	
}
