package domaine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class Semaine {
	public static final int LUNDI = 0;
	public static final int MARDI = 1;
	public static final int MERCREDI = 2;
	public static final int JEUDI = 3;
	public static final int VENDREDI = 4;
	public static final int SAMEDI = 5;
	public static final int DIMANCHE = 6;

	public static final int NOMBRE_JOURS = 7;
	public static final int NOMBRE_TRANCHES = 72;
	public static final int HEURE_DEBUT = 6;
	public static final int MINUTE_DEBUT = 0;
	public static final int HEURE_FIN = 6;
	public static final int MINUTE_FIN = 0;
	public static final int TRANCHE_PAR_HEURE = 4;

	private ArrayList<TrancheHoraire> tranchesHoraire;
	private int idJour;

	public Semaine() {
		this.idJour=1;
		this.tranchesHoraire = new ArrayList<TrancheHoraire>();
		this.initialiser();
	}

	public void ajouterEvenement(Evenement evenement) {
		Jour j;
		int jour;
		for (TrancheHoraire t : this.tranchesHoraire) {
			if (t.evenementInTranche(evenement)) {
				j = new Jour();
				j.setEvenement(evenement,this.idJour);
				j.setIdJour(this.idJour);
				jour = this.convertAnglaisFrancais(evenement.getHeureDebut()
						.get(Calendar.DAY_OF_WEEK));

				t.getJours().set(jour, j);
			}
		}
		this.idJour++;
	}
	
	public void ajouterListeEvenement(List<Evenement> evenements){
		if(evenements != null)
		for(Evenement ev:evenements){
			this.ajouterEvenement(ev);
		}
	}
	
	public Evenement getEvenement(int idJour){
		Evenement evenement = null;
		for (TrancheHoraire t : this.tranchesHoraire) {
			for (Jour jour : t.getJours()) {
				if(jour.getIdJour()==idJour){
					evenement = jour.getEvenement();
				}
			}
		}
		return evenement;
	}
	
	public void modifierEvenement(int idJour, Evenement nouveau) {
		for (TrancheHoraire t : this.tranchesHoraire) {
			for (Jour jour : t.getJours()) {
				if (jour.getIdJour()==idJour) {
					jour.setEvenement(nouveau,idJour);
				}
			}
		}
		idJour++;
	}
	public void supprimerEvenement(int id){
		for (TrancheHoraire t : this.tranchesHoraire) {
			for(Jour j:t.getJours()){
				if(j.getIdJour()==id)
					j.vider();
			}
		}
	}

	private int convertAnglaisFrancais(int jour) {
		int retour = -1;
		switch (jour) {
		case Calendar.MONDAY:
			retour = LUNDI;
			break;
		case Calendar.TUESDAY:
			retour = MARDI;
			break;
		case Calendar.WEDNESDAY:
			retour = MERCREDI;
			break;
		case Calendar.THURSDAY:
			retour = JEUDI;
			break;
		case Calendar.FRIDAY:
			retour = VENDREDI;
			break;
		case Calendar.SATURDAY:
			retour = SAMEDI;
			break;
		case Calendar.SUNDAY:
			retour = DIMANCHE;
			break;

		}
		return retour;
	}

	public List<Evenement> getListeEvenement() {
		ArrayList<Evenement> retour = new ArrayList<Evenement>();

		for (TrancheHoraire t : this.tranchesHoraire) {
			for (Jour jour : t.getJours()) {
				if (jour.getEvenement().getIdEvenement() != -1
						&& !retour.contains(jour.getEvenement()))
					retour.add(jour.getEvenement());
			}
		}
		return (retour);
	}

	private void initialiser() {
		TrancheHoraire tranche;

		

		for (int i = 0; i < NOMBRE_TRANCHES; i++) {
			GregorianCalendar debut = new GregorianCalendar();
			debut.set(Calendar.HOUR_OF_DAY, HEURE_DEBUT);
			debut.set(Calendar.MINUTE, MINUTE_DEBUT);
			debut.add(Calendar.MINUTE, i*(60 / TRANCHE_PAR_HEURE));
			
			tranche = new TrancheHoraire();
			tranche.setHeureDebut(debut);

			debut = new GregorianCalendar();
			debut.set(Calendar.HOUR_OF_DAY, HEURE_DEBUT);
			debut.set(Calendar.MINUTE, MINUTE_DEBUT);
			debut.add(Calendar.MINUTE, (i+1)*(60 / TRANCHE_PAR_HEURE));
			tranche.setHeureFin(debut);

			this.tranchesHoraire.add(tranche);
		}
	}

	public void vider() {
		this.initialiser();
	}

	public static int getNombreTranches() {
		return NOMBRE_TRANCHES;
	}

	public ArrayList<TrancheHoraire> getTranchesHoraire() {
		return tranchesHoraire;
	}

	public void setTranchesHoraire(ArrayList<TrancheHoraire> tranchesHoraire) {
		this.tranchesHoraire = tranchesHoraire;
	}
	
	

}
