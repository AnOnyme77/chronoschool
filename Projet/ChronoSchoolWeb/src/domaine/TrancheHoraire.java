package domaine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TrancheHoraire {
	
	private Calendar heureDebut;
	private Calendar heureFin;
	private ArrayList<Jour> jours;
	
	
	
	public TrancheHoraire() {
		super();
		this.heureDebut = new GregorianCalendar();
		this.heureFin = new GregorianCalendar();
		this.jours = new ArrayList<Jour>();
		
		for(int i=0;i<Semaine.NOMBRE_JOURS;i++){
			this.jours.add(new Jour());
		}
	}

	public TrancheHoraire(Calendar heureDebut, Calendar heureFin,
			ArrayList<Jour> jours) {
		super();
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.jours = jours;
	}

	public ArrayList<Jour> getJours() {
		return jours;
	}
	
	public void setJours(ArrayList<Jour> jours) {
		this.jours = jours;
	}
	
	public Calendar getHeureDebut() {
		return heureDebut;
	}
	
	public void setHeureDebut(Calendar heureDebut) {
		this.heureDebut = heureDebut;
	}
	public Calendar getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(Calendar heureFin) {
		this.heureFin = heureFin;
	}
	
	public String getHeures(){
		String retour = "";
		if(this.heureDebut.get(Calendar.MINUTE)==0)retour = this.heureDebut.get(Calendar.HOUR_OF_DAY)+"H";
		return(retour);
	}
	
	public boolean evenementInTranche(Evenement evenement){
		
		if(evenement.getHeureDebut().get(Calendar.HOUR_OF_DAY)>this.heureFin.get(Calendar.HOUR_OF_DAY)) return false;
		
		if(evenement.getHeureDebut().get(Calendar.HOUR_OF_DAY)==this.heureFin.get(Calendar.HOUR_OF_DAY) 
				&& evenement.getHeureDebut().get(Calendar.MINUTE)>=this.heureFin.get(Calendar.MINUTE))return false;
		
		if(evenement.getHeureFin().get(Calendar.HOUR_OF_DAY)<this.heureDebut.get(Calendar.HOUR_OF_DAY)) return false;
		
		if(evenement.getHeureFin().get(Calendar.HOUR_OF_DAY)==this.heureDebut.get(Calendar.HOUR_OF_DAY) 
				&& evenement.getHeureFin().get(Calendar.MINUTE)<=this.heureDebut.get(Calendar.MINUTE))return false;
		
		return true;
	}
	
}
