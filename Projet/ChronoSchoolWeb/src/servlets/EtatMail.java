package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usecases.GererMail;
import domaine.Constantes;

/**
 * Servlet implementation class EtatMail
 */
@WebServlet("/etatMails.html")
public class EtatMail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private GererMail uc;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute(Constantes.ATT_LISTE, this.uc.listerDate());
		this.getServletContext().getNamedDispatcher(Constantes.IHM_ETATS_MAILS).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
