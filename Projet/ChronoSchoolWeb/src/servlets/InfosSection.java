package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.SecurityParser;
import usecases.GererAnnee;
import usecases.GererUtilisateur;
import domaine.Annee;
import domaine.Constantes;
import domaine.Role;
import domaine.Utilisateur;

@WebServlet("/infosSection.html")
public class InfosSection extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	GererAnnee gestionnaire;
	@EJB
	GererUtilisateur uc;
	SecurityParser securityParser = new SecurityParser();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Annee annee = new Annee();
		
		if(request.getParameter("idAnnee") != null)
			annee = gestionnaire.rechercherAnnee(Integer.parseInt(request.getParameter("idAnnee")));
		
		if (annee == null) {
			HttpSession session = request.getSession(true);
			synchronized (session) {
				session.setAttribute("wrongId", "Cette section n'existe pas");
				response.sendRedirect(response.encodeRedirectURL("listeSection.html"));
				return;
			}			
		} else {
			List<Utilisateur> liste = this.uc.listerTousEtudiant();
			request.setAttribute(Constantes.ATT_LISTE, liste);
			Collection<Role> roles = securityParser.getRolesSecurite(getServletContext().getResourceAsStream("/WEB-INF/security.xml"));
			request.setAttribute("roles", roles);
			request.setAttribute("annee", annee);
		}
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_SECTION).forward(request, response);	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		Annee annee = null;
		int[] res = null;

		synchronized (session) {
			String section, numClasseStr;
			int idAnnee, numClasse;
			List<Utilisateur> etudiants = new ArrayList<Utilisateur>();
			
			if(request.getParameter("idAnnee") != null)
			{
				idAnnee = Integer.parseInt(request.getParameter("idAnnee"));
				annee = gestionnaire.rechercherAnnee(idAnnee);
			}
			else
			{
				annee = new Annee();
			}
			section = request.getParameter("section");
			if (section.equals("")) {
				creerErreur(request, response, "Veuillez renseigner la section", annee);
				return;
			}
			annee.setSection(section);
			numClasseStr = request.getParameter("annee");
			try{
				numClasse = Integer.parseInt(numClasseStr);
			}catch(Exception e){
				creerErreur(request, response, "L'année de la section ne peut pas contenir de lettre", annee);
				return;
			}
			if(numClasse<=0){
				creerErreur(request, response, "L'année de la section doit être strictement positive", annee);
				return;
			}
			numClasse = Integer.parseInt(numClasseStr);
			annee.setNumClasse(numClasse);
			
			if(request.getParameterValues("utilisateur") != null)
			{
				String[] resStr = request.getParameterValues("utilisateur");
				res = new int[resStr.length];
	
				for(int i = 0; i < resStr.length; i++){
					res[i] = Integer.parseInt(resStr[i]);
					Utilisateur user = uc.chercherUtilisateur(res[i]);
					user.setAnnee(annee);
					etudiants.add(user);
				}
			}
			annee.setEtudiants(etudiants);

			if(request.getParameter("idAnnee") != null)
			{
				if (gestionnaire.mettreAJourAnnee(annee) == null)
				{
					creerErreur(request, response, "Une erreur est survenue lors de la modification, veuillez réessayer", annee);
					return;
				}
				else
				{
					session.setAttribute("msgModif", "Modification effectuée avec succès");
					if (((Utilisateur)session.getAttribute(Constantes.ATT_SESSION_USER)).getStatus() == 'A') {
						response.sendRedirect(response.encodeRedirectURL("listeSection.html"));
						return;
					} else {
						response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
						return;
						}
				}
			}
			else
			{
				if (gestionnaire.enregistrerAnnee(annee) == null)
				{
					creerErreur(request, response, "Une erreur est survenue lors de l'ajout, veuillez réessayer", annee);
					return;
				}
				else
				{
					session.setAttribute("msgModif", "Ajout effectué avec succès");
					if (((Utilisateur)session.getAttribute(Constantes.ATT_SESSION_USER)).getStatus() == 'A') {
						response.sendRedirect(response.encodeRedirectURL("listeSection.html"));
						return;
					} else {
						response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
						return;
					}
				}
			}
		}
	}
	
	private void creerErreur(HttpServletRequest request, HttpServletResponse response, String message, Annee annee) throws ServletException, IOException {
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		List<Utilisateur> liste = this.uc.listerTousEtudiant();
		request.setAttribute(Constantes.ATT_LISTE, liste);
		if (annee != null) {
			request.setAttribute("annee", annee);
			Collection<Role> roles = securityParser.getRolesSecurite(getServletContext().getResourceAsStream("/WEB-INF/security.xml"));
			request.setAttribute("roles", roles);
		}
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_SECTION).forward(request, response);
		return;
	}
}
