package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererEquipement;
import usecases.GererLocal;
import domaine.Constantes;
import domaine.Equipement;
import domaine.Local;

/**
 * Servlet implementation class infoLocal
 */
@WebServlet("/infosLocal.html")
public class InfoLocal extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB
	GererLocal gestionnaire;
	@EJB
	GererEquipement gestEquipement;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idLocal = request.getParameter(Constantes.PARAM_IDLOCAL);
		// On initialise un local vide afin qu'il ne soit jamais null
		Local local = new Local();
		request.setAttribute("local", local);
		// On récupère la liste des equipements disponibles
		List<Equipement> equipements = gestEquipement.listerEquipement();
		request.setAttribute("listeEquipements", equipements);
		
		local = (Local) request.getAttribute("local");
		if (local != null) {
			//de cette façon, on récupère les données déjà entrées et correctes
			request.setAttribute("local", local);
		}
		
		// Si aucun id n'est recu en parametre, il s'agit d'une modification
		if (idLocal != null && idLocal != "" || local != null && local.getIdLocal() != 0) {
			// On tente de transformer l'ID
			try {
				int id = Integer.parseInt(idLocal);
				local = gestionnaire.rechercher(id);
				local = gestionnaire.chargerTout(local);
				request.setAttribute("local", local);
			} catch (Exception e) {
				// En cas d'échec, passage en mode ajout
				request.setAttribute(Constantes.ATT_MESSAGE, "ID incorrect, passage en mode ajout.");
				request.setAttribute("ajout", "ajout");
			}
		} else {
			// Sinon, il s'agit d'un ajout
			// On verifie s'il ne s'agit pas d'un retour suite à une erreur
			// afin de récupérer les attributs
			
			request.setAttribute("ajout", "ajout");
		}
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_LOCAL).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String idLocal = request.getParameter(Constantes.PARAM_IDLOCAL);
		HttpSession session = request.getSession(true);
		synchronized (session) {

			// Ajout d'un local
			if (idLocal == null) {
				Local local = new Local();
				String intitule, nbPlacesStr;

				intitule = request.getParameter("intitule");
				if (intitule == "") {
					creerErreur(request, response, "Veuillez renseigner l'intitulé du local.", null);
					return;
				}

				local.setIntitule(intitule);

				nbPlacesStr = request.getParameter("nbPlaces");
				if (nbPlacesStr == "") {
					creerErreur(request, response, "Veuillez renseigner le nombre de places du local", local);
					return;
				}
				try {
					int nbPlaces = Integer.parseInt(nbPlacesStr);
					if (nbPlaces <= 0) {
						creerErreur(request, response, "Le nombre de places est invalide", local);
						return;
					}
					local.setNbPlaces(nbPlaces);

					String[] equipements = request.getParameterValues("equipements");
					List<Equipement> listeEquipements = new ArrayList<Equipement>();
					
					if (equipements != null) {
						try {
							for (String e : equipements) {
								int idEq = Integer.parseInt(e);
								Equipement eq = new Equipement();
								eq.setIdEquipement(idEq);
								eq = gestEquipement.rechercherEquipement(eq);
								if (eq != null) {
									listeEquipements.add(eq);
								}
							}

							local.setEquipements(listeEquipements);

						} catch (NumberFormatException e) {
							creerErreur(request, response, "id d'équipement invalide", local);
							return;
						}
					}

					if (gestionnaire.enregistrer(local) == null) {
						creerErreur(request, response, "Une erreur est survenue lors de l'ajout du local.", local);
						return;
					} else {
						session.setAttribute("msgModif", "Ajout effectué avec succès");
						response.sendRedirect(response.encodeRedirectURL("listeLocaux.html"));
						return;
					}
				} catch (EJBTransactionRolledbackException e) {
					creerErreur(request, response, "Une erreur est survenue lors de l'ajout du local", local);
					return;
				} catch (NumberFormatException e) {
					creerErreur(request, response, "Le nombre de places est invalide", local);
					return;
				}

			} else {
				// Modification d'un local
				try {
					int id = Integer.parseInt(idLocal);
					Local local = gestionnaire.rechercher(id);
					String intitule, nbPlacesStr;

					intitule = request.getParameter("intitule");
					if (intitule == "") {
						creerErreur(request, response, "Veuillez renseigner l'intitulé du local.", local);
						return;
					}

					local.setIntitule(intitule);

					nbPlacesStr = request.getParameter("nbPlaces");
					if (nbPlacesStr == "") {
						creerErreur(request, response, "Veuillez renseigner le nombre de places du local", local);
						return;
					}
					try {
						int nbPlaces = Integer.parseInt(nbPlacesStr);
						if (nbPlaces <= 0) {
							creerErreur(request, response, "Le nombre de places est invalide", local);
							return;
						}
						local.setNbPlaces(nbPlaces);

						String[] equipements = request.getParameterValues("equipements");
						List<Equipement> listeEquipements = new ArrayList<Equipement>();
						
						if (equipements != null) {
							try {
								for (String e : equipements) {
									int idEq = Integer.parseInt(e);
									Equipement eq = new Equipement();
									eq.setIdEquipement(idEq);
									eq = gestEquipement.rechercherEquipement(eq);
									if (eq != null) {
										listeEquipements.add(eq);
									}
								}

								local.setEquipements(listeEquipements);

							} catch (NumberFormatException e) {
								creerErreur(request, response, "id d'équipement invalide", local);
								return;
							}
						}


						if (gestionnaire.mettreAJour(local) == null) {
							creerErreur(request, response, "Une erreur est survenue lors de la modification du local.",
									local);
							return;
						} else {
							session.setAttribute("msgModif", "La modification s'est effectuée avec succès");
							response.sendRedirect(response.encodeRedirectURL("listeLocaux.html"));
							return;
						}
					} catch (EJBTransactionRolledbackException e) {
						creerErreur(request, response, "Une erreur est survenue lors de l'ajout du local", local);
						return;
					} catch (NumberFormatException e) {
						creerErreur(request, response, "Le nombre de places est invalide", local);
						return;
					}

				} catch (Exception e) {
					creerErreur(request, response, "Une erreur est survenue lors de la modification du local.", null);
					return;
				}
			}
		}
	}

	private void creerErreur(HttpServletRequest request, HttpServletResponse response, String message, Local local)
			throws ServletException, IOException {
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		List<Equipement> equipements = gestEquipement.listerEquipement();
		request.setAttribute("listeEquipements", equipements);
		if (local != null) {
			if(local.getIdLocal() != 0)
				local = gestionnaire.chargerTout(local);
			request.setAttribute("local", local);
		}
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_LOCAL).forward(request, response);
		return;
	}

}
