package servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usecases.GererCalendar;
import usecases.GererHoraire;
import domaine.Constantes;
import domaine.Horaire;


@WebServlet("/listeHoraires.html")
public class ListeHoraires extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	GererHoraire uc;
	
	@EJB
	GererCalendar ucCalendar;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		InfosHoraire.nettoyerSession(request);
		
		List<Horaire> liste = this.uc.listerHorairesActifs();
		
		request.setAttribute(Constantes.ATT_LISTE, liste);
		this.getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_HORAIRES).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id;
		Horaire db = null; 
		String idString = request.getParameter("supprimer");
		if(idString!=null && !idString.isEmpty()){
			try{
				id=Integer.parseInt(idString);
				Horaire horaire = new Horaire();
				horaire.setIdHoraire(id);
				
				
				this.uc.supprimerHoraireLogique(horaire);
			}
			catch(Exception e){
				this.afficherErreur("Id incorrect", request, response);
			}
		}
		this.doGet(request, response);
	}
	
	private void afficherErreur(String message, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		request.setAttribute(Constantes.ATT_OPERATION, false);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		request.getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_HORAIRES).forward(request, response);
	}

}
