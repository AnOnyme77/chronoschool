package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.ParserDate;
import usecases.GererAnnee;
import usecases.GererCalendar;
import usecases.GererEvenement;
import usecases.GererHoraire;
import domaine.Annee;
import domaine.Constantes;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Semaine;

@WebServlet("/infosHoraire.html")
public class InfosHoraire extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int ID_AJOUT_EVENEMENT = 1;
	private static final int ID_ENREGISTRER = 2;
	private static final int ID_SUPPRIMER_EVENEMENT = 3;
	private static final int ERREUR = 0;

	@EJB
	private GererAnnee ucAnnee;

	@EJB
	private GererHoraire ucHoraire;

	@EJB
	private GererEvenement ucEvenement;
	
	@EJB
	private GererCalendar ucCalendar;

	private boolean reponseEnvoyee = false;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.reponseEnvoyee = false;

		Horaire horaire;
		horaire = this.preparerHoraire(request);

		Semaine semaine = new Semaine();
		semaine.ajouterListeEvenement(horaire.getEvenements());
		request.setAttribute("semaine", semaine);

		List<Annee> annees = this.ucAnnee.listerAnnee();
		request.setAttribute("liste", annees);

		request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_HORAIRE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		Horaire horaire = null;
		Horaire db = null;
		switch (this.casPost(request)) {
		case ID_AJOUT_EVENEMENT:
			horaire = this.getHoraireRequest(request);
			this.updateHoraireSession(horaire, request);
			response.sendRedirect(response.encodeRedirectURL("infosEvenement.html"));
			break;

		case ID_ENREGISTRER:
			horaire = this.getHoraireRequest(request);
			this.verifierHoraire(horaire, request, response);
			if (!this.reponseEnvoyee) {
				this.updateHoraireSession(horaire, request);

				horaire = InfosHoraire.getHoraireSession(request);
				if (horaire.getIdHoraire() == 0) {
					db = this.ucHoraire.ajouterHoraire(horaire);
					if (db == null) {
						afficherErreur(ucHoraire.getMessage(), request, response);
						return;
					}
					
				} else {
					db = this.ucHoraire.modifierHoraire(horaire, false);
					if (db == null) {
						afficherErreur(ucHoraire.getMessage(), request, response);
						return;
					}
				}

				InfosHoraire.nettoyerSession(request);
				response.sendRedirect(response.encodeURL("listeHoraires.html"));
			}else{ 
				this.updateHoraireSession(horaire, request);
				horaire = InfosHoraire.getHoraireSession(request);
				if (horaire.getIdHoraire() == 0) {
					db = this.ucHoraire.ajouterHoraire(horaire);
					if (db == null) {
						afficherErreur(ucHoraire.getMessage(), request, response);
						return;
					}
					
				} else {
					db = this.ucHoraire.modifierHoraire(horaire, false);
					if (db == null) {
						afficherErreur(ucHoraire.getMessage(), request, response);
						return;
					}
				}

				InfosHoraire.nettoyerSession(request);
				response.sendRedirect(response.encodeURL("listeHoraires.html"));
			}
			break;

		case ID_SUPPRIMER_EVENEMENT:
			horaire = InfosHoraire.getHoraireSession(request);
			Evenement evenement = InfosEvenement.getEvenementHoraire(request.getParameter("intit"),
					request.getParameter("desc"), request.getParameter("hDeb"), request.getParameter("hFin"), request);

			horaire.getEvenements().remove(evenement);
			InfosHoraire.ajouterHoraireSession(horaire, request);
			this.doGet(request, response);
			break;

		}
		
	}

	private void verifierHoraire(Horaire horaire, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean erreur = false;
		if (!erreur && horaire.getAnnee() == null) {
			erreur = true;
			this.afficherErreur("Veuillez sélectionner une année", request, response);
		}

		if (!erreur && horaire.getDateDebut() == null && !this.reponseEnvoyee) {
			erreur = true;
			this.afficherErreur("Veuillez remplir la date de début", request, response);
		}

		if (!erreur && horaire.getDateFin() == null && !this.reponseEnvoyee) {
			erreur = true;
			this.afficherErreur("Veuillez remplir la date de fin", request, response);
		}

		if (!erreur && horaire.getDateDebut().after(horaire.getDateFin()) && !this.reponseEnvoyee) {
			erreur = true;
			this.afficherErreur("La date de début doit être après la date de fin", request, response);
		}
	}

	/* Méthodes de fonctionnement général */
	private int casPost(HttpServletRequest request) {
		if (request.getParameter("ajouter") != null)
			return ID_AJOUT_EVENEMENT;

		if (request.getParameter("enregistrer") != null)
			return ID_ENREGISTRER;

		if (request.getParameter("supprimer") != null)
			return ID_SUPPRIMER_EVENEMENT;

		return (ERREUR);
	}

	public static void nettoyerSession(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		synchronized (session) {
			session.setAttribute("horaire", null);
		}

	}

	/* Méthodes concernant l'horaire */
	private Horaire preparerHoraire(HttpServletRequest request) {
		Horaire horaire = null;
		boolean url = false, session = false;
		String idString = request.getParameter("idHoraire");
		if (idString != null && !idString.isEmpty())
			url = true;

		horaire = InfosHoraire.getHoraireSession(request);
		if (horaire != null) {
			session = true;
		}

		if (url == true) {
			horaire = this.getHoraireDB(idString);
			List<Evenement> liste = new ArrayList<Evenement>();
			Horaire semaine = this.ucHoraire.getSemaineAnnee(horaire.getAnnee(),
					this.ucHoraire.premierLundiApresDebut(horaire.getDateDebut()));
			for (Evenement event : semaine.getEvenements()) {
				liste.add(this.ucEvenement.chargerTout(event));
			}
			horaire.setEvenements(liste);
		}

		if (!session && !url) {
			horaire = new Horaire();
		}

		InfosHoraire.ajouterHoraireSession(horaire, request);

		return horaire;
	}

	private void updateHoraireSession(Horaire horaire, HttpServletRequest request) {
		Horaire horaireSession = null;
		horaireSession = InfosHoraire.getHoraireSession(request);
		horaireSession.setDateDebut(horaire.getDateDebut());
		horaireSession.setAnnee(horaire.getAnnee());
		horaireSession.setDateFin(horaire.getDateFin());
		horaireSession.setStatut('\u0000');
		InfosHoraire.ajouterHoraireSession(horaireSession, request);
	}

	private Horaire getHoraireRequest(HttpServletRequest request) {
		Horaire horaire = new Horaire();
		ParserDate parser = new ParserDate();
		Annee annee = null;
		Calendar date = null;
		int id;

		String dateString = request.getParameter("dateDebut");
		if (dateString != null && !dateString.isEmpty()) {
			date = parser.parser(dateString);
			horaire.setDateDebut(date);
		}

		dateString = request.getParameter("dateFin");
		if (dateString != null && !dateString.isEmpty()) {
			date = parser.parser(dateString);
			horaire.setDateFin(date);
		}

		dateString = request.getParameter("annee");
		if (dateString != null && !dateString.isEmpty()) {
			try {
				id = Integer.parseInt(dateString);
				annee = this.ucAnnee.rechercherAnnee(id);
				if (annee != null)
					horaire.setAnnee(annee);
			} catch (Exception e) {

			}
		}
		return (horaire);
	}

	public static Horaire getHoraireSession(HttpServletRequest request) {
		Horaire horaire = null;
		Horaire horaireSession;
		// Si un horaire est reçu via la session
		HttpSession session = request.getSession(true);
		synchronized (session) {
			horaireSession = (Horaire) session.getAttribute("horaire");
			if (horaireSession != null) {
				horaire = horaireSession;
			}
		}
		return horaire;
	}

	public static void ajouterHoraireSession(Horaire horaire, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		synchronized (session) {
			session.setAttribute("horaire", horaire);
		}
	}

	private Horaire getHoraireDB(String idString) {
		Horaire horaire = null;

		if (idString != null) {
			try {
				int id = Integer.parseInt(idString);
				horaire = new Horaire();
				horaire.setIdHoraire(id);

				horaire = this.ucHoraire.rechercherHoraire(horaire);

				if (horaire != null)
					horaire = this.ucHoraire.chargerTout(horaire);
			} catch (Exception e) {

			}

		}
		return (horaire);
	}

	private void afficherErreur(String message, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.reponseEnvoyee = true;
		request.setAttribute(Constantes.ATT_OPERATION, false);
		request.setAttribute(Constantes.ATT_MESSAGE, message);

		List<Annee> annees = this.ucAnnee.listerAnnee();
		request.setAttribute("liste", annees);

		Semaine semaine = new Semaine();
		semaine.ajouterListeEvenement(InfosHoraire.getHoraireSession(request).getEvenements());
		request.setAttribute("semaine", semaine);

		request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_HORAIRE).forward(request, response);
	}
}
