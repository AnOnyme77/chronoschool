package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.ParserDate;
import outils.SecurityParser;
import usecases.GererUtilisateur;
import domaine.Constantes;
import domaine.Role;
import domaine.Sexe;
import domaine.Utilisateur;

/**
 * Servlet implementation class InfosUtilisateur
 */
@WebServlet("/infosUtilisateur.html")
public class InfosUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB
	GererUtilisateur gestionnaire;
	ParserDate parserDate = new ParserDate();
	SecurityParser securityParser = new SecurityParser();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idUtilisateur = Integer.parseInt(request.getParameter("idUtilisateur"));
		Utilisateur user = gestionnaire.chercherUtilisateur(idUtilisateur);
		if (user == null) {
			HttpSession session = request.getSession(true);
			synchronized (session) {
				session.setAttribute("wrongId", "Cet utilisateur n'existe pas");
				response.sendRedirect(response.encodeRedirectURL("listeUtilisateurs.html"));
				return;
			}			
		} else {
			Calendar c = user.getDateNaissance();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			String date = sdf.format(c.getTime());

			request.setAttribute("date", date);
			Collection<Role> roles = securityParser.getRolesSecurite(getServletContext().getResourceAsStream("/WEB-INF/security.xml"));
			request.setAttribute("roles", roles);
			// On passe l'utilisateur à modifier dans l'attribut objet
			request.setAttribute("utilisateur", user);
		}
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_UTILISATEUR).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		Utilisateur utilisateur = null;

		synchronized (session) {
			String id, nom, prenom, email, dateStr, statut, mdp, mdpC;
			int idUtilisateur;
			Sexe sexe;
			Calendar date = null;

			id = request.getParameter("idUser");
			idUtilisateur = Integer.parseInt(id);
			utilisateur = gestionnaire.chercherUtilisateur(idUtilisateur);
			nom = request.getParameter("nom");
			if (nom.equals("")) {
				creerErreur(request, response, "Veuillez renseigner votre nom", utilisateur);
				return;
			}
			prenom = request.getParameter("prenom");
			if (prenom.equals("")) {
				creerErreur(request, response, "Veuillez renseigner votre prénom", utilisateur);
				return;
			}
			email = request.getParameter("email");
			if (email.equals("")) {
				creerErreur(request, response, "Veuillez renseigner votre email", utilisateur);
				return;
			}
			dateStr = request.getParameter("dateNaiss");
			if (dateStr.equals("")) {
				creerErreur(request, response, "Veuillez renseigner votre date de naissance", utilisateur);
				return;
			}
			date = parserDate.parser(dateStr);

			if (date.after(GregorianCalendar.getInstance()) || date.equals(GregorianCalendar.getInstance())) {
				creerErreur(request, response, "La date entrée est incorrecte", utilisateur);
				return;
			}
			mdp = request.getParameter("motDePasse");
			mdpC = request.getParameter("motDePasseConf");
			statut = request.getParameter("role");

			if (request.getParameter("sexe").equals("M")) {
				sexe = Sexe.Homme;
			} else {
				sexe = Sexe.Femme;
			}

			// On vérifie la disponibilité de l'adresse email
			if (!email.equals(utilisateur.getEmail())) {
				// l'email a été modifié
				if (gestionnaire.existe(email)) {
					creerErreur(request, response, "Cette adresse mail n'est plus disponible", utilisateur);
					return;
				}
			}

			utilisateur.setNom(nom);
			utilisateur.setPrenom(prenom);
			utilisateur.setEmail(email);
			utilisateur.setDateNaissance(date);
			utilisateur.setSexe(sexe);

			utilisateur.setStatus(statut.toCharArray()[0]);

			// On vérifie si les mots de passe sont remplis
			if (!mdp.equals("")) {
				// On verifie si les mdp correspondent
				if (!mdp.equals(mdpC)) {
					// erreur de mots de passe
					creerErreur(request, response, "Les mots de passe ne correspondent pas", utilisateur);
					return;
				} else {
					// On set le mdp pour la modification
					utilisateur.setMdp(mdp);
				}
			}

			if (gestionnaire.modifierUtilisateur(utilisateur) == null) {
				creerErreur(request, response, "Une erreur est survenue lors de la modification, veuillez réessayer", utilisateur);
				return;
			} else {
				// L'utilisateur a été ajouté
				// Devra être redirigé vers la page de connexion et non l'index
				// (ou pas ?)
				session.setAttribute("msgModif", "Modification effectuée avec succès");
				if (((Utilisateur)session.getAttribute(Constantes.ATT_SESSION_USER)).getStatus() == 'A') {
					response.sendRedirect(response.encodeRedirectURL("listeUtilisateurs.html"));
					return;
				} else {
					response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
					return;
				}
			}
		}
	}

	private void creerErreur(HttpServletRequest request, HttpServletResponse response, String message, Utilisateur user) throws ServletException, IOException {
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		if (user != null) {
			request.setAttribute("utilisateur", user);
			Calendar c = user.getDateNaissance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			request.setAttribute("date", sdf.format(c.getTime()));
			Collection<Role> roles = securityParser.getRolesSecurite(getServletContext().getResourceAsStream("/WEB-INF/security.xml"));
			request.setAttribute("roles", roles);
		}
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_UTILISATEUR).forward(request, response);
		return;
	}
}
