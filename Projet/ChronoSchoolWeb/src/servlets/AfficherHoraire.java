package servlets;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererEvenement;
import usecases.GererHoraire;
import usecases.GererMail;
import usecases.GererUtilisateur;
import domaine.Constantes;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Semaine;
import domaine.Utilisateur;

/**
 * Servlet implementation class AfficherHoraire
 */
@WebServlet(urlPatterns = { "/afficherHoraire.html" })
public class AfficherHoraire extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private GererHoraire ucHoraire;
	@EJB
	private GererEvenement ucEvenement;
	@EJB
	private GererUtilisateur ucUser;
	@EJB
	private GererMail ucMail;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur user;
		GregorianCalendar date = null;
		Semaine semaine = new Semaine();
		Horaire horaire=null;

		
		date = getDate(request, response);
		
		user = this.getUserSession(request, response);
		
		if(user.getStatus()=='A'){
			response.sendRedirect(response.encodeRedirectURL("listeHoraires.html"));
			return;
		}
		
		if(user.getStatus()=='P'){
			horaire = this.ucHoraire.getSemaineProf(user, date);
			if(horaire!=null){
				semaine.ajouterListeEvenement(horaire.getEvenements());
			}
			else{
				this.preparerErreur("Horaire inconnu", request, response);
			}
		}
		
		if(user.getStatus()=='U'){
			user = this.ucUser.chargerTout(user);
			
			horaire = this.ucHoraire.getSemaineAnnee(user.getAnnee(), date);
			
			if(horaire!=null){
				semaine.ajouterListeEvenement(horaire.getEvenements());
			}
			else{
				this.preparerErreur("Horaire inconnu", request, response);
			}
			
		}  
		GregorianCalendar dateDebut = (GregorianCalendar)date.clone();
		request.setAttribute("dateDebut", dateDebut);
		
		GregorianCalendar dateFin = (GregorianCalendar)date.clone();
		dateFin.add(Calendar.DAY_OF_MONTH, 6);
		request.setAttribute("dateFin", dateFin);
		
		GregorianCalendar datePrecedente = (GregorianCalendar) date.clone();
		datePrecedente.add(Calendar.DAY_OF_MONTH, -7);
		request.setAttribute("datePrecedente", datePrecedente);
		
		GregorianCalendar dateSuivante = (GregorianCalendar)date.clone();
		dateSuivante.add(Calendar.DAY_OF_MONTH, 7);
		request.setAttribute("dateSuivante", dateSuivante);
		
		user = ucUser.chargerTout(user);
		
		request.setAttribute("annee", user.getAnnee());
		request.setAttribute("horaire", horaire);
		request.setAttribute("semaine", semaine);
		request.setAttribute("user", user);
		this.getServletContext().getNamedDispatcher(Constantes.IHM_AFFICHER_HORAIRE).forward(request, response);
	}

	private GregorianCalendar getDate(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		GregorianCalendar date;
		String dateString;
		int jour;
		int mois;
		int annee;
		if(request.getParameter("date")==null)
			date = this.premierJourSemaine(new GregorianCalendar());
		else{
			dateString=request.getParameter("date");
			String [] parties = dateString.split("-");
			if(parties.length!=3){
				this.preparerErreur("Date incorrecte", request, response);
				date = this.premierJourSemaine(new GregorianCalendar());
			}
			else{
				try{
					jour = Integer.parseInt(parties[0]);
					mois = Integer.parseInt(parties[1])-1;
					annee = Integer.parseInt(parties[2]);
					date=new GregorianCalendar(annee,mois,jour);
				}
				catch(Exception e){
					this.preparerErreur("Date incorrecte", request, response);
					date = this.premierJourSemaine(new GregorianCalendar());
				}
			}
		}
		return date;
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(this.isDonnerEvent(request)){
			this.donnerEvenement(request,response);
		}
		
		if(this.isAnnulerEvent(request)){
			this.annulerEvenement(request,response);
		}
		
		this.doGet(request, response);
	}
	
	private boolean isAnnulerEvent(HttpServletRequest request){
		return(request.getParameter("annuler")!=null);
	}
	
	private boolean isDonnerEvent(HttpServletRequest request){
		return(request.getParameter("donner")!=null);
	}
	
	
	private void annulerEvenement(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Evenement event = new Evenement();
		String idString;
		int id;
		List<Utilisateur> destinataire;
		
		idString = request.getParameter("annuler");
		id = Integer.parseInt(idString);
		try{
			event = ucEvenement.rechercherEvenement(id);
			event.setStatut('S');
			event = ucEvenement.mettreAJourEvenement(event);
			destinataire = event.getCours().getAnnee().getEtudiants();
			ucMail.envoyer("Cours annulé", "Le cours de "+event.getIntitule()+" du " + event.getHeureDebut().get(Calendar.DATE) + "/" + event.getHeureDebut().get(Calendar.MONTH)+1 + "/" + event.getHeureDebut().get(Calendar.YEAR) +" à "+ event.getHeureDebut().get(Calendar.HOUR)+"h"+ event.getHeureDebut().get(Calendar.MINUTE)+" à été annulé pour cause d'absence de votre professeur.", destinataire);
		}
		catch(Exception e){
			this.preparerErreur("Id incorrect", request, response);
		}
	}

	private void donnerEvenement(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Evenement event = new Evenement();
		String idString;
		int id;
		List<Utilisateur> destinataire;
		
		idString = request.getParameter("donner");
		id = Integer.parseInt(idString);
		
		try{
			event = ucEvenement.rechercherEvenement(id);
			event.setStatut('A');
			event = ucEvenement.mettreAJourEvenement(event);
			destinataire = event.getCours().getAnnee().getEtudiants();
			ucMail.envoyer("Cours annulé", "Le cours de "+event.getIntitule()+" du " + event.getHeureDebut().get(Calendar.DATE) + "/" + event.getHeureDebut().get(Calendar.MONTH)+1 + "/" + event.getHeureDebut().get(Calendar.YEAR) +" à "+ event.getHeureDebut().get(Calendar.HOUR)+"h"+ event.getHeureDebut().get(Calendar.MINUTE)+" à été reprogrammé par votre professeur. Le cours se donnera donc comme prévu par l'horaire.", destinataire);
		}
		catch(Exception e){
			this.preparerErreur("Id incorrect", request, response);
		}
	}
	
	private GregorianCalendar premierJourSemaine(GregorianCalendar cal){
		switch(cal.get(Calendar.DAY_OF_WEEK)){
			case Calendar.TUESDAY:
				cal.add(Calendar.DAY_OF_MONTH, -1);
				break;
			case Calendar.WEDNESDAY:
				cal.add(Calendar.DAY_OF_MONTH, -2);
				break;
			case Calendar.THURSDAY:
				cal.add(Calendar.DAY_OF_MONTH, -3);
				break;
			case Calendar.FRIDAY:
				cal.add(Calendar.DAY_OF_MONTH, -4);
				break;
			case Calendar.SATURDAY:
				cal.add(Calendar.DAY_OF_MONTH, -5);
				break;
			case Calendar.SUNDAY:
				cal.add(Calendar.DAY_OF_MONTH, -6);
				break;
		}
		return cal;
	}

	private Utilisateur getUserSession(HttpServletRequest request, HttpServletResponse response){
		Utilisateur user = null;
		HttpSession session = request.getSession(true);
		synchronized (session) {
			user = (Utilisateur)session.getAttribute(Constantes.ATT_SESSION_USER);
		}
		return user;
	}

	private void preparerErreur(String message, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		request.setAttribute(Constantes.ATT_OPERATION, false);
		request.setAttribute(Constantes.ATT_MESSAGE, message);
	}

}
