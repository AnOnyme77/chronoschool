package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererUtilisateur;
import domaine.Constantes;
import domaine.Utilisateur;

/**
 * Servlet implementation class TemplatePage
 */
@WebServlet("/index.html")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	GererUtilisateur gererUtilisateur;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		synchronized (session) {
			Utilisateur user = (Utilisateur) session.getAttribute(Constantes.ATT_SESSION_USER);
			if (user == null)
				this.getServletContext().getNamedDispatcher(Constantes.IHM_INDEX).forward(request, response);
			else
				if(user.getStatus()=='A')
					response.sendRedirect(response.encodeRedirectURL("listeHoraires.html"));
				else
					response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter(Constantes.PARAM_EMAIL);
		String mdp = request.getParameter(Constantes.PARAM_MDP);
		
		if( email==null || mdp==null)
		{
			request.setAttribute(Constantes.ATT_MESSAGE, "Veuillez entrez un mot de passe et un email valide");
			getServletContext().getNamedDispatcher(Constantes.IHM_INDEX).forward(request, response);
		}
		else
		{
			Utilisateur user = this.gererUtilisateur.connecterUtilisateur(email, mdp);
			
			if(user==null)
			{
				request.setAttribute(Constantes.ATT_MESSAGE, "Identifiant ou mot de passe incorrect");
				getServletContext().getNamedDispatcher(Constantes.IHM_INDEX).forward(request, response);
			}
			else
			{
				HttpSession session = request.getSession(true);
				synchronized (session) {
					session.setAttribute(Constantes.ATT_SESSION_USER, user);
					if(user.getStatus()=='A')
						response.sendRedirect(response.encodeRedirectURL("listeHoraires.html"));
					else
						response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
				}
			}
		}
	}

}
