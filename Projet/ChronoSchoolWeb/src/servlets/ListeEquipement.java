package servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererEquipement;
import domaine.Constantes;
import domaine.Equipement;

/**
 * Servlet implementation class ListeEquipement
 */
@WebServlet("/listeEquipement.html")
public class ListeEquipement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	GererEquipement uc;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Equipement> liste = this.uc.listerEquipement();
		String messageSession=null;
		boolean reussi=true;
		HttpSession session = request.getSession(true);
		synchronized (session) {
			messageSession = (String)session.getAttribute(Constantes.ATT_MESSAGE);
			session.setAttribute(Constantes.ATT_MESSAGE, null);
			session.setAttribute(Constantes.ATT_OPERATION, null);
			
		}
		
		
		if(messageSession!=null && !messageSession.isEmpty()){
			request.setAttribute(Constantes.ATT_MESSAGE, messageSession);
		}
		request.setAttribute(Constantes.ATT_OPERATION, reussi);
		request.setAttribute(Constantes.ATT_LISTE, liste);
		request.getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_EQUIPEMENT).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idString = request.getParameter("supprimer");
		
		if(idString==null || idString.isEmpty()){
			request.setAttribute(Constantes.ATT_MESSAGE, "Opération incorrecte");
		}
		else{
			try{
				int id=Integer.parseInt(idString);
				Equipement equipement = new Equipement();
				equipement.setIdEquipement(id);
				equipement = this.uc.supprimerEquipement(equipement);
				if(equipement != null)request.setAttribute(Constantes.ATT_MESSAGE, "Opération effectuée");
				else{
					request.setAttribute(Constantes.ATT_OPERATION, false);
					request.setAttribute(Constantes.ATT_MESSAGE, "Opération échouée");
				}
			}
			catch(NumberFormatException e){
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Opération incorrecte");
			}
			catch(Exception e){
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Opération impossible : un local utilise cet équipement");
			}
		}
		this.doGet(request, response);
	}

}
