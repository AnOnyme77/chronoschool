package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usecases.GererLocal;
import domaine.Constantes;
import domaine.Local;

/**
 * Servlet implementation class SListeLocaux
 */
@WebServlet("/listeLocaux.html")
public class ListeLocaux extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB
	GererLocal gestionnaire;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Local> listeLocaux = gestionnaire.lister();
		ArrayList<Local>complet = new ArrayList<Local>();
		for(Local l:listeLocaux){
			l = this.gestionnaire.chargerTout(l);
			complet.add(l);
		}
		request.setAttribute("listeLocaux", complet);
		getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_LOCAUX).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String idLocal = request.getParameter("idLocal");
		try {
			int id = Integer.parseInt(idLocal);
			gestionnaire.supprimer(id);
			if (gestionnaire.rechercher(id) == null)
				request.setAttribute(Constantes.ATT_OPERATION, true);
				request.setAttribute("message", "Suppression effectuée avec succès");
		} catch (Exception e) {
			request.setAttribute(Constantes.ATT_OPERATION, false);
			request.setAttribute("message", "Echec de la suppression");
		}
		doGet(request, response);
	}
}
