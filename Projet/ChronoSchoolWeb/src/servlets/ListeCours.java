package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererCours;
import domaine.Constantes;
import domaine.Cours;
import domaine.Utilisateur;

/**
 * Servlet implementation class TemplatePage
 */
@WebServlet("/listeCours.html")
public class ListeCours extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	GererCours gererCours;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		synchronized (session) {
			Utilisateur user = (Utilisateur) session.getAttribute(Constantes.ATT_SESSION_USER);
			// On ne verifie pas si l'utilisateur est null car ceci est deja
			// testé par le filtre
			List<Cours> listeCours = new ArrayList<Cours>();
			switch (user.getStatus()) {
			case 'U':
				if (user.getAnnee() != null) {
					int idAnnee = user.getAnnee().getIdAnnee();
					listeCours = gererCours.listerCoursParAnnee(idAnnee);
				}
				break;
			case 'P':
				listeCours = gererCours.listerCoursParProfesseur(user.getIdUtilisateur());
				break;
			case 'A':
				listeCours = gererCours.listerTousCours();
				break;
			}
			
			ArrayList<Cours> complet = new ArrayList<>();
			for(Cours c:listeCours){
				c = this.gererCours.chargerTout(c);
				complet.add(c);
			}
			
			request.setAttribute(Constantes.ATT_LISTE, complet);
		}
		this.getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_COURS).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String idCours = request.getParameter(Constantes.PARAM_SUPPRIMER);
		if (idCours != null) {
			Cours cours = new Cours();
			cours.setIdCours(Integer.parseInt(idCours));
			gererCours.supprimerCours(cours);
			if(gererCours.getMessage().contains("suppression effectuée")){
				request.setAttribute(Constantes.ATT_OPERATION, true);
				request.setAttribute(Constantes.ATT_MESSAGE, "Suppression effectuée avec succès.");
			}else{
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Échec de la suppression.");
			}
		}

		this.doGet(request, response);
	}

}
