package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.ParserDateHeure;
import outils.ParserHeure;
import usecases.GererCours;
import usecases.GererDisponibilite;
import usecases.GererEvenement;
import usecases.GererLocal;
import domaine.Constantes;
import domaine.Cours;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Local;

/**
 * Servlet implementation class InfosEvenement
 */
@WebServlet("/infosEvenement.html")
public class InfosEvenement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final int AFFICHER_MODIF = 1;
	private static final int ENREG_MODIF = 2;
	private static final int ENREG_NOUVEAU = 3;

	@EJB
	private GererLocal ucLocal;

	@EJB
	private GererCours ucCours;

	@EJB
	private GererEvenement ucEvenement;

	@EJB
	private GererDisponibilite ucDisponibilite;

	private boolean reponseEnvoyee = false;

	private String message = "";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.reponseEnvoyee = false;

		// Chargement des locaux
		List<Local> listeLocaux = getListeLocaux();

		List<Cours> listeCoursEquip = getListeCours(request);

		String[] jours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche" };

		request.setAttribute("jours", jours);
		request.setAttribute("listeCours", listeCoursEquip);
		request.setAttribute(Constantes.ATT_LISTE, listeLocaux);
		if (!this.reponseEnvoyee)
			request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_EVENEMENT).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.reponseEnvoyee = false;

		String intit = request.getParameter("intit");
		String desc = request.getParameter("desc");
		String hDeb = request.getParameter("hDeb");
		String hFin = request.getParameter("hFin");

		Evenement event = null;
		Evenement ancien = null;
		Horaire horaire = null;
		switch (this.getCasPost(request)) {

		case ENREG_NOUVEAU:
			event = this.getEvenementRequest(request, response);
			if (!reponseEnvoyee) {
				if (chevaucheTemporel(event, request, response)) {
					afficherErreur((message.isEmpty() ? "Deux événements ne peuvent avoir lieu au même moment au sein d'un même horaire." : message), event,
							request, response);
					return;
				}
				if (verifierDisponibilite(event, request, response)) {
					horaire = InfosHoraire.getHoraireSession(request);
					horaire.getEvenements().add(event);
					InfosHoraire.ajouterHoraireSession(horaire, request);
					if (!this.reponseEnvoyee)
						response.sendRedirect(response.encodeURL("infosHoraire.html"));
				}
			}else{
				return;
			}
			break;

		case AFFICHER_MODIF:
			event = InfosEvenement.getEvenementHoraire(intit, desc, hDeb, hFin, request);
			request.setAttribute(Constantes.ATT_OBJET, event);
			doGet(request, response);
			break;
		case ENREG_MODIF:
			horaire = InfosHoraire.getHoraireSession(request);
			event = this.getEvenementRequest(request, response);
			ancien = InfosEvenement.getEvenementHoraire(intit, desc, hDeb, hFin, request);
			if (chevaucheTemporel(event, request, response)) {
				afficherErreur("Deux événements ne peuvent avoir lieu au même moment au sein d'un même horaire.", ancien, request, response);
				return;
			}

			int indice = getIndiceEvent(intit, desc, hDeb, hFin, request);
			if (indice == -1) {
				afficherErreur("Indice invalide", event, request, response);
				return;
			}

			if (ancien.getIdEvenement() != 0)
				ancien = ucEvenement.chargerTout(ancien);
			ancien.setCours(event.getCours());
			ancien.setDescription(event.getDescription());
			ancien.setHeureDebut(event.getHeureDebut());
			ancien.setHeureFin(event.getHeureFin());
			ancien.setIntitule(event.getIntitule());
			ancien.setLocaux(event.getLocaux());
			horaire.getEvenements().set(indice, ancien);

			InfosHoraire.ajouterHoraireSession(horaire, request);
			if (!this.reponseEnvoyee)
				response.sendRedirect(response.encodeURL("infosHoraire.html"));
			break;
		}

	}

	private boolean verifierDisponibilite(Evenement evenement, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean ok = true;
		String message = "";

		if (!this.ucDisponibilite.disponibiliteAnnee(evenement)) {
			ok = false;
			message += "Les élèves ne sont pas disponibles à cette heure <br>";
		}

		if (!this.ucDisponibilite.disponibiliteLocal(evenement)) {
			ok = false;
			message += "Ce local n'est pas disponible à cette heure <br>";
		}

		if (!this.ucDisponibilite.disponibiliteProfesseur(evenement)) {
			ok = false;
			message += "Ce professeur n'est pas disponible à cette heure <br>";
		}

		if (evenement.getCours() != null) {
			message += "Les locaux suivants ne disposent pas du bon matériel: <br>";
			for (Local local : evenement.getLocaux()) {
				if (!this.ucDisponibilite.equipementLocal(local, evenement.getCours())) {
					ok = false;
					message += "- " + local.getIntitule() + "<br>";
				}
			}
		}
		if (!this.reponseEnvoyee && !ok) {
			this.afficherErreur(message, evenement, request, response);
		}
		return ok;
	}

	private int getCasPost(HttpServletRequest request) {
		boolean valeurs = false;
		String intitule = request.getParameter("interne");
		String intit = request.getParameter("intit");
		String desc = request.getParameter("desc");
		String hDeb = request.getParameter("hDeb");
		String hFin = request.getParameter("hFin");

		if (desc == null)
			desc = "Non définie";

		if (intit != null && !intit.isEmpty() && desc != null && !desc.isEmpty() && hDeb != null && !hDeb.isEmpty() && hFin != null && !hFin.isEmpty())
			valeurs = true;

		if (valeurs && intitule == null)
			return AFFICHER_MODIF;

		if (valeurs && intitule != null && !intitule.isEmpty() && getIndiceEvent(intit, desc, hDeb, hFin, request) != -1)
			return ENREG_MODIF;

		if (!valeurs && intitule == null || valeurs && getIndiceEvent(intit, desc, hDeb, hFin, request) == -1)
			return ENREG_NOUVEAU;
		
		if(!valeurs && intitule != null )
			return ENREG_NOUVEAU;
		return 0;
	}

	private Evenement getHiddenEvent(HttpServletRequest request) {
		ParserDateHeure pdh = new ParserDateHeure();

		String intit = request.getParameter("intit");
		String desc = request.getParameter("desc");
		String hDeb = request.getParameter("hDeb");
		String hFin = request.getParameter("hFin");

		Evenement evenement = new Evenement();
		evenement.setIntitule(intit);
		evenement.setDescription(desc);
		evenement.setHeureDebut(pdh.parser(hDeb));
		evenement.setHeureFin(pdh.parser(hFin));

		return evenement;
	}

	private List<Cours> getListeCours(HttpServletRequest request) {
		Horaire horaire = InfosHoraire.getHoraireSession(request);

		// Récupération de la liste des cours et de leurs équipements
		List<Cours> listeCours;
		List<Cours> listeCoursEquip = new ArrayList<Cours>();

		if (horaire != null && horaire.getAnnee() != null) {
			listeCours = this.ucCours.listerCoursParAnnee(horaire.getAnnee().getIdAnnee());
		} else {
			listeCours = this.ucCours.listerTousCours();
		}

		for (Cours cours : listeCours) {
			listeCoursEquip.add(this.ucCours.chargerTout(cours));
		}
		return listeCoursEquip;
	}

	private List<Local> getListeLocaux() {
		List<Local> liste = this.ucLocal.lister();
		List<Local> listeLocaux = new ArrayList<Local>();
		for (Local local : liste) {
			listeLocaux.add(this.ucLocal.chargerTout(local));
		}
		return listeLocaux;
	}

	public Evenement getEvenementRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Evenement evenement = new Evenement();
		int id;
		ParserHeure parser = new ParserHeure();

		String intitule = request.getParameter("intitule");
		if ((intitule == null || intitule.isEmpty()) && !this.reponseEnvoyee)
			this.afficherErreur("Veuillez remplir le champs Intitulé", evenement, request, response);
		evenement.setIntitule(intitule);

		String description = request.getParameter("description");
		if (description == null || description.isEmpty())
			description = "Non définie";
		evenement.setDescription(description);

		String jour = request.getParameter("jour");
		int numJour;
		switch (jour) {
		case "lundi":
			numJour = Calendar.MONDAY;
			break;
		case "mardi":
			numJour = Calendar.TUESDAY;
			break;
		case "mercredi":
			numJour = Calendar.WEDNESDAY;
			break;
		case "jeudi":
			numJour = Calendar.THURSDAY;
			break;
		case "vendredi":
			numJour = Calendar.FRIDAY;
			break;
		case "samedi":
			numJour = Calendar.SATURDAY;
			break;
		case "dimanche":
			numJour = Calendar.SUNDAY;
			break;
		default:
			numJour = Calendar.MONDAY;
			break;
		}

		String heureDebut = request.getParameter("heureDebut");
		if ((heureDebut == null || heureDebut.isEmpty()) && !this.reponseEnvoyee) {
			this.afficherErreur("Veuillez remplir l'heure de début", evenement, request, response);
		}
		Calendar debut = parser.parser(heureDebut);
		if (debut == null && !this.reponseEnvoyee)
			this.afficherErreur("Heure de début incorrecte", evenement, request, response);
		if (debut != null)
			debut.set(Calendar.DAY_OF_WEEK, numJour);
		evenement.setHeureDebut(debut);

		String heureFin = request.getParameter("heureFin");
		if ((heureFin == null || heureFin.isEmpty()) && !this.reponseEnvoyee)
			this.afficherErreur("Veuillez remplir l'heure de fin", evenement, request, response);
		Calendar fin = parser.parser(heureFin);
		if (fin == null && !this.reponseEnvoyee)
			this.afficherErreur("Heure de fin incorrecte", evenement, request, response);
		if (fin != null)
			fin.set(Calendar.DAY_OF_WEEK, numJour);
		evenement.setHeureFin(fin);

		String[] locaux = request.getParameterValues("locaux");
		if (locaux != null && locaux.length > 0 && !this.reponseEnvoyee) {
			id = 0;
			Local local;
			ArrayList<Local> listeLocaux = new ArrayList<Local>();
			for (int i = 0; i < locaux.length; i++) {
				try {
					id = Integer.parseInt(locaux[i]);
					local = this.ucLocal.rechercher(id);
					if (local == null)
						this.afficherErreur("Local inconnu", evenement, request, response);
					listeLocaux.add(local);
				} catch (Exception e) {
					this.afficherErreur("Opération impossible", evenement, request, response);
				}
			}
			evenement.setLocaux(listeLocaux);
		}

		Cours cours = new Cours();
		String coursString = request.getParameter("cours");
		if (coursString != null && !coursString.isEmpty()&& !this.reponseEnvoyee) {
			try {
				id = Integer.parseInt(coursString);
				cours.setIdCours(id);
				cours = this.ucCours.rechercherCours(cours);
				if (cours == null && !this.reponseEnvoyee)
					this.afficherErreur("Cours inconnu", evenement, request, response);

			} catch (Exception e) {
				this.afficherErreur("Opération impossible", evenement, request, response);
			}
			evenement.setCours(cours);
		}

		evenement.setStatut('A');

		Horaire horaire = InfosHoraire.getHoraireSession(request);
		evenement.setHoraire(horaire);

		return evenement;
	}

	private void afficherErreur(String message, Evenement evenement, HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		this.reponseEnvoyee = true;
		request.setAttribute(Constantes.ATT_OPERATION, false);
		request.setAttribute(Constantes.ATT_MESSAGE, message);

		String[] jours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche" };

		request.setAttribute("jours", jours);

		List<Local> listeLocaux = getListeLocaux();

		List<Cours> listeCoursEquip = getListeCours(request);

		request.setAttribute("jours", jours);
		request.setAttribute(Constantes.ATT_OBJET, evenement);
		request.setAttribute("listeCours", listeCoursEquip);
		request.setAttribute(Constantes.ATT_LISTE, listeLocaux);
		request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_EVENEMENT).forward(request, response);
	}

	public static Evenement getEvenementHoraire(String intit, String desc, String hDeb, String hFin, HttpServletRequest request) {
		Evenement event = new Evenement();
		ParserDateHeure pdh = new ParserDateHeure();
		HttpSession session = request.getSession(true);
		event.setIntitule(intit);
		event.setDescription(desc);
		event.setHeureDebut(pdh.parser(hDeb));
		event.setHeureFin(pdh.parser(hFin));

		synchronized (session) {
			Horaire horaire = InfosHoraire.getHoraireSession(request);
			int ind = -1;
			for (int i = 0; i < horaire.getEvenements().size() && ind == -1; i++) {
				if (horaire.getEvenements().get(i).equalsLow(event)) {
					ind = i;
					event = horaire.getEvenements().get(i);
				}
			}
			if (ind == -1)
				return null;
		}
		return event;
	}

	public static int getIndiceEvent(String intit, String desc, String hDeb, String hFin, HttpServletRequest request) {
		Evenement event = new Evenement();
		ParserDateHeure pdh = new ParserDateHeure();
		HttpSession session = request.getSession(true);
		int ind = -1;
		event.setIntitule(intit);
		event.setDescription(desc);
		event.setHeureDebut(pdh.parser(hDeb));
		event.setHeureFin(pdh.parser(hFin));

		synchronized (session) {
			Horaire horaire = InfosHoraire.getHoraireSession(request);
			for (int i = 0; i < horaire.getEvenements().size() && ind == -1; i++) {
				if (horaire.getEvenements().get(i).equalsLow(event)) {
					ind = i;
					event = horaire.getEvenements().get(i);
				}
			}
		}
		return ind;
	}

	/**
	 * Verifie si l'evenement en chevauche un autre dans l'horaire type
	 * 
	 * @param evenement
	 *            : l'evenement a verifier
	 * @param request
	 *            : afin d'y recuperer la liste des evenements en session
	 * @return true si chevauche<br>
	 *         false si ne chevauche pas
	 */
	private boolean chevaucheTemporel(Evenement evenement, HttpServletRequest request, HttpServletResponse response) {
		if (evenement == null)
			return true;
		if (request == null)
			return true;
		Horaire horaire = InfosHoraire.getHoraireSession(request);
		if (horaire == null)
			return true;

		Evenement event = null;

		if (evenement.getHeureDebut() == null || evenement.getHeureFin() == null) {
			message = "Veuillez renseigner l'heure de début et l'heure de fin de l'événement";
			return true;
		}

		if (request.getParameter("hDeb").equals("") || request.getParameter("hFin").equals("")) {
			try {
				event = getEvenementRequest(request, response);
			} catch (Exception e) {
				message = "Une erreur est survenue lors de la validation de l'événement";
				return true;
			}

		} else
			event = getHiddenEvent(request);

		List<Evenement> liste = horaire.getEvenements();
		boolean chevauche = false;
		for (int i = 0; i < liste.size() && !chevauche; i++) {
			// On vérifie s'il s'agit du meme jour de la semaine, étant donné
			// que la vérification se fait sur un horaire type et non un vrai
			if (event == null || event != null && !event.equalsLow(liste.get(i))) {
				// S'il ne s'agit pas de lui même
				if (evenement.getHeureDebut().get(Calendar.DAY_OF_WEEK) == liste.get(i).getHeureDebut().get(Calendar.DAY_OF_WEEK)) {
					if (heureToInt(evenement.getHeureDebut()) < heureToInt(liste.get(i).getHeureFin()))
						if (heureToInt(evenement.getHeureFin()) > heureToInt(liste.get(i).getHeureDebut()))
							chevauche = true;
				}
			}
		}
		return chevauche;
	}

	private int heureToInt(Calendar heure) {
		int h = heure.get(Calendar.HOUR_OF_DAY) * 100;
		h += heure.get(Calendar.MINUTE);
		return h;
	}
}
