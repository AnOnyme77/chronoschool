package servlets;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usecases.GererUtilisateur;
import domaine.Constantes;

/**
 * Servlet implementation class TemplatePage
 */
@WebServlet("/mdpOublie.html")
public class MdpOublie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	GererUtilisateur gererUtilisateur;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getNamedDispatcher(Constantes.IHM_MDPOUBLIE).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter(Constantes.PARAM_EMAIL);

		Pattern rfc2822 = Pattern.compile(
		        "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
		);

		if (!rfc2822.matcher(email).matches()) {
			String erreurMdpOublie = "L'email indiquée est invalide, veuillez indiquer un email valide pour récupérer votre mot de passe";
			request.setAttribute("erreurMdpOublie", erreurMdpOublie);
		}
		else
		{
			gererUtilisateur.mdpOublie(email);
	
			String messageMdpOublie = "L'utilisateur correspondant à cet email recevra sont nouveau mot de passe par email.";
			request.setAttribute("messageMdpOublie", messageMdpOublie);
		}
		
		doGet(request, response);
	}

}
