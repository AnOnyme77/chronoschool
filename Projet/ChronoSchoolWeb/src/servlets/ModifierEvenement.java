package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usecases.GererEvenement;
import domaine.Constantes;
import domaine.Evenement;

/**
 * Servlet implementation class ModifierEvenement
 */
@WebServlet("/modifierEvenement.html")
public class ModifierEvenement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private GererEvenement ucEvenement;
	
	private boolean reponseEnvoyee = false;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=0;
		String idString = request.getParameter("idEvenement");
		Evenement evenement =null;
		this.reponseEnvoyee=false;
		if(idString==null || idString.isEmpty()){
			this.reponseEnvoyee=true;
			response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
		}
		
		try{
			id = Integer.parseInt(idString);
			evenement = this.ucEvenement.rechercherEvenement(id);
			
			if(evenement==null)
				response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
			
			evenement = this.ucEvenement.chargerTout(evenement);
			
			if(evenement==null)
				response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
			
			request.setAttribute(Constantes.ATT_OBJET, evenement);
		}
		catch(Exception e){
			response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
		}
		
		
		if(!this.reponseEnvoyee)
			request.getServletContext().getNamedDispatcher(Constantes.IHM_MODIFIER_EVENEMENT).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Evenement evenement = this.getEvenementRequest(request,response);
		Evenement db = this.ucEvenement.rechercherEvenement(evenement.getIdEvenement());
		
		if(db==null && !this.reponseEnvoyee)this.afficherErreur("Modification impossible", evenement, request, response);
		
		if(db!=null){
			db.setIntitule(evenement.getIntitule());
			db.setDescription(evenement.getDescription());
		}
		
		this.ucEvenement.mettreAJourEvenement(db);
		
		response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
	}
	
	private Evenement getEvenementRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		Evenement evenement = new Evenement();
		
		int id;
		String idString = request.getParameter("idEvenement");
		try{
			id = Integer.parseInt(idString);
			evenement.setIdEvenement(id);
		}
		catch(Exception e){
			this.reponseEnvoyee=true;
			response.sendRedirect(response.encodeRedirectURL("afficherHoraire.html"));
		}
		
		
		String intitule = request.getParameter("intitule");
		if(intitule==null || intitule.isEmpty())this.afficherErreur("Veuillez remplir le champs intitulé", evenement, request, response);
		evenement.setIntitule(intitule);
		
		String description = request.getParameter("description");
		evenement.setDescription(description);
		
		return evenement;
	}
	
	private void afficherErreur(String message, Evenement evenement, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.reponseEnvoyee = true;
		request.setAttribute(Constantes.ATT_OPERATION, false);
		request.setAttribute(Constantes.ATT_MESSAGE, message);

		request.setAttribute(Constantes.ATT_OBJET, evenement);
		request.getServletContext().getNamedDispatcher(Constantes.IHM_MODIFIER_EVENEMENT).forward(request, response);
	}

}
