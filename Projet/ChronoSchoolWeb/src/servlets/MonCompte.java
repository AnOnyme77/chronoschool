package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.ParserDate;
import usecases.GererUtilisateur;
import domaine.Constantes;
import domaine.Sexe;
import domaine.Utilisateur;

/**
 * Servlet implementation class MonCompte
 */
@WebServlet("/monCompte.html")
public class MonCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	GererUtilisateur gestionnaire;
	ParserDate parserDate = new ParserDate();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String date;
		// Récupération de la date en format input date
		HttpSession session = request.getSession(true);
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(Constantes.ATT_SESSION_USER);
		Calendar c = utilisateur.getDateNaissance();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		date = sdf.format(c.getTime());

		request.setAttribute("date", date);
		getServletContext().getNamedDispatcher(Constantes.IHM_COMPTE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		Utilisateur user = null;

		synchronized (session) {
			String nom, prenom, email, dateStr, mdp, mdpC;
			Sexe sexe;
			Calendar date = null;
			user = (Utilisateur) session.getAttribute(Constantes.ATT_SESSION_USER);
			//Vérification de l'intégrité des inputs

			nom = request.getParameter("nom");
			if(nom.equals("")){
				creerErreur(request, response, "Veuillez renseigner votre nom", user);
				return;
			}
			prenom = request.getParameter("prenom");
			if(prenom.equals("")){
				creerErreur(request, response, "Veuillez renseigner votre prénom", user);
				return;
			}
			email = request.getParameter("email");
			if(email.equals("")){
				creerErreur(request, response, "Veuillez renseigner votre email", user);
				return;
			}
			Pattern rfc2822 = Pattern.compile(
			        "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
			);

			if (!rfc2822.matcher(email).matches()) {
				creerErreur(request, response, "adresse email invalide", user);
				return;
			}
			dateStr = request.getParameter("dateNaiss");
			if(dateStr.equals("")){
				creerErreur(request, response, "Veuillez renseigner votre date de naissance", user);
				return;
			}
			try{
				date = parserDate.parser(dateStr);
			}catch(Exception e){
				creerErreur(request, response, "Date de naissance incorrecte, veuillez utiliser le format aaaa-mm-jj ou un navigateur plus évolué.", user);
				return;
			}
			
			if(date.after(GregorianCalendar.getInstance()) || date.equals(GregorianCalendar.getInstance())){
				creerErreur(request, response, "La date entrée est incorrecte", user);
				return;
			}
			mdp = request.getParameter("motDePasse");
			mdpC = request.getParameter("motDePasseConf");

			if (request.getParameter("sexe").equals("M")) {
				sexe = Sexe.Homme;
			} else {
				sexe = Sexe.Femme;
			}
			
			//On vérifie la disponibilité de l'adresse email
			if(!email.equals(user.getEmail())){
				//l'email a été modifié
				if(gestionnaire.existe(email)){
					creerErreur(request, response, "Cette adresse mail n'est plus disponible", user);
					return;
				}
			}

			user.setNom(nom);
			user.setPrenom(prenom);
			user.setEmail(email);
			user.setDateNaissance(date);
			user.setSexe(sexe);

			// On vérifie si les mots de passe sont remplis
			if (!mdp.equals("")) {
				// On verifie si les mdp correspondent
				if (!mdp.equals(mdpC)) {
					// erreur de mots de passe
					creerErreur(request, response, "Les mots de passe ne correspondent pas", user);
					return;
				} else {
					// On set le mdp pour la modification
					user.setMdp(mdp);
				}
			}

			if (gestionnaire.modifierUtilisateur(user) == null) {
				// ça n'a pas fonctionné
				creerErreur(request, response, "Une erreur est survenue lors de la modification, veuillez réessayer", user);
				return;
			} else {
				// L'utilisateur a été ajouté
				// Devra être redirigé vers la page de connexion et non l'index
				// (ou pas ?)
				session.setAttribute("msgModif", "Modification effectuée avec succès");
				if (user.getStatus() == 'A') {
					response.sendRedirect(response.encodeRedirectURL("monCompte.html"));
					return;
				} else {
					response.sendRedirect(response.encodeRedirectURL("monCompte.html"));
					return;
				}
			}

		}
	}
	
	private void creerErreur(HttpServletRequest request, HttpServletResponse response, String message, Utilisateur user) throws ServletException, IOException{
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		request.setAttribute("user", user);
		Calendar c = user.getDateNaissance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		request.setAttribute("date",sdf.format(c.getTime()));
		getServletContext().getNamedDispatcher(Constantes.IHM_COMPTE).forward(request, response);
		return;
	}

}
