package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererAnnee;
import usecases.GererCours;
import usecases.GererEquipement;
import usecases.GererUtilisateur;
import domaine.Annee;
import domaine.Constantes;
import domaine.Cours;
import domaine.CoursProfesseur;
import domaine.Equipement;
import domaine.Utilisateur;

/**
 * Servlet implementation class InfoCours
 */
@WebServlet("/infosCours.html")
public class InfoCours extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final int AJOUT=0;
	private static final int MODIFICATION=1;
	
	@EJB
	private GererAnnee gererAnnee;
	
	@EJB
	private GererCours gererCours;
	
	@EJB
	private GererUtilisateur gererUtilisateur;
	
	@EJB
	private GererEquipement gererEquipement;
	
	private boolean reponseEnvoyee=false;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.reponseEnvoyee=false;
		int id;
		String idString = request.getParameter("idCours");
		Cours cours = null;
		if(idString!=null){
			try{
				id = Integer.parseInt(idString);
				cours = new Cours();
				cours.setIdCours(id);
				cours = this.gererCours.rechercherCours(cours);
				cours = this.gererCours.chargerTout(cours);
			}
			catch(Exception e){
				
			}
		}
		HttpSession session = request.getSession(true);
		synchronized (session) {
		Utilisateur user = (Utilisateur) session.getAttribute(Constantes.ATT_SESSION_USER);
		user = gererUtilisateur.chargerTout(user);
		request.setAttribute("userCours", user);
		}
		request.setAttribute(Constantes.ATT_OBJET, cours);
		
		List<Annee> listeAnnee = gererAnnee.listerAnnee();
		request.setAttribute(Constantes.ATT_LISTE_ANNEE, listeAnnee);
		
		List<Utilisateur> listeProfesseur = gererUtilisateur.listerTousProfesseur();
		request.setAttribute(Constantes.ATT_LISTE_PROFESSEUR, listeProfesseur);
		
		List<Equipement> listeEquipement = gererEquipement.listerEquipement();
		request.setAttribute(Constantes.ATT_LISTE_EQUIPEMENT, listeEquipement);
		
		getServletContext().getNamedDispatcher(Constantes.IHM_INFO_COURS).forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		synchronized (session) {
			Utilisateur user = (Utilisateur) session.getAttribute(Constantes.ATT_SESSION_USER);
			this.reponseEnvoyee=false;
			Cours cours = this.getCoursRequest(request, response);
			if(!this.reponseEnvoyee && (user.getStatus()=='A' || user.getStatus()=='P')){
				switch(this.casPost(request)){
					case AJOUT:
						this.gererCours.ajouterCours(cours);
						response.sendRedirect(response.encodeRedirectURL("listeCours.html"));
						break;
					case MODIFICATION:
						this.gererCours.modifierCours(cours);
						response.sendRedirect(response.encodeRedirectURL("listeCours.html"));
						break;
				}
			}
		}
	}
	
	private Cours getCoursRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Cours cours = new Cours();
		
		int id;
		String idString  = request.getParameter("idCours");
		if(idString!=null && !idString.isEmpty()){
			try{
				id = Integer.parseInt(idString);
				cours.setIdCours(id);
			}
			catch(Exception e){
				if(!this.reponseEnvoyee)
					this.creerErreur(request, response, "Id incorrect", cours);
			}
		}
		
		String intitule = request.getParameter("intitule");
		if(!this.reponseEnvoyee && (intitule==null || intitule.isEmpty()))
			this.creerErreur(request, response, "Veuillez remplir le champs intitule", cours);
		cours.setIntitule(intitule);
		
		String description = request.getParameter("description");
		cours.setDescription(description);
		
		String idAnnee  = request.getParameter("annee");
		if(!this.reponseEnvoyee && (idAnnee==null || idAnnee.isEmpty()))this.creerErreur(request, response, "Annee incorrecte", cours);
		try{
			id = Integer.parseInt(idAnnee);
			
			Annee annee = this.gererAnnee.rechercherAnnee(id);
			if(!this.reponseEnvoyee && annee==null)this.creerErreur(request, response, "Annee introuvable", cours);
			cours.setAnnee(annee);
			
		}
		catch(Exception e){
			if(!this.reponseEnvoyee)
				this.creerErreur(request, response, "Id incorrect", cours);
		}
		
		String titulaire = request.getParameter("titulaire");
		if(!this.reponseEnvoyee &&(titulaire==null || titulaire.isEmpty()))
			this.creerErreur(request, response, "Le cours doit avoir un professeur titulaire", cours);
		
		String[] idProfs = request.getParameterValues("professeur");
		if(!this.reponseEnvoyee &&(idProfs==null || idProfs.length<=0))
			this.creerErreur(request, response, "Le cours doit contenir des professeurs", cours);
		List<CoursProfesseur> listeCP = new ArrayList<CoursProfesseur>();
		for(int i=0;!this.reponseEnvoyee && i<idProfs.length;i++){
			try{
				id = Integer.parseInt(idProfs[i]);
				
				Utilisateur utilisateur = this.gererUtilisateur.chercherUtilisateur(id);
				if(!this.reponseEnvoyee && utilisateur==null)this.creerErreur(request, response, "Annee introuvable", cours);
				
				CoursProfesseur cp = new CoursProfesseur();
				cp.setCours(cours);
				if(titulaire!=null && titulaire.equals(idProfs[i]))cp.setTitulaire(true);
				else cp.setTitulaire(false);
				cp.setProfesseur(utilisateur);
				
				listeCP.add(cp);
				
			}
			catch(Exception e){
				if(!this.reponseEnvoyee)
					this.creerErreur(request, response, "Id incorrect", cours);
				break;
			}
		}
		cours.setProfesseurs(listeCP);
		
		String[] idEquipements = request.getParameterValues("equipement");
		List<Equipement> listeEquipements = new ArrayList<Equipement>();
		if(idEquipements!=null)
			for(int i=0;!this.reponseEnvoyee && i<idEquipements.length;i++){
				try{
					id = Integer.parseInt(idEquipements[i]);
					
					Equipement equipement = new Equipement();
					equipement.setIdEquipement(id);
					
					equipement = this.gererEquipement.rechercherEquipement(equipement);
					if(!this.reponseEnvoyee && equipement==null)this.creerErreur(request, response, "Equipement introuvable", cours);
					
					listeEquipements.add(equipement);
					
				}
				catch(Exception e){
					if(!this.reponseEnvoyee)
						this.creerErreur(request, response, "Id incorrect", cours);
					break;
				}
			}
		
		cours.setEquipements(listeEquipements);
		
		
		return cours;
	}
	
	private int casPost(HttpServletRequest request){
		int retour = InfoCours.AJOUT;
		
		String idString  = request.getParameter("idCours");
		if(idString!=null && !idString.isEmpty() && !idString.equals("0"))retour = InfoCours.MODIFICATION;
		
		return retour;
	}
	
	private void creerErreur(HttpServletRequest request, HttpServletResponse response, String message, Cours cours)
			throws ServletException, IOException {
		this.reponseEnvoyee = true;
		
		request.setAttribute(Constantes.ATT_MESSAGE, message);
		
		List<Annee> listeAnnee = gererAnnee.listerAnnee();
		request.setAttribute(Constantes.ATT_LISTE_ANNEE, listeAnnee);
		
		List<Utilisateur> listeProfesseur = gererUtilisateur.listerTousProfesseur();
		request.setAttribute(Constantes.ATT_LISTE_PROFESSEUR, listeProfesseur);
		
		List<Equipement> listeEquipement = gererEquipement.listerEquipement();
		request.setAttribute(Constantes.ATT_LISTE_EQUIPEMENT, listeEquipement);

		request.setAttribute(Constantes.ATT_OBJET, cours);
		
		request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_COURS).forward(request, response);
	}

}
