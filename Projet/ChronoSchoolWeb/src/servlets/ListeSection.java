package servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outils.SecurityParser;
import usecases.GererAnnee;
import domaine.Annee;
import domaine.Constantes;

/**
 * Servlet implementation class TemplatePage
 */
@WebServlet("/listeSection.html")
public class ListeSection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	GererAnnee uc;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Annee> liste = this.uc.listerAnnee();
		SecurityParser parser = new SecurityParser();
		request.setAttribute(Constantes.ATT_LISTE, liste);
		request.setAttribute("roles", parser.getRolesSecurite(this.getServletContext().getResourceAsStream("/WEB-INF/security.xml")));
		getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_SECTION).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter(Constantes.PARAM_SUPPRIMER);
		try{
			int idAnnee = Integer.parseInt(id);
			
			this.uc.supprimerAnnee(idAnnee);
			
			request.setAttribute(Constantes.ATT_OPERATION, true);
			request.setAttribute(Constantes.ATT_MESSAGE, "Suppression effectuée avec succès.");
		}
		catch(NumberFormatException e){
			request.setAttribute(Constantes.ATT_OPERATION, false);
			request.setAttribute(Constantes.ATT_MESSAGE, "Échec de la suppression.");
		}
		
		doGet(request, response);
	}

}
