package servlets;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outils.ParserDate;
import usecases.GererUtilisateur;
import domaine.Constantes;
import domaine.Sexe;
import domaine.Utilisateur;

/**
 * Servlet implementation class Inscription
 */
@WebServlet("/inscription.html")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ParserDate parserDate = new ParserDate();
	@EJB
	GererUtilisateur gestionnaire;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().getNamedDispatcher(Constantes.IHM_INSCRIPTION).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		Utilisateur user = null;
		
		synchronized (session) {
			if(request.getParameter("submit").equals("Inscription")){
				String nom, prenom, email, dateStr, mdp, mdpC;
				Sexe sexe;
				Calendar date = null;
				
				nom = request.getParameter("nom");
				if(nom.equals("")){
					creerErreur(request, response, "Veuillez renseigner votre nom", user);
					return;
				}
				prenom = request.getParameter("prenom");
				if(prenom.equals("")){
					creerErreur(request, response, "Veuillez renseigner votre prénom", user);
					return;
				}
				email = request.getParameter("email");
				if(email.equals("")){
					creerErreur(request, response, "Veuillez renseigner votre email", user);
					return;
				}
				dateStr = request.getParameter("dateNaiss");
				if(dateStr.equals("")){
					creerErreur(request, response, "Veuillez renseigner votre date de naissance", user);
					return;
				}
				try{
					date = parserDate.parser(dateStr);
				}catch(Exception e){
					e.printStackTrace();
					creerErreur(request, response, "Date de naissance incorrecte, veuillez utiliser le format aaaa-mm-jj ou un navigateur plus évolué.", user);
					return;
				}
				
				if(date.after(GregorianCalendar.getInstance()) || date.equals(GregorianCalendar.getInstance())){
					creerErreur(request, response, "La date entrée est incorrecte", user);
					return;
				}
				mdp = request.getParameter("motDePasse");
				if(mdp.equals("")){
					creerErreur(request, response, "Veuillez renseigner votre mot de passe", user);
					return;
				}
				mdpC = request.getParameter("motDePasseConf");
				if(mdpC.equals("")){
					creerErreur(request, response, "Veuillez confirmer votre mot de passe", user);
					return;
				}
	
				if(request.getParameter("sexe").equals("M"))
				{
					sexe = Sexe.Homme;
				}
				else
				{
					sexe = Sexe.Femme;
				}
				
				//Verification de la disponibilité de l'adresse mail
				if(gestionnaire.existe(email)){
					creerErreur(request, response, "Cette adresse mail n'est plus disponible", user);
					return;
				}
				
				//On vérifie que les deux mots de passe concordent
				if(!mdp.equals(mdpC)){
					creerErreur(request, response, "Vos mots de passe ne correspondent pas", user);
					return;
				}else{
					user = new Utilisateur();
					user.setNom(nom);
					user.setPrenom(prenom);
					user.setEmail(email);
					user.setMdp(mdp); 
					user.setDateNaissance(date);
					user.setSexe(sexe);
					user.setStatus('U');//Par défaut, utilisateur
					
					if(gestionnaire.ajouterUtilisateur(user) == null){
						//ça n'a pas fonctionné
						creerErreur(request, response, gestionnaire.getMessage(), user);
					}else{
						//L'utilisateur a été ajouté
						//Devra être redirigé vers la page de connexion et non l'index (ou pas ?)
						session.setAttribute("msgInscription", "Inscription effectuée avec succès");
						response.sendRedirect(response.encodeRedirectURL("index.html"));
						return;
					}					
				}
				
			}
		}
	}
	private void creerErreur(HttpServletRequest request, HttpServletResponse response, String message, Utilisateur user) throws ServletException, IOException{
		request.setAttribute("msg", message);
		getServletContext().getNamedDispatcher(Constantes.IHM_INSCRIPTION).forward(request, response);
		return;
	}

}
