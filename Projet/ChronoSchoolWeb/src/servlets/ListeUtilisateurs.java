package servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outils.SecurityParser;
import usecases.GererUtilisateur;
import domaine.Constantes;
import domaine.Utilisateur;

/**
 * Servlet implementation class ListeUtilisateurs
 */
@WebServlet("/listeUtilisateurs.html")
public class ListeUtilisateurs extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	GererUtilisateur uc;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Utilisateur> liste = this.uc.listerTousUtilisateurs();
		SecurityParser parser = new SecurityParser();
		request.setAttribute(Constantes.ATT_LISTE, liste);
		request.setAttribute("roles", parser.getRolesSecurite(this.getServletContext().getResourceAsStream("/WEB-INF/security.xml")));
		getServletContext().getNamedDispatcher(Constantes.IHM_LISTE_UTILISATEURS).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter(Constantes.PARAM_SUPPRIMER);
		try{
			int idUtili = Integer.parseInt(id);
			
			this.uc.supprimerUtilisateur(idUtili);
			
			request.setAttribute(Constantes.ATT_OPERATION, true);
			request.setAttribute(Constantes.ATT_MESSAGE, "Suppression effectuée avec succès");
		}
		catch(NumberFormatException e){
			request.setAttribute(Constantes.ATT_OPERATION, false);
			request.setAttribute(Constantes.ATT_MESSAGE, "Échec de la suppression.");
		}
		
		doGet(request, response);
	}

}
