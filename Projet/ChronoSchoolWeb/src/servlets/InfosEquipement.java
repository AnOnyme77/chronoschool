package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usecases.GererEquipement;
import usecases.GererLocal;
import domaine.Constantes;
import domaine.Equipement;
import domaine.Local;

/**
 * Servlet implementation class InfosEquipement
 */
@WebServlet("/infosEquipement.html")
public class InfosEquipement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private GererEquipement uc;
	
	@EJB
	private GererLocal ucLocaux;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idString = request.getParameter("idEquipement");
		if(idString!=null && !idString.isEmpty()){
			try{
				int id = Integer.parseInt(idString);
				Equipement equipement = new Equipement();
				equipement.setIdEquipement(id);
				
				equipement = this.uc.rechercherEquipement(equipement);
				if(equipement!=null)equipement = this.uc.chargerTout(equipement);
				if(equipement!=null){
					request.setAttribute(Constantes.ATT_OBJET, equipement);
				}
				else{
					request.setAttribute(Constantes.ATT_OPERATION, false);
					request.setAttribute(Constantes.ATT_MESSAGE, "Equipement introuvable");
				}
			}
			catch(Exception e){
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Paramètre incorrect");
			}
		}
		request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_EQUIPEMENT).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(this.isAjout(request, response))ajouter(request,response);
		else if(this.isSuppression(request, response))supprimer(request,response);
		else modifier(request, response);
	}
	


	private void supprimer(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Equipement equipement = this.getEquipementFromRequest(request, response);
		Local local = new Local();
		boolean erreur = false;
		if(equipement!=null){
			String idString = request.getParameter("supprimer");
			if(idString!=null && !idString.isEmpty()){
				try{
					int id = Integer.parseInt(idString);
					local.setIdLocal(id);
					this.ucLocaux.supprimerEquipement(local, equipement);
					
					HttpSession session = request.getSession(true);
					synchronized(session){
						session.setAttribute(Constantes.ATT_OPERATION, true);
						session.setAttribute(Constantes.ATT_MESSAGE, "Suppression effectuée");
					}
				}
				catch(Exception e){
					erreur=true;
					request.setAttribute(Constantes.ATT_OPERATION, false);
					request.setAttribute(Constantes.ATT_MESSAGE, "Paramètre incorrect");
				}
			}
			else{
				erreur = true;
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Local inconnu");
			}
		}
		else{
			erreur = true;
			request.setAttribute(Constantes.ATT_OPERATION, false);
			request.setAttribute(Constantes.ATT_MESSAGE, "Veuillez remplir les champs");
		}
		if(erreur)request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_EQUIPEMENT).forward(request, response);
		else response.sendRedirect(response.encodeRedirectURL("listeEquipement.html"));
	}

	private void ajouter(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Equipement equipement = this.getEquipementFromRequest(request, response);
		boolean erreur=false;
		if(equipement==null){
			erreur=true;
			
		}
		else{
			equipement = this.uc.ajouterEquipement(equipement);
			if(equipement==null){
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Cet équipement existe déjà");
				erreur=true;
			}
			else{
				HttpSession session = request.getSession(true);
				synchronized(session){
					session.setAttribute(Constantes.ATT_OPERATION, true);
					session.setAttribute(Constantes.ATT_MESSAGE, "Enregistrement effectué avec succès");
				}
			}
		}
		if(erreur)request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_EQUIPEMENT).forward(request, response);
		else response.sendRedirect(response.encodeRedirectURL("listeEquipement.html"));
		
	}
	
	private void modifier(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Equipement equipement = this.getEquipementFromRequest(request, response);
		boolean erreur=false;
		if(equipement!=null){
			equipement = this.uc.modifierEquipement(equipement);
			if(equipement==null){
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Cet intitulé existe déjà");
				erreur=true;
			}
			else{
				HttpSession session = request.getSession(true);
				synchronized(session){
					session.setAttribute(Constantes.ATT_OPERATION, true);
					session.setAttribute(Constantes.ATT_MESSAGE, "Modification effectué avec succès");
				}
			}
		}
		else{
			erreur=true;
		}
		if(erreur)request.getServletContext().getNamedDispatcher(Constantes.IHM_INFO_EQUIPEMENT).forward(request, response);
		else response.sendRedirect(response.encodeRedirectURL("listeEquipement.html"));
	}

	private Equipement getEquipementFromRequest(HttpServletRequest request,
			HttpServletResponse response){
		
		String intitule = request.getParameter("intitule");
		if(intitule==null || intitule.isEmpty()){
			request.setAttribute(Constantes.ATT_OPERATION, false);
			request.setAttribute(Constantes.ATT_MESSAGE, "Veuillez remplir le champs intitule");
			return null;
		}
		
		String description = request.getParameter("description");
		if(description==null || description.isEmpty()){
			description="";
		}
		
		Equipement equipement = new Equipement();
		equipement.setIntitule(intitule);
		equipement.setDescription(description);
		
		String idString = request.getParameter("idEquipement");
		if(idString!=null && !idString.isEmpty()){
			try{
				int id = Integer.parseInt(idString);
				equipement.setIdEquipement(id);
			}
			catch(Exception e){
				request.setAttribute(Constantes.ATT_OPERATION, false);
				request.setAttribute(Constantes.ATT_MESSAGE, "Paramètre incorrect");
			}
		}
		
		return equipement;
	}
	
	private boolean isAjout(HttpServletRequest request, HttpServletResponse response){
		String id = request.getParameter("id");
		return (id==null || id.isEmpty()); 
	}
	private boolean isSuppression(HttpServletRequest request, HttpServletResponse response){
		String id = request.getParameter("supprimer");
		return (id!=null && !id.isEmpty()); 
	}

}
