package testsDaos;

import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dao.EquipementDao;
import domaine.Equipement;

public class TestEquipementDao {
	
	private ArrayList<Equipement> listeEquipement;
	private ArrayList<Equipement> listeEquipementDB;	
	
	EquipementDao equipementDao;
	
	
	
	
	@BeforeClass
	public void initialiser(){
		
		Context jndi;
		try {
			jndi = new InitialContext();
			equipementDao = (EquipementDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/EquipementDaoImpl!dao.EquipementDao");
			jndi.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		Equipement equipement = new Equipement();
			equipement.setDescription("Projecteur capable de projeter sur un éléphant indonésien");
			equipement.setIntitule("Projecteur haute performance");
			
			this.listeEquipement = new ArrayList<Equipement>();
			
			listeEquipement.add(equipement);
			
		equipement = new Equipement();
			equipement.setDescription("PC capable de lancer démineur sans planter !");
			equipement.setIntitule("Ordinateur");
			
			listeEquipement.add(equipement);
			
		equipement = new Equipement();
			equipement.setDescription("Chaise capable de porter un Somalien de 2kg!");
			equipement.setIntitule("Chaise");
			
			listeEquipement.add(equipement);
			
		
	}
	
	@Test
	public void ajouter() {
		int taille;
		int ancienneTaille = this.equipementDao.lister().size();
		Equipement retour = null;
		this.listeEquipementDB = new ArrayList<Equipement>();
		for(Equipement equipement :  this.listeEquipement){
			retour = this.equipementDao.enregistrer(equipement);
			Assert.assertNotNull(retour);
			Assert.assertTrue(retour.getIdEquipement()>0);
			this.listeEquipementDB.add(retour);
		}
		taille = this.equipementDao.lister().size();
		Assert.assertTrue(taille==(ancienneTaille+listeEquipement.size()));
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void lister(){
		Assert.assertTrue(this.equipementDao.lister().size()>=listeEquipementDB.size());
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void rechercher(){
		Equipement retour = null;
		for(Equipement equipement : this.listeEquipementDB){
			retour = this.equipementDao.rechercher(equipement.getIdEquipement());
			Assert.assertNotNull(retour);
			Assert.assertEquals(retour.getIdEquipement(), equipement.getIdEquipement());
		}
	}
	
	@Test(dependsOnMethods={"ajouter","rechercher"})
	public void modifier(){
		Equipement retour = null;
		Equipement equipement = this.equipementDao.rechercher(this.listeEquipementDB.get(0).getIdEquipement());

		equipement.setDescription("projecteur qui projete des chevres.");
		this.equipementDao.mettreAJour(equipement);
		
		retour = this.equipementDao.rechercher(this.listeEquipementDB.get(0).getIdEquipement());
		
		Assert.assertEquals(retour.getDescription(), equipement.getDescription());
		
	}
	
	@Test(dependsOnMethods={"ajouter","lister","modifier"})
	public void supprimer(){
		int taille;
		int tailleAncienne = this.equipementDao.lister().size();
		this.equipementDao.supprimer(this.listeEquipementDB.get(0).getIdEquipement());
		this.listeEquipementDB.remove(0);
		taille = this.equipementDao.lister().size();

		Assert.assertTrue(taille<tailleAncienne);
		
		for(Equipement equipement : this.listeEquipementDB){
			this.equipementDao.supprimer(equipement.getIdEquipement());
		}
				
	}	
	
}
