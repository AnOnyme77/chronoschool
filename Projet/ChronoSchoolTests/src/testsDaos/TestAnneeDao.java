package testsDaos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dao.AnneeDao;
import dao.UtilisateurDao;
import domaine.Annee;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestAnneeDao {

	private AnneeDao anneeDao;
	private UtilisateurDao userDao;

	private ArrayList<String> sections;
	private ArrayList<Integer> numeroAnnee;

	private ArrayList<Annee> listeAnnees;

	// listes pour les etudiants
	private ArrayList<Utilisateur> listeUser;

	private ArrayList<String> listeNoms;
	private ArrayList<String> listePrenom;
	private ArrayList<String> listeMdp;
	private ArrayList<Calendar> listeNaissance;
	private ArrayList<String> listeEmail;
	private ArrayList<Sexe> listeSexe;
	private ArrayList<Character> listeStatus;

	@BeforeClass
	public void initialiser() {

		Context jndi;
		try {
			jndi = new InitialContext();
			anneeDao = (AnneeDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/AnneeDaoImpl!dao.AnneeDao");
			userDao = (UtilisateurDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/UtilisateurDaoImpl!dao.UtilisateurDao");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		// Initialisation d'une série d'élèves de test
		initUser();
		// Initialisation des années
		sections = new ArrayList<String>();
		numeroAnnee = new ArrayList<Integer>();
		listeAnnees = new ArrayList<Annee>();
		
		sections.add("IG");
		sections.add("SMS");
		sections.add("AS");

		numeroAnnee.add(1);
		numeroAnnee.add(2);
		numeroAnnee.add(3);

		for (String s : sections) {
			for (Integer i : numeroAnnee) {
				Annee a = new Annee();
				a.setSection(s);
				a.setNumClasse(i);
				listeAnnees.add(a);
			}
		}
	}
	
	private void initUser() {
		Utilisateur user = null;
		this.listeUser = new ArrayList<Utilisateur>();
		
		this.listeNoms = new ArrayList<String>();
		this.listePrenom = new ArrayList<String>();
		this.listeMdp = new ArrayList<String>();
		this.listeEmail = new ArrayList<String>();
		this.listeNaissance = new ArrayList<Calendar>();
		this.listeSexe = new ArrayList<Sexe>();
		this.listeStatus = new ArrayList<Character>();

		this.listeNoms.add("AnneeEvrard");
		this.listeNoms.add("AnneeDeglas");
		this.listeNoms.add("AnneeCarly");

		this.listePrenom.add("AnneeLaurent");
		this.listePrenom.add("AnneeEmilie");
		this.listePrenom.add("AnneeSebastien");

		this.listeMdp.add("a");
		this.listeMdp.add("b");
		this.listeMdp.add("c");

		this.listeEmail.add("Anneeevrardlaurent77@gmail.com");
		this.listeEmail.add("Anneeemiliedeglas77@gmail.com");
		this.listeEmail.add("AnneeTheBigBoss@gmail.com");

		this.listeNaissance.add(new GregorianCalendar(1993, 05, 15));
		this.listeNaissance.add(new GregorianCalendar(1994, 04, 6));
		this.listeNaissance.add(new GregorianCalendar(1994, 05, 15));

		this.listeSexe.add(Sexe.Homme);
		this.listeSexe.add(Sexe.Femme);
		this.listeSexe.add(Sexe.Homme);

		this.listeStatus.add('A');
		this.listeStatus.add('B');
		this.listeStatus.add('C');

		for (int i = 0; i < this.listeNoms.size(); i++) {
			user = new Utilisateur();
			user.setNom(this.listeNoms.get(i));
			user.setPrenom(this.listePrenom.get(i));
			user.setEmail(this.listeEmail.get(i));
			user.setMdp(this.listeMdp.get(i));
			user.setDateNaissance(this.listeNaissance.get(i));
			user.setSexe(this.listeSexe.get(i));
			user.setStatus(this.listeStatus.get(i));

			this.listeUser.add(this.userDao.enregistrer(user));
		}
	}

	@Test
	public void testAjouterAnnee() {
		int nbAnnees;
		int nbAnneesOld = this.anneeDao.lister().size();
		Annee retour = null;
		for (Annee annee : this.listeAnnees) {
			retour = this.anneeDao.enregistrer(annee);
			Assert.assertNotNull(retour);
			Assert.assertTrue(retour.getIdAnnee() > 0);
		}
		nbAnnees = this.anneeDao.lister().size();
		Assert.assertEquals(nbAnnees, nbAnneesOld + (this.listeAnnees.size()));
	}
	
	@Test(dependsOnMethods={"testAjouterAnnee"})
	public void testListerAnnee(){
		Assert.assertTrue(this.anneeDao.lister().size() > 0);
	}
	
	@Test(dependsOnMethods={"testAjouterAnnee"})
	public void testModifierAnnee(){
		Annee annee = this.anneeDao.rechercher(1);
		Annee retour = null;
		annee.setSection("SectionTest");
		retour = this.anneeDao.mettreAJour(annee);
		Assert.assertNotNull(retour);
		Assert.assertEquals(retour.getSection(), "SectionTest");
	}
	/**
	 * Teste l'ajout d'étudiants dans une année n'en contenant pas encore
	 */
	@Test(dependsOnMethods={"testModifierAnnee"})
	public void testAjoutUserOnEmpty(){
		Annee annee = this.anneeDao.rechercher(1);
		annee = this.anneeDao.chargerTout(annee);
		Annee retour = null;
		Assert.assertTrue(annee.getEtudiants().size() == 0);

		retour = this.anneeDao.ajouterEtudiant(annee, this.listeUser.get(1));
		
		Assert.assertNotNull(retour);
		Assert.assertNotEquals(annee.getEtudiants(), retour.getEtudiants());
	}
	
	@Test(dependsOnMethods={"testAjoutUserOnEmpty"})
	public void testListerEtudiants(){
		Annee annee = this.anneeDao.rechercher(1);
		List<Utilisateur> liste = this.anneeDao.listerEtudiants(annee);
		Assert.assertNotNull(liste);
		Assert.assertEquals(this.listeUser.get(1).getEmail(), liste.get(0).getEmail());
	}
	
	@Test(dependsOnMethods={"testListerEtudiants"})
	public void testAjoutUser(){
		int nbEtudiants, nbEtudiantsOld;
		Annee annee = this.anneeDao.rechercher(1);
		Annee retour = null;
		
		nbEtudiantsOld = this.anneeDao.listerEtudiants(annee).size();
		
		//A cause du test précédent, cet utilisateur est deja supposé être ajouté 
		//et ne devrait donc pas pouvoir l'etre une deuxieme fois
		Assert.assertNull(this.anneeDao.ajouterEtudiant(annee, this.listeUser.get(1)));

		Assert.assertNotNull(this.anneeDao.ajouterEtudiant(annee, this.listeUser.get(0)));
		Assert.assertNotNull(this.anneeDao.ajouterEtudiant(annee, this.listeUser.get(2)));
		retour = this.anneeDao.rechercher(1);
		nbEtudiants = this.anneeDao.listerEtudiants(retour).size();
		Assert.assertEquals(nbEtudiants, nbEtudiantsOld + 2);
	}
	
	@Test(dependsOnMethods={"testAjoutUser"})
	public void testModifierUser(){
		Annee annee = this.anneeDao.rechercher(1);
		int nbEtudiantsOld = this.anneeDao.listerEtudiants(annee).size();
		int nbEtudiants;
		Annee retour = null;
		//Verification de la modification d'un user existant
		Utilisateur user = this.anneeDao.listerEtudiants(annee).get(1);
		user.setNom("NomTest");
		retour = this.anneeDao.modifierEtudiant(annee, user);
		Assert.assertNotNull(retour);
		nbEtudiants = this.anneeDao.listerEtudiants(annee).size();
		Assert.assertEquals(nbEtudiants, nbEtudiantsOld);
		
		//Creation d'un nouvel utilisateur
		Utilisateur newUser = new Utilisateur();
		newUser.setNom("NewNom");
		newUser.setPrenom("newPrenom");
		newUser.setEmail("email@mail.com");
		newUser.setMdp("1234");
		newUser.setDateNaissance(new GregorianCalendar(1990, 02, 02));
		newUser.setSexe(Sexe.Homme);
		newUser.setStatus('A');
		newUser = this.userDao.enregistrer(newUser);
		
		retour = this.anneeDao.modifierEtudiant(retour, newUser);
		nbEtudiants = this.anneeDao.listerEtudiants(retour).size();
		Assert.assertEquals(nbEtudiantsOld + 1, nbEtudiants);
	}
	
	@Test(dependsOnMethods={"testModifierUser"})
	public void testSupprimerAnnee(){
		Annee annee = this.anneeDao.rechercher(2);
		this.anneeDao.supprimer(annee.getIdAnnee());
		Assert.assertNull(this.anneeDao.rechercher(2));
	}
}
