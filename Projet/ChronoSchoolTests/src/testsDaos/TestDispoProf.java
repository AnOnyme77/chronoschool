package testsDaos;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dao.CoursDao;
import dao.CoursProfesseurDao;
import dao.EvenementDao;
import dao.UtilisateurDao;
import domaine.Cours;
import domaine.CoursProfesseur;
import domaine.Evenement;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestDispoProf {
	private UtilisateurDao utilisateurDao;
	private CoursProfesseurDao coursProfesseurDao;
	private CoursDao coursDao;
	private EvenementDao evenementDao;
	
	private List<CoursProfesseur> listeCoursProfesseur;
	private Utilisateur prof;
	private CoursProfesseur coursProfesseur;
	private Cours cours;
	private Evenement evenement;
	
	@BeforeClass
	public void initialiser(){
		Context jndi;
		try {
			jndi = new InitialContext();
			utilisateurDao = (UtilisateurDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/UtilisateurDaoImpl!dao.UtilisateurDao");
			coursProfesseurDao = (CoursProfesseurDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/CoursProfesseurDaoImpl!dao.CoursProfesseurDao");
			coursDao = (CoursDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/CoursDaoImpl!dao.CoursDao");
			evenementDao = (EvenementDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/EvenementDaoImpl!dao.EvenementDao");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		//Init user
		prof = new Utilisateur();
		prof.setNom("Prof");
		prof.setPrenom("La");
		prof.setDateNaissance(new GregorianCalendar(1970, 01, 01));
		prof.setSexe(Sexe.Femme);
		prof.setEmail("leprof@mail.com");
		prof.setStatus('P');
		prof.setMdp("password");
		prof = utilisateurDao.enregistrer(prof);
		
		//Init cours
		cours = new Cours();
		cours.setIntitule("Le cours du prof");
		cours.setDescription("La description du cours");
		cours = coursDao.enregistrer(cours);
		
		//Init cp
		coursProfesseur = new CoursProfesseur();
		coursProfesseur.setTitulaire(true);
		coursProfesseur.setProfesseur(prof);
		coursProfesseur.setCours(cours);
		coursProfesseur = coursProfesseurDao.enregistrer(coursProfesseur);
		
		listeCoursProfesseur = new ArrayList<>();
		listeCoursProfesseur.add(coursProfesseur);
		
		prof.setCours(listeCoursProfesseur);
		prof = utilisateurDao.mettreAJour(prof);
		
		cours.setProfesseurs(listeCoursProfesseur);
		cours = coursDao.mettreAJour(cours);
		
		//A ce stade, nous possédons un cours lié à un professeur via un coursprofesseur
		
		//Cration d'un évènement lié au cours
		evenement = new Evenement();
		evenement.setIntitule("evenement de test");
		evenement.setDescription("Description de l'évènement de test");
		evenement.setCours(cours);
		evenement.setHeureDebut(new GregorianCalendar(2015, 0, 28, 8, 15));
		evenement.setHeureFin(new GregorianCalendar(2015, 0, 28, 9, 15));
		evenement.setStatut('A');
		evenement = evenementDao.enregistrer(evenement);
	}
	
	/**
	 * Ce test vérifie que le professeur soit bien indiqué comme indisponible 
	 * durant les heures où il donne cours. 
	 */
	@Test
	private void testDispo(){
		Evenement e = this.evenementDao.rechercher(evenement.getIdEvenement());
		Assert.assertEquals(e.getStatut(), 'A');
		
		e.setStatut('S');
		e = this.evenementDao.mettreAJour(e);
		
		Assert.assertEquals(e.getStatut(), 'S');
		
		e.setStatut('A');
		e = this.evenementDao.mettreAJour(e);
		
		Assert.assertEquals(e.getStatut(), 'A');
	}
}
