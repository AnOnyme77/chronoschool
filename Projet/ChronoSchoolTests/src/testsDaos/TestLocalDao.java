package testsDaos;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dao.LocalDao;
import domaine.Equipement;
import domaine.Local;

public class TestLocalDao {
	
	private List<Integer> listNbPlaces;
	private List<String> listIntitule;
	private List<Equipement> listEquip;
	private List<Local> listLocal;
	private List<Local> listLocalDB;
	private List<String> listIntituleE;
	private List<String> listDescriptionE;
	
	LocalDao dao;
	
	@BeforeClass
	public void initialiser(){
		
		Context jndi;
		try {
			jndi = new InitialContext();
			dao = (LocalDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/LocalDaoImpl!dao.LocalDao");
			jndi.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
initialiserEquip();
		
		listNbPlaces.add(20);
		listNbPlaces.add(30);
		listNbPlaces.add(40);
		
		listIntitule.add("L40");
		listIntitule.add("E5");
		listIntitule.add("B12");
		
		for(int i = 0; i < listIntitule.size(); i++){
			Local l = new Local();
			l.setIntitule(listIntitule.get(i));
			l.setNbPlaces(listNbPlaces.get(i));
			listLocal.add(l);
		}		
	}
	
	private void initialiserEquip() {
		listDescriptionE = new ArrayList<String>();
		listIntituleE = new ArrayList<String>();
		
		listDescriptionE.add("Projecteur");
		listDescriptionE.add("Télévision");
		listDescriptionE.add("Tableau et même effacable !");
		
		listIntituleE.add("Projo");
		listIntituleE.add("TV");
		listIntituleE.add("Tableau");
		
		listEquip = new ArrayList<Equipement>();
		
		for(int i = 0; i < listDescriptionE.size(); i++){
			Equipement equip = new Equipement();
			equip.setDescription(listDescriptionE.get(i));
			equip.setIntitule(listIntituleE.get(i));
			listEquip.add(equip);
		}
	}
	
	@Test
	public void ajouter() {
		int taille;
		int ancienneTaille = this.dao.lister().size();
		Local retour = null;
		for(Local local :  this.listLocal){
			retour = this.dao.enregistrer(local);
			Assert.assertNotNull(retour);
			Assert.assertTrue(retour.getIdLocal()>0);
			this.listLocalDB.add(retour);
		}
		taille = this.dao.lister().size();
		Assert.assertTrue(taille==(ancienneTaille+3));
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void lister(){
		Assert.assertTrue(this.dao.lister().size()>=0);
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void rechercherByIntitule(){
		Local retour = null;
		for(Local local : this.listLocalDB){
			retour = this.dao.rechercher(local.getIntitule());
			Assert.assertTrue(retour.getIntitule().equals(local.getIntitule()));
		}
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void rechercherById(){
		Local retour = null;
		for(Local local : this.listLocalDB){
			retour = this.dao.rechercher(local.getIdLocal());
			Assert.assertTrue(retour.getIntitule().equals(local.getIntitule()));
		}
	}
	
	@Test(dependsOnMethods={"ajouter","rechercher"})
	public void modifier(){
		Local retour = null;
		Local local = this.dao.rechercher(this.listLocalDB.get(this.listLocalDB.size()-1).getIdLocal());

		local.setNbPlaces(5);
		this.dao.mettreAJour(local);
		
		retour = this.dao.rechercher(this.listLocalDB.get(this.listLocalDB.size()-1).getIdLocal());
		
		Assert.assertTrue(retour.getNbPlaces()==local.getNbPlaces());
		
	}
	
	@Test(dependsOnMethods={"ajouter","lister"})
	public void supprimer(){
		int taille;
		int tailleAncienne = this.dao.lister().size();
		this.dao.supprimer(this.listLocalDB.get(this.listLocalDB.size()-1).getIdLocal());
		taille = this.dao.lister().size();

		Assert.assertTrue(taille<tailleAncienne);
	}	
}
