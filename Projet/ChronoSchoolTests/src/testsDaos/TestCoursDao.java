package testsDaos;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererAnnee;
import usecases.GererUtilisateur;
import dao.CoursDao;
import domaine.Annee;
import domaine.Cours;
import domaine.CoursProfesseur;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestCoursDao {
	
	private List<Utilisateur> users;
	private List<Cours> cours;
	private List<Annee> annees;
	private GererUtilisateur ucUtilisateur;
	private CoursDao coursDao;
	private GererAnnee ucAnnee;
	
	@BeforeClass
	public void beforeClass() {
		this.users = new ArrayList<Utilisateur>();
		this.cours = new ArrayList<Cours>();
		
		Context jndi;
		try {
			jndi = new InitialContext();
			this.coursDao = (CoursDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/CoursDaoImpl!dao.CoursDao");
			this.ucUtilisateur = (GererUtilisateur) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererUtilisateurImpl!usecases.GererUtilisateur");
			this.ucAnnee = (GererAnnee) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererAnneeImpl!usecases.GererAnnee");
			jndi.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		Utilisateur p1 = new Utilisateur();
		p1.setNom("Pollart");
		p1.setDateNaissance(new GregorianCalendar(1990,05,15));
		p1.setEmail("michel.pollart@helha.be");
		p1.setMdp("michel");
		p1.setPrenom("Michel");
		p1.setSexe(Sexe.Homme);
		p1.setStatus('P');
		p1 = this.ucUtilisateur.ajouterUtilisateur(p1);
		this.users.add(p1);
		
		Utilisateur p2 = new Utilisateur();
		p2.setNom("Dal");
		p2.setDateNaissance(new GregorianCalendar(1990,05,15));
		p2.setEmail("christine.dal@helha.be");
		p2.setMdp("christine");
		p2.setPrenom("Christine");
		p2.setSexe(Sexe.Femme);
		p2.setStatus('P');
		p2 = this.ucUtilisateur.ajouterUtilisateur(p2);
		this.users.add(p2);
		
		Utilisateur p3 = new Utilisateur();
		p3.setNom("Gouwy");
		p3.setDateNaissance(new GregorianCalendar(1990,05,15));
		p3.setEmail("jean-louis.gouwy@helha.be");
		p3.setMdp("jean-louis");
		p3.setPrenom("Jean-Louis");
		p3.setSexe(Sexe.Homme);
		p3.setStatus('P');
		p3=this.ucUtilisateur.ajouterUtilisateur(p3);
		this.users.add(p3);
		
		this.annees = new ArrayList<Annee>();
		Annee a1 = new Annee();
		a1.setNumClasse(1);
		a1.setSection("IG");
		a1.setEtudiants(null);
		a1=this.ucAnnee.enregistrerAnnee(a1);
		this.annees.add(a1);
		
		Annee a2 = new Annee();
		a2.setNumClasse(2);
		a2.setSection("IG");
		a2.setEtudiants(null);
		a2=this.ucAnnee.enregistrerAnnee(a2);
		this.annees.add(a2);
		
		
	}
	
	@Test
	public void ajouterCours() {
		ArrayList<CoursProfesseur>coursProfesseur = new ArrayList<CoursProfesseur>();
		Cours cours1 = new Cours();
		cours1.setIntitule("Mathématique");
		cours1.setDescription("Donné par Monsieur Pollart");
		CoursProfesseur cp = new CoursProfesseur();
		cp.setCours(cours1);
		cp.setProfesseur(this.users.get(0));
		cp.setTitulaire(true);
		coursProfesseur.add(cp);
		cp = new CoursProfesseur();
		cp.setCours(cours1);
		cp.setProfesseur(this.users.get(2));
		cp.setTitulaire(false);
		coursProfesseur.add(cp);
		cours1.setAnnee(this.annees.get(0));
		cours1.setProfesseurs(coursProfesseur);
		cours1 = this.coursDao.enregistrer(cours1);
		Assert.assertNotNull(cours1);
		Assert.assertNotEquals(cours1.getIdCours(),0);
		this.cours.add(cours1);
		
		coursProfesseur = new ArrayList<CoursProfesseur>();
		Cours cours2 = new Cours();
		cours2.setIntitule("C");
		cours2.setDescription("Much difficult");
		cp = new CoursProfesseur();
		cp.setCours(cours2);
		cp.setProfesseur(this.users.get(2));
		cp.setTitulaire(true);
		coursProfesseur.add(cp);
		cp = new CoursProfesseur();
		cp.setCours(cours2);
		cp.setProfesseur(this.users.get(1));
		cp.setTitulaire(false);
		coursProfesseur.add(cp);
		cours2.setAnnee(this.annees.get(1));
		cours2.setProfesseurs(coursProfesseur);
		cours2 = this.coursDao.enregistrer(cours2);
		Assert.assertNotNull(cours2);
		Assert.assertNotEquals(cours2.getIdCours(),0);
		this.cours.add(cours2);
	}
	
	@Test(dependsOnMethods={"ajouterCours"})
	public void getCours(){
		Cours cours = this.coursDao.rechercher(this.cours.get(0).getIdCours());
		Assert.assertNotNull(cours);
	}
	
	@Test(dependsOnMethods={"getCours"})
	public void modifierCours(){
		Cours cours = this.cours.get(1);
		cours = this.coursDao.rechercher(cours.getIdCours());
		
		cours.setDescription("Vlegede");
		cours.setIntitule("Circonvolutions");
		cours = this.coursDao.mettreAJour(cours);
		Assert.assertNotNull(cours);
		
		cours = this.coursDao.rechercher(cours.getIdCours());
		Assert.assertEquals(cours.getDescription(), "Vlegede");
	}
	
	@Test(dependsOnMethods={"modifierCours"})
	public void supprimerCours(){
		this.coursDao.supprimer(this.cours.get(0).getIdCours());
		
		Cours cours = this.coursDao.rechercher(this.cours.get(0).getIdCours());
		Assert.assertNull(cours);
	}
  

	@AfterClass
	public void afterClass() {
	}

}
