package testsDaos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import dao.UtilisateurDao;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestUtilisateurDao {
	
	private ArrayList<Utilisateur> listeUtili;
	private ArrayList<Utilisateur> listeUtiliDB;
	
	private ArrayList<String> listeNoms;
	private ArrayList<String> listePrenom;
	private ArrayList<String> listeMdp;
	private ArrayList<Calendar> listeNaissance;
	private ArrayList<String> listeEmail;
	private ArrayList<Sexe> listeSexe;
	private ArrayList<Character> listeStatus;
	
	UtilisateurDao dao;
	
	
	@BeforeClass
	public void initialiser(){
		
		Context jndi;
		try {
			jndi = new InitialContext();
			dao = (UtilisateurDao) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/UtilisateurDaoImpl!dao.UtilisateurDao");
			jndi.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		Utilisateur user=null;
		this.listeUtili = new ArrayList<Utilisateur>();
		this.listeUtiliDB = new ArrayList<Utilisateur>();
		
		this.listeNoms = new ArrayList<String>();
		this.listePrenom = new ArrayList<String>();
		this.listeMdp = new ArrayList<String>();
		this.listeEmail = new ArrayList<String>();
		this.listeNaissance = new ArrayList<Calendar>();
		this.listeSexe = new ArrayList<Sexe>();
		this.listeStatus = new ArrayList<Character>();
		
		this.listeNoms.add("Evrard");
		this.listeNoms.add("Deglas");
		this.listeNoms.add("Carly");
		
		this.listePrenom.add("Laurent");
		this.listePrenom.add("Emilie");
		this.listePrenom.add("Sebastien");
		
		this.listeMdp.add("a");
		this.listeMdp.add("b");
		this.listeMdp.add("c");
		
		this.listeEmail.add("evrardlaurent77@gmail.com");
		this.listeEmail.add("emiliedeglas77@gmail.com");
		this.listeEmail.add("TheBigBoss@gmail.com");
		
		this.listeNaissance.add(new GregorianCalendar(1993,05,15));
		this.listeNaissance.add(new GregorianCalendar(1994,04,6));
		this.listeNaissance.add(new GregorianCalendar(1994,05,15));
		
		this.listeSexe.add(Sexe.Homme);
		this.listeSexe.add(Sexe.Femme);
		this.listeSexe.add(Sexe.Homme);
		
		this.listeStatus.add('A');
		this.listeStatus.add('B');
		this.listeStatus.add('C');
		
		for(int i=0;i<this.listeNoms.size();i++){
			user = new Utilisateur();
			user.setNom(this.listeNoms.get(i));
			user.setPrenom(this.listePrenom.get(i));
			user.setEmail(this.listeEmail.get(i));
			user.setMdp(this.listeMdp.get(i));
			user.setDateNaissance(this.listeNaissance.get(i));
			user.setSexe(this.listeSexe.get(i));
			user.setStatus(this.listeStatus.get(i));
			
			this.listeUtili.add(user);
		}
		
	}
	
	@Test
	public void ajouter() {
		int taille;
		int ancienneTaille = this.dao.lister().size();
		Utilisateur retour = null;
		for(Utilisateur user :  this.listeUtili){
			retour = this.dao.enregistrer(user);
			Assert.assertNotNull(retour);
			Assert.assertTrue(retour.getIdUtilisateur()>0);
			this.listeUtiliDB.add(retour);
		}
		taille = this.dao.lister().size();
		Assert.assertTrue(taille==(ancienneTaille+3));
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void lister(){
		Assert.assertTrue(this.dao.lister().size()>=0);
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void rechercher(){
		Utilisateur retour = null;
		for(Utilisateur user : this.listeUtiliDB){
			retour = this.dao.rechercher(user.getIdUtilisateur());
			Assert.assertTrue(retour.getEmail().equals(user.getEmail()));
		}
	}
	
	@Test(dependsOnMethods={"ajouter","rechercher"})
	public void modifier(){
		Utilisateur retour = null;
		Utilisateur user = this.dao.rechercher(this.listeUtiliDB.get(this.listeUtiliDB.size()-1).getIdUtilisateur());

		user.setSexe(Sexe.Femme);
		this.dao.mettreAJour(user);
		
		retour = this.dao.rechercher(this.listeUtiliDB.get(this.listeUtiliDB.size()-1).getIdUtilisateur());
		
		Assert.assertTrue(retour.getSexe()==user.getSexe());
		
	}
	
	@Test(dependsOnMethods={"ajouter","lister","modifier"})
	public void supprimer(){
		int taille;
		int tailleAncienne = this.dao.lister().size();
		this.dao.supprimer(this.listeUtiliDB.get(this.listeUtiliDB.size()-1).getIdUtilisateur());
		taille = this.dao.lister().size();

		Assert.assertTrue(taille<tailleAncienne);
		
	}	
	
}
