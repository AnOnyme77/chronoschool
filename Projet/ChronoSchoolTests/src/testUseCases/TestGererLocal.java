package testUseCases;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererEquipement;
import usecases.GererLocal;
import domaine.Equipement;
import domaine.Local;

public class TestGererLocal {
	
	private GererLocal gestionnaire;
	private GererEquipement gestionnaireE;
	
	private List<Integer> listNbPlaces;
	private List<String> listIntitule;
	private List<Equipement> listEquip;
	private List<Local> listLocal;

	private List<String> listIntituleE;
	private List<String> listDescriptionE;
	
	@BeforeClass
	public void init(){
		Context jndi;
		try {
			jndi = new InitialContext();
			gestionnaire = (GererLocal) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererLocalImpl!usecases.GererLocal");
			gestionnaireE = (GererEquipement) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererEquipementImpl!usecases.GererEquipement");
			jndi.close();
		} catch (NamingException e) { 
			e.printStackTrace();
		}
		
		initialiserEquip();
		
		listNbPlaces.add(20);
		listNbPlaces.add(30);
		listNbPlaces.add(40);
		
		listIntitule.add("L40");
		listIntitule.add("E5");
		listIntitule.add("B12");
		
		for(int i = 0; i < listIntitule.size(); i++){
			Local l = new Local();
			l.setIntitule(listIntitule.get(i));
			l.setNbPlaces(listNbPlaces.get(i));
			listLocal.add(l);
		}		
	}
	
	private void initialiserEquip() {
		listDescriptionE = new ArrayList<String>();
		listIntituleE = new ArrayList<String>();
		
		listDescriptionE.add("Projecteur");
		listDescriptionE.add("Télévision");
		listDescriptionE.add("Tableau et même effacable !");
		
		listIntituleE.add("Projo");
		listIntituleE.add("TV");
		listIntituleE.add("Tableau");
		
		listEquip = new ArrayList<Equipement>();
		
		for(int i = 0; i < listDescriptionE.size(); i++){
			Equipement equip = new Equipement();
			equip.setDescription(listDescriptionE.get(i));
			equip.setIntitule(listIntituleE.get(i));
			equip = this.gestionnaireE.ajouterEquipement(equip);
			listEquip.add(equip);
		}
	}
	
	@Test
	public void testAjouterLocal(){
		List<Local> local = new ArrayList<Local>();
		for(Local l : listLocal){
			local.add(this.gestionnaire.enregistrer(l));
		}
		Assert.assertEquals(this.gestionnaire.rechercher(1), local.get(1));
	}
	
	@Test(dependsOnMethods={"testAjouterLocal"})
	public void testListerLocal(){
		List<Local> local = this.gestionnaire.lister();
		Assert.assertEquals(local.size(), 3);
	}
	
	@Test(dependsOnMethods={"testListerLocal"})
	public void testModifierLocal(){
		Local local = this.gestionnaire.rechercher(1);
		local.setNbPlaces(36);
		Local retour = this.gestionnaire.mettreAJour(local);
		Assert.assertNotNull(retour);
		Assert.assertEquals(retour.getNbPlaces(), 36);
	}
	
	@Test(dependsOnMethods={"testModifierLocal"})
	public void testAjoutEquip(){
		Local local = this.gestionnaire.rechercher(1);
		for(Equipement u : listEquip){
			local = this.gestionnaire.ajouterEquipement(local, u);
		}
		Assert.assertEquals(local.getEquipements().size(), listEquip.size());
	}
	
	@Test(dependsOnMethods={"testAjoutEquip"})
	public void testModifEquip(){
		Local local = this.gestionnaire.rechercher(1);
		Equipement equip = local.getEquipements().get(1);
		equip.setDescription("C'est un aspirateur");
		local = this.gestionnaire.modifierEquipement(local, equip);
		Assert.assertEquals(local.getEquipements().get(1).getDescription(), equip.getDescription());
	}
	
	@Test(dependsOnMethods={"testModifEquip"})
	public void testSupprimerLocal(){
		Local local = this.gestionnaire.rechercher(1);
		this.gestionnaire.supprimer(local);
		Assert.assertNull(this.gestionnaire.rechercher(local.getIdLocal()));
	}
}
