package testUseCases;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererAnnee;
import usecases.GererUtilisateur;
import domaine.Annee;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestGererAnnee {
	
	private GererAnnee gestionnaireAnnee;
	private GererUtilisateur gestionnaireUtilisateur;
	
	private List<Annee> listeAnnees;
	
	private List<String> listeSections;
	private List<Integer> numeroAnnee;
	
	// listes pour les etudiants
	private List<Utilisateur> listeUser;

	private List<String> listeNoms;
	private List<String> listePrenoms;
	private List<String> listeMdp;
	private List<Calendar> listeNaissances;
	private List<String> listeEmails;
	private List<Character> listeStatus;
	
	
	@BeforeClass
	public void initialiser(){
		Context jndi;
		try{
			jndi = new InitialContext();
			gestionnaireAnnee = (GererAnnee) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererAnneeImpl!usecases.GererAnnee");
			gestionnaireUtilisateur = (GererUtilisateur) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererUtilisateurImpl!usecases.GererUtilisateur");
			jndi.close();
		}catch(NamingException e){
			e.printStackTrace();
		}
		
		//Initialisation des users
		initialiserUsers();
		
		//Initialisation des annees
		listeSections = new ArrayList<String>();
		numeroAnnee = new ArrayList<Integer>();
		listeAnnees = new ArrayList<Annee>();
		
		listeSections.add("IG");
		listeSections.add("SMS");
		listeSections.add("AS");
		
		numeroAnnee.add(1);
		numeroAnnee.add(2);
		numeroAnnee.add(3);
		
		for(String s : listeSections){
			for(Integer i : numeroAnnee){
				Annee a = new Annee();
				a.setSection(s);
				a.setNumClasse(i);
				a.setEtudiants(null);
				listeAnnees.add(a);
			}
		}		
	}

	private void initialiserUsers() {
		listeNoms = new ArrayList<String>();
		listePrenoms = new ArrayList<String>();
		listeMdp = new ArrayList<String>();
		listeEmails = new ArrayList<String>();
		listeNaissances = new ArrayList<Calendar>();
		listeStatus = new ArrayList<Character>();
		listeUser= new ArrayList<Utilisateur>();
		
		listeNoms.add("TOTO");
		listeNoms.add("TATA");
		listeNoms.add("TITI");
		
		listePrenoms.add("Toto");
		listePrenoms.add("Tata");
		listePrenoms.add("Titi");
		
		listeMdp.add("toto");
		listeMdp.add("tata");
		listeMdp.add("titi");
		
		listeEmails.add("prof1@gmail.com");
		listeEmails.add("prof2@gmail.com");
		listeEmails.add("prof3@gmail.com");
		
		listeNaissances.add(new GregorianCalendar(1991,01,01));
		listeNaissances.add(new GregorianCalendar(1992,02,02));
		listeNaissances.add(new GregorianCalendar(1993,03,03));
		
		listeStatus.add('U');
		listeStatus.add('U');
		listeStatus.add('U');
		
		for(int i = 0; i < listeNoms.size(); i++){
			Utilisateur user = new Utilisateur();
			user.setNom(listeNoms.get(i));
			user.setPrenom(listePrenoms.get(i));
			user.setMdp(listeMdp.get(i));
			user.setSexe(Sexe.Homme);
			user.setEmail(listeEmails.get(i));
			user.setDateNaissance(listeNaissances.get(i));
			user.setStatus(listeStatus.get(i));
			user = this.gestionnaireUtilisateur.ajouterUtilisateur(user);
			listeUser.add(user);
		}
	}
	
	@Test
	public void testAjouterAnnee(){
		List<Annee> liste = new ArrayList<Annee>();
		for(Annee a : listeAnnees){
			liste.add(this.gestionnaireAnnee.enregistrerAnnee(a));
		}
		Assert.assertEquals(this.gestionnaireAnnee.rechercherAnnee(2), liste.get(1));
	}
	
	@Test(dependsOnMethods={"testAjouterAnnee"})
	public void testListerAnnees(){
		List<Annee> liste = this.gestionnaireAnnee.listerAnnee();
		Assert.assertEquals(liste.size(), 9);
	}
	
	@Test(dependsOnMethods={"testListerAnnees"})
	public void testModifierAnnee(){
		Annee annee = this.gestionnaireAnnee.rechercherAnnee(1);
		annee.setNumClasse(5);
		annee.setEtudiants(null);
		Annee retour = this.gestionnaireAnnee.mettreAJourAnnee(annee);
		Assert.assertNotNull(retour);
		Assert.assertEquals(retour.getNumClasse(), 5);
	}
	
	@Test(dependsOnMethods={"testModifierAnnee"})
	public void testAjoutEtudiant(){
		Annee annee = this.gestionnaireAnnee.rechercherAnnee(1);
		for(Utilisateur u : listeUser){
			this.gestionnaireAnnee.ajouterEtudtiant(annee, u);
		}
		List<Utilisateur> listeUserAnnee = this.gestionnaireAnnee.listerEtudiants(annee);
		Assert.assertEquals(listeUserAnnee.size(), listeUser.size());
	}
	
	@Test(dependsOnMethods={"testAjoutEtudiant"})
	public void testModifEtudiant(){
		Annee annee = this.gestionnaireAnnee.rechercherAnnee(1);
		Annee annee2 = this.gestionnaireAnnee.rechercherAnnee(3);
		annee = this.gestionnaireAnnee.chargerTout(annee);
		annee2 = this.gestionnaireAnnee.chargerTout(annee2);
		Utilisateur user = annee.getEtudiants().get(1);
		annee2 = this.gestionnaireAnnee.modifierEtudiant(annee2, user);
		Assert.assertEquals(annee2.getEtudiants().get(0).getNom(), user.getNom());
	}
	
	@Test(dependsOnMethods={"testModifEtudiant"})
	public void testSupprimerAnnee(){
		Annee annee = this.gestionnaireAnnee.rechercherAnnee(1);
		this.gestionnaireAnnee.supprimerAnnee(annee);
		Assert.assertNull(this.gestionnaireAnnee.rechercherAnnee(annee.getIdAnnee()));
	}
}
