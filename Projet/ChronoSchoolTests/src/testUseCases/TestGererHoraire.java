package testUseCases;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererAnnee;
import usecases.GererCours;
import usecases.GererEvenement;
import usecases.GererHoraire;
import usecases.GererLocal;
import usecases.GererUtilisateur;
import domaine.Annee;
import domaine.Cours;
import domaine.CoursProfesseur;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Local;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestGererHoraire {
	
	private List<Horaire> listeHoraire;
	private List<Horaire> listeHoraireDB;
	private List<Evenement> listeEvenement;
	private List<Annee> listeAnnee;
	private List<Cours> listeCours;
	private List<Utilisateur> listeUtilisateur;
	private List<Local> listeLocal;
	
	private GererEvenement gererEvenement;
	private GererAnnee gererAnnee;
	private GererUtilisateur gererUtilisateur;
	private GererCours gererCours;
	private GererLocal gererLocal;
	
	private GererHoraire gererHoraire;
	
	@BeforeClass
	public void beforeClass() {
		
		Context jndi;
		try {
			jndi = new InitialContext();
			this.gererHoraire = (GererHoraire) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererHoraireImpl!usecases.GererHoraire");
			this.gererEvenement = (GererEvenement) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererEvenementImpl!usecases.GererEvenement");
			this.gererAnnee = (GererAnnee) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererAnneeImpl!usecases.GererAnnee");
			this.gererUtilisateur = (GererUtilisateur) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererUtilisateurImpl!usecases.GererUtilisateur");
			this.gererCours = (GererCours) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererCoursImpl!usecases.GererCours");
			this.gererLocal = (GererLocal) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererLocalImpl!usecases.GererLocal");
			jndi.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		//initialisation locaux
		this.listeLocal = new ArrayList<Local>();
		initLocal();
		
		//initialisation des annee
		this.listeAnnee = new ArrayList<Annee>(); 
		initAnnee();
		
		//initialisation utilisateur
		this.listeUtilisateur = new ArrayList<Utilisateur>();
		initUtilisateur();
		
		//initialisation des cours
		this.listeCours = new ArrayList<Cours>();
		initCours();
		
		//initialisation des horaire
		this.listeHoraireDB = new ArrayList<Horaire>();
		this.listeHoraire = new ArrayList<Horaire>();
		initHoraire();
		
		//initialisation des evenement
		this.listeEvenement = new ArrayList<Evenement>();
		initEvenement();
		
	}
	


	private void initLocal() {
		Local l1 = new Local();
		l1.setIntitule("local1");
		l1.setNbPlaces(20);
		l1 = this.gererLocal.enregistrer(l1);
		this.listeLocal.add(l1);
		
		Local l2 = new Local();
		l2.setIntitule("local2");
		l2.setNbPlaces(25);
		l2 = this.gererLocal.enregistrer(l2);
		this.listeLocal.add(l2);
		
		Local l3 = new Local();
		l3.setIntitule("local3");
		l3.setNbPlaces(30);
		l3 = this.gererLocal.enregistrer(l3);
		this.listeLocal.add(l3);
		
	}



	private void initAnnee() {
		Annee a1 = new Annee();
		a1.setNumClasse(1);
		a1.setSection("IG");
		a1 = gererAnnee.enregistrerAnnee(a1);
		this.listeAnnee.add(a1);
		
		Annee a2 = new Annee();
		a2.setNumClasse(2);
		a2.setSection("IG");
		a2 = gererAnnee.enregistrerAnnee(a2);
		this.listeAnnee.add(a2);
		
		Annee a3 = new Annee();
		a3.setNumClasse(1);
		a3.setSection("SMS");
		a3 = gererAnnee.enregistrerAnnee(a3);
		this.listeAnnee.add(a3);
		
	}



	private void initUtilisateur() {
		Utilisateur u1 = new Utilisateur();
		u1.setDateNaissance(new GregorianCalendar(1993,05,15));
		u1.setEmail("toto1@hotmail.com");
		u1.setMdp("toto1");
		u1.setNom("Dupond");
		u1.setPrenom("toto1");
		u1.setSexe(Sexe.Homme);
		u1.setStatus('B');
		u1 = gererUtilisateur.ajouterUtilisateur(u1);
		this.listeUtilisateur.add(u1);
		
		Utilisateur u2 = new Utilisateur();
		u2.setDateNaissance(new GregorianCalendar(1993,05,15));
		u2.setEmail("toto2@hotmail.com");
		u2.setMdp("toto2");
		u2.setNom("Dupond");
		u2.setPrenom("toto2");
		u2.setSexe(Sexe.Homme);
		u2.setStatus('B');
		u2 = gererUtilisateur.ajouterUtilisateur(u2);
		this.listeUtilisateur.add(u2);
		
		Utilisateur u3 = new Utilisateur();
		u3.setDateNaissance(new GregorianCalendar(1993,05,15));
		u3.setEmail("toto3@hotmail.com");
		u3.setMdp("toto3");
		u3.setNom("Dupond");
		u3.setPrenom("toto3");
		u3.setSexe(Sexe.Homme);
		u3.setStatus('B');
		u3 = gererUtilisateur.ajouterUtilisateur(u3);
		this.listeUtilisateur.add(u3);
	}



	private void initCours() {
		Cours c1 = new Cours();
		c1.setAnnee(this.listeAnnee.get(0));
		c1.setDescription("Etude des differentes technologie internet sur le marché");
		c1.setIntitule("Technologie internet");
		ArrayList<CoursProfesseur> listecp1 = new ArrayList<CoursProfesseur>();
		CoursProfesseur cp1 = new CoursProfesseur();
		cp1.setCours(c1);
		cp1.setProfesseur(this.listeUtilisateur.get(0));
		cp1.setTitulaire(true);
		listecp1.add(cp1);
		CoursProfesseur cp2 = new CoursProfesseur();
		cp2.setCours(c1);
		cp2.setProfesseur(this.listeUtilisateur.get(1));
		cp2.setTitulaire(false);
		listecp1.add(cp2);
		c1.setProfesseurs(listecp1);
		c1 = this.gererCours.ajouterCours(c1);
		this.listeCours.add(c1);
		
		Cours c2 = new Cours();
		c2.setAnnee(this.listeAnnee.get(1));
		c2.setDescription("Etude du réseau internet et interne");
		c2.setIntitule("Reseau");
		ArrayList<CoursProfesseur> listecp2 = new ArrayList<CoursProfesseur>();
		cp1 = new CoursProfesseur();
		cp1.setCours(c2);
		cp1.setProfesseur(this.listeUtilisateur.get(2));
		cp1.setTitulaire(true);
		listecp2.add(cp1);
		c2.setProfesseurs(listecp2);
		c2 = this.gererCours.ajouterCours(c2);
		this.listeCours.add(c2);

		Cours c3 = new Cours();
		c3.setAnnee(this.listeAnnee.get(2));
		c3.setDescription("Etude des differentes technologie internet sur le marché");
		c3.setIntitule("Technologie internet");
		ArrayList<CoursProfesseur> listecp3 = new ArrayList<CoursProfesseur>();
		cp1 = new CoursProfesseur();
		cp1.setCours(c3);
		cp1.setProfesseur(this.listeUtilisateur.get(1));
		cp1.setTitulaire(true);
		listecp3.add(cp1);
		c3.setProfesseurs(listecp3);
		c3 = this.gererCours.ajouterCours(c3);
		this.listeCours.add(c3);
	}

	private void initHoraire() {
		Horaire h1 = new Horaire();
		h1.setAnnee(listeAnnee.get(0));
		h1.setDateDebut(new GregorianCalendar(2013,9,01));
		h1.setDateFin(new GregorianCalendar(2014,6,30));
		this.listeHoraire.add(h1);
		
		Horaire h2 = new Horaire();
		h2.setAnnee(listeAnnee.get(1));
		h2.setDateDebut(new GregorianCalendar(2013,9,1));
		h2.setDateFin(new GregorianCalendar(2014,6,30));
		this.listeHoraire.add(h2);
		
		Horaire h3 = new Horaire();
		h3.setAnnee(listeAnnee.get(2));
		h3.setDateDebut(new GregorianCalendar(2013,9,1));
		h3.setDateFin(new GregorianCalendar(2014,6,30));
		this.listeHoraire.add(h3);
	}

	private void initEvenement() {
		// evenement pour horaire 1
		Evenement e1 = new Evenement();
		e1.setCours(this.listeCours.get(0));
		e1.setHeureDebut(new GregorianCalendar(2013, 10, 1, 8, 15));
		e1.setHeureFin(new GregorianCalendar(2013, 10, 1, 10, 15));
		e1.setHoraire(this.listeHoraire.get(0));
		e1.setIntitule("cours 1");
		ArrayList<Local> listeL1 = new ArrayList<Local>();
		listeL1.add(this.listeLocal.get(0));
		e1.setLocaux(listeL1);
		e1.setStatut('A');
		e1 = this.gererEvenement.enregistrerEvenement(e1);
		this.listeEvenement.add(e1);
		
		Evenement e2 = new Evenement();
		e2.setCours(this.listeCours.get(1));
		e2.setHeureDebut(new GregorianCalendar(2013, 10, 1, 10, 30));
		e2.setHeureFin(new GregorianCalendar(2013, 10, 1, 12, 30));
		e2.setHoraire(this.listeHoraire.get(0));
		e2.setIntitule("cours 2");
		ArrayList<Local> listeL2 = new ArrayList<Local>();
		listeL2.add(this.listeLocal.get(0));
		e2.setLocaux(listeL2);
		e2.setStatut('A');
		e2 = this.gererEvenement.enregistrerEvenement(e2);
		this.listeEvenement.add(e2);
		
		Evenement e3 = new Evenement();
		e3.setCours(this.listeCours.get(2));
		e3.setHeureDebut(new GregorianCalendar(2013, 10, 1, 13, 30));
		e3.setHeureFin(new GregorianCalendar(2013, 10, 1, 15, 30));
		e3.setHoraire(this.listeHoraire.get(0));
		e3.setIntitule("cours 3");
		ArrayList<Local> listeL3 = new ArrayList<Local>();
		listeL3.add(this.listeLocal.get(0));
		listeL3.add(this.listeLocal.get(1));
		e3.setLocaux(listeL3);
		e3.setStatut('A');
		e3 = this.gererEvenement.enregistrerEvenement(e3);
		this.listeEvenement.add(e3);
		
		// evenement pour horaire 2
		Evenement e4 = new Evenement();
		e4.setCours(this.listeCours.get(1));
		e4.setHeureDebut(new GregorianCalendar(2013, 10, 1, 8, 15));
		e4.setHeureFin(new GregorianCalendar(2013, 10, 1, 10, 15));
		e4.setHoraire(this.listeHoraire.get(1));
		e4.setIntitule("cours 1");
		ArrayList<Local> listeL4 = new ArrayList<Local>();
		listeL4.add(this.listeLocal.get(1));
		e4.setLocaux(listeL4);
		e4.setStatut('A');
		e4 = this.gererEvenement.enregistrerEvenement(e4);
		this.listeEvenement.add(e4);
		
		Evenement e5 = new Evenement();
		e5.setCours(this.listeCours.get(2));
		e5.setHeureDebut(new GregorianCalendar(2013, 11, 1, 10, 30));
		e5.setHeureFin(new GregorianCalendar(2013, 11, 1, 12, 30));
		e5.setHoraire(this.listeHoraire.get(1));
		e5.setIntitule("cours 2");
		ArrayList<Local> listeL5 = new ArrayList<Local>();
		listeL5.add(this.listeLocal.get(1));
		e5.setLocaux(listeL5);
		e5.setStatut('A');
		e5 = this.gererEvenement.enregistrerEvenement(e5);
		this.listeEvenement.add(e5);
		
		Evenement e6 = new Evenement();
		e6.setCours(this.listeCours.get(0));
		e6.setHeureDebut(new GregorianCalendar(2013, 11, 1, 13, 30));
		e6.setHeureFin(new GregorianCalendar(2013, 11, 1, 15, 30));
		e6.setHoraire(this.listeHoraire.get(1));
		e6.setIntitule("cours 3");
		ArrayList<Local> listeL6 = new ArrayList<Local>();
		listeL6.add(this.listeLocal.get(2));
		e6.setLocaux(listeL3);
		e6.setStatut('A');
		e6 = this.gererEvenement.enregistrerEvenement(e6);
		this.listeEvenement.add(e6);
		
		// evenement pour horaire 3
		Evenement e7 = new Evenement();
		e7.setCours(this.listeCours.get(2));
		e7.setHeureDebut(new GregorianCalendar(2013, 10, 1, 8, 15));
		e7.setHeureFin(new GregorianCalendar(2013, 10, 1, 10, 15));
		e7.setHoraire(this.listeHoraire.get(0));
		e7.setIntitule("cours 1");
		ArrayList<Local> listeL7 = new ArrayList<Local>();
		listeL7.add(this.listeLocal.get(2));
		e7.setLocaux(listeL7);
		e7.setStatut('A');
		e7 = this.gererEvenement.enregistrerEvenement(e7);
		this.listeEvenement.add(e7);
		
		Evenement e8 = new Evenement();
		e8.setCours(this.listeCours.get(0));
		e8.setHeureDebut(new GregorianCalendar(2013, 10, 1, 10, 30));
		e8.setHeureFin(new GregorianCalendar(2013, 10, 1, 12, 30));
		e8.setHoraire(this.listeHoraire.get(0));
		e8.setIntitule("cours 2");
		ArrayList<Local> listeL8 = new ArrayList<Local>();
		listeL8.add(this.listeLocal.get(2));
		e8.setLocaux(listeL8);
		e8.setStatut('A');
		e8 = this.gererEvenement.enregistrerEvenement(e8);
		this.listeEvenement.add(e8);
		
		Evenement e9 = new Evenement();
		e9.setCours(this.listeCours.get(1));
		e9.setHeureDebut(new GregorianCalendar(2013, 12, 1, 13, 30));
		e9.setHeureFin(new GregorianCalendar(2013, 12, 1, 15, 30));
		e9.setHoraire(this.listeHoraire.get(0));
		e9.setIntitule("cours 3");
		ArrayList<Local> listeL9 = new ArrayList<Local>();
		listeL9.add(this.listeLocal.get(0));
		listeL9.add(this.listeLocal.get(1));
		e9.setLocaux(listeL9);
		e9.setStatut('A');
		e9 = this.gererEvenement.enregistrerEvenement(e9);
		this.listeEvenement.add(e9);
		
	}



	// debut des test
	@Test
	public void ajouterHoraire() {
		Horaire retour;
		for (Horaire horaire : this.listeHoraire) {
			retour = this.gererHoraire.ajouterHoraire(horaire);
			this.listeHoraireDB.add(retour);
			Assert.assertNotNull(retour);
		}
		
		// test ajout horaire invalide
		Assert.assertNull(this.gererHoraire.ajouterHoraire(new Horaire()));
		Horaire horaireInvalide = new Horaire();
		horaireInvalide.setAnnee(this.listeAnnee.get(0));
		horaireInvalide.setDateDebut(new GregorianCalendar(2013,6,1));
		horaireInvalide.setDateFin(new GregorianCalendar(2011,9,30));
		Assert.assertNull(this.gererHoraire.ajouterHoraire(horaireInvalide));
	}
	
	@Test(dependsOnMethods={"ajouterHoraire"})
	public void getHoraire(){
		for (Horaire horaire : this.listeHoraireDB) {
			Horaire retour;
			retour = this.gererHoraire.rechercherHoraire(horaire);
			Assert.assertNotNull(retour);
			Assert.assertEquals(horaire, retour);
		}
		Assert.assertNull(this.gererHoraire.rechercherHoraire(new Horaire()));
	}
	
	@Test(dependsOnMethods={"ajouterHoraire"})
	public void getHoraireParSemaineAnnee(){
		Horaire retour = this.gererHoraire.getSemaineAnnee(this.listeAnnee.get(0), new GregorianCalendar(2013, 10, 1));
		Assert.assertNotNull(retour);
		Assert.assertEquals(retour, this.listeHoraireDB.get(0));
		// demande d'un horaire inexistant
		Assert.assertNull(this.gererHoraire.getSemaineAnnee(this.listeAnnee.get(0), new GregorianCalendar(1999, 10, 1)));
	}
	
	@Test(dependsOnMethods={"ajouterHoraire"})
	public void getHoraireParSemaineProfesseur(){
		Horaire retour = this.gererHoraire.getSemaineProf(this.listeUtilisateur.get(0), new GregorianCalendar(2013, 10, 1));
		Assert.assertNotNull(retour);
		// demande d'un horaire inexistant
		Assert.assertNull(this.gererHoraire.getSemaineProf(this.listeUtilisateur.get(0), new GregorianCalendar(1999, 10, 1)));
	}
	
	@Test(dependsOnMethods={"getHoraire, getHoraireParSemaineAnnee, getHoraireParSemaineProfesseur"})
	public void modifierHoraire(){
		
		Horaire horaire = this.gererHoraire.rechercherHoraire(this.listeHoraireDB.get(0));
		horaire.setAnnee(this.listeAnnee.get(2));
		horaire = this.gererHoraire.modifierHoraire(horaire);
		Assert.assertNotNull(horaire);
		// horaire inexistant
		Assert.assertNull(this.gererHoraire.modifierHoraire(new Horaire()));
	}

	@Test(dependsOnMethods={"modifierHoraire"})
	public void supprimerHoraire(){
		for (Horaire horaire : this.listeHoraireDB) {
			this.gererHoraire.supprimerHoraire(horaire);
			Assert.assertNull(this.gererHoraire.rechercherHoraire(horaire));
		}
	}
  

	@AfterClass
	public void afterClass() {
		
	}

}