package testUseCases;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererAnnee;
import usecases.GererCours;
import usecases.GererUtilisateur;
import domaine.Annee;
import domaine.Cours;
import domaine.CoursProfesseur;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestGererCours {
	
	private GererCours gererCours;
	private GererUtilisateur ucUtilisateur;
	private GererAnnee ucAnnee;
	private ArrayList<Utilisateur> users;
	private ArrayList<Annee> annees;
	private ArrayList<Cours> cours;

	@BeforeClass
	public void initialiser(){
		
		this.users = new ArrayList<Utilisateur>();
		this.annees = new ArrayList<Annee>();
		this.cours = new ArrayList<Cours>();
		
		Context jndi;
		try {
			jndi = new InitialContext();
			this.gererCours = (GererCours) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererCoursImpl!usecases.GererCours");
			this.ucUtilisateur = (GererUtilisateur) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererUtilisateurImpl!usecases.GererUtilisateur");
			this.ucAnnee = (GererAnnee) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererAnneeImpl!usecases.GererAnnee");
			jndi.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		Utilisateur p1 = new Utilisateur();
		p1.setNom("Pollart");
		p1.setDateNaissance(new GregorianCalendar(1990,05,15));
		p1.setEmail("michel.pollart@helha.be");
		p1.setMdp("michel");
		p1.setPrenom("Michel");
		p1.setSexe(Sexe.Homme);
		p1.setStatus('P');
		p1 = this.ucUtilisateur.ajouterUtilisateur(p1);
		this.users.add(p1);
		
		Utilisateur p2 = new Utilisateur();
		p2.setNom("Leagrand");
		p2.setDateNaissance(new GregorianCalendar(1990,05,15));
		p2.setEmail("olivier.legrand@helha.be");
		p2.setMdp("olivier");
		p2.setPrenom("Olivier");
		p2.setSexe(Sexe.Homme);
		p2.setStatus('P');
		p2 = this.ucUtilisateur.ajouterUtilisateur(p2);
		this.users.add(p2);
		
		Annee a1 = new Annee();
		a1.setNumClasse(1);
		a1.setSection("IG");
		a1=this.ucAnnee.enregistrerAnnee(a1);
		this.annees.add(a1);
		
		Annee a2 = new Annee();
		a2.setNumClasse(2);
		a2.setSection("IG");
		a2=this.ucAnnee.enregistrerAnnee(a2);
		this.annees.add(a2);
	}

	@Test
	public void ajouter() {
		ArrayList<CoursProfesseur>coursProfesseur = new ArrayList<CoursProfesseur>();
		Cours cours1 = new Cours();
		cours1.setIntitule("Mathématique");
		cours1.setDescription("Donné par Monsieur Pollart");
		CoursProfesseur cp = new CoursProfesseur();
		cp.setCours(cours1);
		cp.setProfesseur(this.users.get(0));
		cp.setTitulaire(true);
		coursProfesseur.add(cp);
		cp = new CoursProfesseur();
		cp.setCours(cours1);
		cp.setProfesseur(this.users.get(1));
		cp.setTitulaire(false);
		coursProfesseur.add(cp);
		cours1.setAnnee(this.annees.get(0));
		cours1.setProfesseurs(coursProfesseur);
		cours1 = this.gererCours.ajouterCours(cours1);
		Assert.assertNotNull(cours1);
		Assert.assertNotEquals(cours1.getIdCours(),0);
		this.cours.add(cours1);
	}
	
	@Test(dependsOnMethods={"ajouter"})
	public void getCours(){
		Cours cours = this.gererCours.rechercherCours(this.cours.get(0));
		Assert.assertNotNull(cours);
	}
	
	@Test(dependsOnMethods={"getCours"})
	public void modifierCours(){
		Cours cours = this.cours.get(0);
		cours = this.gererCours.rechercherCours(cours);
		
		cours.setDescription("Vlegede");
		cours.setIntitule("Circonvolutions");
		cours = this.gererCours.modifierCours(cours);
		Assert.assertNotNull(cours);
		
		cours = this.gererCours.rechercherCours(cours);
		Assert.assertEquals(cours.getDescription(), "Vlegede");
	}
	
	@Test(dependsOnMethods={"modifierCours"})
	public void supprimerCours(){
		this.gererCours.supprimerCours(this.cours.get(0));
		
		Cours cours = this.gererCours.rechercherCours(this.cours.get(0));
		Assert.assertNull(cours);
	}
}
