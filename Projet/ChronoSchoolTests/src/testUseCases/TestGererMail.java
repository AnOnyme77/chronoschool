package testUseCases;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.annotations.Test;

import usecases.GererMail;
import usecases.GererUtilisateur;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestGererMail {
  @Test
  public void f() {
	  Context jndi;
	  GererMail gestionnaire = null;
	  GererUtilisateur gestionnaireUtili = null;
		try {
			jndi = new InitialContext();
			 gestionnaire = (GererMail) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererMailImpl!usecases.GererMail");
			 gestionnaireUtili = (GererUtilisateur) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererUtilisateurImpl!usecases.GererUtilisateur");
			jndi.close();
		} catch (NamingException e) { 
			e.printStackTrace();
		}
		
		Utilisateur utili = new Utilisateur();
		utili.setDateNaissance(new GregorianCalendar(1993,04,15));
		utili.setEmail("anonyme77.dev4u@gmail.com");
		utili.setMdp("coucou");
		utili.setNom("EVrard");
		utili.setPrenom("Marie");
		utili.setSexe(Sexe.Femme);
		utili.setStatus('U');
		
		gestionnaireUtili.ajouterUtilisateur(utili);
		
		utili  = gestionnaireUtili.rechercherUtilisateur("evrardmarie77@gmail.com");
		
		ArrayList<Utilisateur> dest = new ArrayList<Utilisateur>();
		dest.add(utili);
		
		gestionnaire.envoyer("coucou", "tvvmb", dest);
	  
  }
}
