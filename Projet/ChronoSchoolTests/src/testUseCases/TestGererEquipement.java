package testUseCases;

import static org.testng.AssertJUnit.assertNotNull;

import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererEquipement;
import domaine.Equipement;

public class TestGererEquipement {
	
	private ArrayList<Equipement> listeEquipement;
	private ArrayList<Equipement> listeEquipementDB;	
	
	GererEquipement gereEquipement;
	
	@BeforeClass
	public void init(){
		Context jndi;
		try {
			jndi = new InitialContext();
			gereEquipement = (GererEquipement) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererEquipementImpl!usecases.GererEquipement");
			jndi.close();
		} catch (NamingException e) { 
			e.printStackTrace();
		}
		
		Equipement equipement = new Equipement();
		equipement.setDescription("Projecteur capable de projeter sur un éléphant indonésien");
		equipement.setIntitule("Projecteur haute performance");
		
		this.listeEquipement = new ArrayList<Equipement>();
			listeEquipement.add(equipement);
		
		equipement = new Equipement();
			equipement.setDescription("PC capable de lancer démineur sans planter !");
			equipement.setIntitule("Ordinateur");
			
			listeEquipement.add(equipement);
			
		equipement = new Equipement();
			equipement.setDescription("Chaise capable de porter un Somalien de 2kg!");
			equipement.setIntitule("Chaise");
			
			listeEquipement.add(equipement);
		
	}
	
	@Test
	public void testAjouterEquipement(){
		Equipement equipementDB;
		this.listeEquipementDB = new ArrayList<Equipement>();
		for (Equipement equipement : this.listeEquipement)
		{
			equipementDB = this.gereEquipement.ajouterEquipement(equipement);
			this.listeEquipementDB.add(equipementDB);
			assertNotNull(equipementDB);
		}
		Assert.assertNull(this.gereEquipement.ajouterEquipement(new Equipement()));
	}
	
	@Test(dependsOnMethods={"testAjouterEquipement"})
	public void testModifierEquipement(){
		Equipement retour = null;
		Equipement equipement = this.gereEquipement.rechercherEquipement(this.listeEquipementDB.get(0));

		equipement.setDescription("projecteur qui projete des chevres.");
		this.gereEquipement.modifierEquipement(equipement);
		
		retour = this.gereEquipement.rechercherEquipement(this.listeEquipementDB.get(0));
		
		Assert.assertEquals(retour.getDescription(), equipement.getDescription());
	}
	
	@Test(dependsOnMethods={"testModifierEquipement"})
	public void testListerEquipement(){
		assertNotNull(this.gereEquipement.listerEquipement());
		Assert.assertTrue(this.listeEquipementDB.size()<= this.gereEquipement.listerEquipement().size());
	}
	
	@Test(dependsOnMethods={"testListerEquipement"})
	public void testSupprimerEquipement(){
		int taille; 
		int tailleAncienne = this.gereEquipement.listerEquipement().size();
		this.gereEquipement.supprimerEquipement(this.listeEquipementDB.get(0));
		this.listeEquipementDB.remove(0);
		taille = this.gereEquipement.listerEquipement().size();

		Assert.assertTrue(taille<tailleAncienne);
		
		for(Equipement equipement : this.listeEquipementDB){
			this.gereEquipement.supprimerEquipement(equipement);
		}
	}
}
