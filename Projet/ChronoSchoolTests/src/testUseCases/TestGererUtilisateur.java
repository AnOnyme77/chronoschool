package testUseCases;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import usecases.GererUtilisateur;
import domaine.Sexe;
import domaine.Utilisateur;

public class TestGererUtilisateur {
	
	private GererUtilisateur gestionnaire;
	
	private List<String> noms;
	private List<String> prenoms;
	private List<String> mdps;
	private List<String> emails;
	private List<Calendar> dateNaissances;
	private List<Character> listeStatus;
	
	private List<Utilisateur> utilisateurs;
	
	@BeforeClass
	public void init(){
		Context jndi;
		try {
			jndi = new InitialContext();
			gestionnaire = (GererUtilisateur) jndi.lookup("ejb:ChronoSchool/ChronoSchoolEJB/GererUtilisateurImpl!usecases.GererUtilisateur");
			jndi.close();
		} catch (NamingException e) { 
			e.printStackTrace();
		}
		
		noms = new ArrayList<String>();
		prenoms = new ArrayList<String>();
		mdps = new ArrayList<String>();
		emails = new ArrayList<String>();
		dateNaissances = new ArrayList<Calendar>();
		listeStatus = new ArrayList<Character>();
		
		noms.add("Carly");
		noms.add("Evrard");
		noms.add("DelaJungle");
		
		prenoms.add("Sébastien");
		prenoms.add("Laurent");
		prenoms.add("Nigel");
		
		mdps.add("MDP1");
		mdps.add("MDP2");
		mdps.add("MDP3");
		
		emails.add("emailSeb@gmail.com");
		emails.add("emailLaurent@gmail.com");
		emails.add("email");
		
		dateNaissances.add(new GregorianCalendar(1994, 03, 17));
		dateNaissances.add(new GregorianCalendar(2025,12,15));
		dateNaissances.add(new GregorianCalendar());
		
		listeStatus.add('E');
		listeStatus.add('P');
		listeStatus.add('A');
		
		utilisateurs = new ArrayList<Utilisateur>();
		
		for(int i = 0; i < noms.size(); i++){
			Utilisateur user = new Utilisateur();
			user.setNom(noms.get(i));
			user.setPrenom(prenoms.get(i));
			user.setMdp(mdps.get(i));
			user.setSexe(Sexe.Homme);
			user.setEmail(emails.get(i));
			user.setDateNaissance(dateNaissances.get(i));
			user.setStatus(listeStatus.get(i));
			utilisateurs.add(user);
		}
		
	}
	
	@Test
	public void testAjouterUtilisateur(){
		Utilisateur userA = gestionnaire.ajouterUtilisateur(utilisateurs.get(0));
		Utilisateur userB = gestionnaire.ajouterUtilisateur(utilisateurs.get(1));
		Utilisateur userC = gestionnaire.ajouterUtilisateur(utilisateurs.get(2));
		
		assertNotNull(userA);
		assertTrue(userB == null);//date naissance invalide
		assertTrue(userC == null);//email et date naissance invalides
	}
	
	@Test(dependsOnMethods={"testAjouterUtilisateur"})
	public void testModifierUtilisateur(){
		Utilisateur user1 = gestionnaire.rechercherUtilisateur(utilisateurs.get(0).getEmail());
		user1.setPrenom("Jack");
		Utilisateur userA = gestionnaire.modifierUtilisateur(user1);
		
		assertEquals("Jack", userA.getPrenom());
	}
	
	@Test(dependsOnMethods={"testModifierUtilisateur"})
	public void testListerUtilisateurs(){
		List<Utilisateur> liste = gestionnaire.listerTousUtilisateurs();
		assertTrue(liste.size() == 1);
	}
	
	@Test(dependsOnMethods={"testListerUtilisateurs"})
	public void testSupprimerUtilisateur(){
		Utilisateur user = gestionnaire.listerTousUtilisateurs().get(0);
		gestionnaire.supprimerUtilisateur(user.getIdUtilisateur());
		assertTrue(gestionnaire.listerTousUtilisateurs().size() == 0);
	}
}
