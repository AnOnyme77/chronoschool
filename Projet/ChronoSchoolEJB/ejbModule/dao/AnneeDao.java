package dao;

import java.util.List;

import javax.ejb.Remote;

import domaine.Annee;
import domaine.Utilisateur;
@Remote
public interface AnneeDao extends Dao<Integer, Annee> {
	List<Annee> lister();
	Annee rechercher(Integer idAnnee);
	Annee enregistrer(Annee annee);
	Annee mettreAJour(Annee annee);
	void supprimer(Integer idAnnee);
	void supprimer(Annee annee);
	Annee ajouterEtudiant(Annee annee, Utilisateur etudiant);
	Annee modifierEtudiant(Annee annee, Utilisateur etudiant);
	void supprimerEtudiant(Annee annee, Utilisateur etudiant);
	List<Utilisateur> listerEtudiants(Annee annee);
	Annee chargerTout(Annee annee);
}