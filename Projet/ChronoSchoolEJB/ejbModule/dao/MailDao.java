package dao;

import javax.ejb.Remote;

import domaine.Mail;
@Remote
public interface MailDao extends Dao<Integer, Mail> {

}
