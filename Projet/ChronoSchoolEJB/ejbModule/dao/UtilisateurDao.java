package dao;

import java.util.List;

import javax.ejb.Remote;

import domaine.Utilisateur;
@Remote
public interface UtilisateurDao extends Dao<Integer, Utilisateur> {
	
	Utilisateur enregistrer(Utilisateur user);
	List<Utilisateur> lister();
	Utilisateur mettreAJour(Utilisateur user);
	Utilisateur rechercher(int idUtilisateur);
	Utilisateur rechercher(String email);
	void supprimer(int idUtilisateur);
	List<Utilisateur> listerProfesseur();
	List<Utilisateur> listerEtudiant();
	Utilisateur chargerTout(Utilisateur user);

}
