package dao;

import java.util.List;

import javax.ejb.Remote;

import domaine.Cours;
@Remote
public interface CoursDao extends Dao<Integer, Cours> {
	
	Cours enregistrer(Cours cours);
	List<Cours> lister();
	List<Cours> listerP(int idProfesseur);
	List<Cours> listerA(int idAnnee);
	Cours mettreAJour(Cours cours);
	Cours rechercher(int idCours);
	void supprimer(int idCours);
	Cours chargerTout(int idCours);
}
