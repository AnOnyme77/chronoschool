package dao;

import java.util.List;

import javax.ejb.Remote;

import domaine.CoursProfesseur;
@Remote
public interface CoursProfesseurDao extends Dao<Integer, CoursProfesseur> {	
	CoursProfesseur enregistrer(CoursProfesseur coursProfesseur);
	List<CoursProfesseur> lister();
	List<CoursProfesseur> listerParCours(int idCours);
	List<CoursProfesseur> listerParProfesseur(int idProfesseur);
 	CoursProfesseur mettreAJour(CoursProfesseur coursProfesseur);
	CoursProfesseur rechercher(int idCoursProfesseur);
	void supprimerCP(int idCoursProfesseur);
}
