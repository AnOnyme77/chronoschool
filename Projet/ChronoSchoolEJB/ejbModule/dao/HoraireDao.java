package dao;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Remote;

import domaine.Annee;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Utilisateur;
@Remote
public interface HoraireDao extends Dao<Integer, Horaire> {
	Horaire getSemaineAnnee(Annee annee, Calendar dateDebutSemaine);
	Horaire getSemaineProf(Utilisateur utilisateur, Calendar dateDebutSemaine);
	List<Evenement> getEvenementType(Horaire horaire);
	Horaire chargerTout(Horaire horaire);
	List<Horaire> getHorairesAnnee(Annee annee);
	List<Horaire> listerHorairesActif();
}
