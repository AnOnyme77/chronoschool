package dao;

import javax.ejb.Remote;

import domaine.Equipement;

@Remote
public interface EquipementDao extends Dao<Integer, Equipement> {
	public Equipement rechercherIntitule(String intitule);
	public Equipement chargerTout(int id);
}
