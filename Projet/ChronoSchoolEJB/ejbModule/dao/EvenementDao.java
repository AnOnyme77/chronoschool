package dao;

import java.util.List;

import javax.ejb.Remote;

import domaine.Evenement;

@Remote
public interface EvenementDao extends Dao<Integer, Evenement> {

	List<Evenement> listerAvecParams(String queryString, Object[] params);
	List<Evenement> listerParProfesseur(int idUtilisateur);
	List<Evenement> listerParAnnee(int idAnnee);
	List<Evenement> listerParLocal(int idLocal);
	Evenement chargerTout(Evenement evenement);
	Evenement mettreAJour(Evenement evenement);
	Evenement rechercher(Integer idEvenement);
}
