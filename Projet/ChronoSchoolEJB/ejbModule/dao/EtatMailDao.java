package dao;

import java.util.List;

import javax.ejb.Remote;

import domaine.EtatMail;

@Remote
public interface EtatMailDao extends Dao<Integer, EtatMail> {
	public List<EtatMail> listeAEnvoyer();
	public List<EtatMail> listerDate();
}
