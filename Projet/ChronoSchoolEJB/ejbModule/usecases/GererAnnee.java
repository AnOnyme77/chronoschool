package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Annee;
import domaine.Utilisateur;

@Remote
public interface GererAnnee {
	List<Annee> listerAnnee();
	Annee rechercherAnnee(Integer idAnnee);
	Annee enregistrerAnnee(Annee annee);
	Annee mettreAJourAnnee(Annee annee);
	void supprimerAnnee(Integer idAnnee);
	void supprimerAnnee(Annee annee);
	Annee ajouterEtudtiant(Annee annee, Utilisateur etudiant);
	Annee modifierEtudiant(Annee annee, Utilisateur etudiant);
	void supprimerEtudiant(Annee annee, Utilisateur etudiant);
	Annee chargerTout(Annee annee);
	List<Utilisateur> listerEtudiants(Annee annee);
}
