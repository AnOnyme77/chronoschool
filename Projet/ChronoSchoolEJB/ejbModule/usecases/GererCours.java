package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Cours;

@Remote
public interface GererCours {
	
	Cours ajouterCours(Cours cours);
	Cours modifierCours(Cours cours);
	List<Cours> listerTousCours();
	List<Cours> listerCoursParProfesseur(int idProfesseur);
	List<Cours> listerCoursParAnnee(int idAnnee);
	void supprimerCours(Cours cours);
	Cours rechercherCours(Cours cours);
	String getMessage();
	Cours chargerTout(Cours cours);
}
