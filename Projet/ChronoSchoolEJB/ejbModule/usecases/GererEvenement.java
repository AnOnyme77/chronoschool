package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Annee;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Local;
import domaine.Utilisateur;

@Remote
public interface GererEvenement {
	List<Evenement> listerAvecParams(String queryString, Object[] params);
	List<Evenement> listerEvenementParLocal(Local local);
	List<Evenement> listerEvenementParAnnee(Annee annee);
	List<Evenement> listerEvenementParProfesseur(Utilisateur utilisateur);
	Evenement rechercherEvenement(int idEvenement);
	Evenement mettreAJourEvenement(Evenement evenement);
	Evenement chargerTout(Evenement evenement);
	Evenement enregistrerEvenement(Evenement evenement);
	void supprimerEvenement(Evenement evenement);
	boolean chevaucheTemps(Horaire horaire, Evenement evenement);
}

