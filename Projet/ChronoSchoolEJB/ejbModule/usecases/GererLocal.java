package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Equipement;
import domaine.Local;

@Remote
public interface GererLocal {
	Local enregistrer(Local local);
	Local rechercher(String intitule);
	Local rechercher(int id);
	List<Local> lister();
	Local mettreAJour(Local local);
	List<Equipement> listerEquipement(Local local);
	Local ajouterEquipement(Local local, Equipement equipement);
	Local modifierEquipement(Local local, Equipement equipement);
	void supprimerEquipement(Local local, Equipement equipement);
	Local chargerTout(Local local);
	void supprimer(Local local);
	void supprimer(int id);
}
