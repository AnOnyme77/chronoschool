package usecases;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Remote;

import domaine.Annee;
import domaine.Horaire;
import domaine.Utilisateur;

@Remote
public interface GererHoraire {
	public Horaire ajouterHoraire(Horaire horaire);
	/**
	 * @param horaire : l'horaire a modifier
	 * @param estComplet : true s'il s'agit d'un horaire complet<br>
	 * 					   false s'il s'agit d'un horaire type
	 * @return l'horaire modifié en cas de réussite, null autrement.
	 */
	public Horaire modifierHoraire(Horaire horaire, boolean estComplet);
	public void supprimerHoraire(Horaire horaire);
	public List<Horaire> listerHoraire();
	public List<Horaire> listerHorairesActifs();
	public Horaire getSemaineProf(Utilisateur utilisateur, Calendar dateDebut);
	public Horaire getSemaineAnnee(Annee annee, Calendar dateDebut);
	public Horaire rechercherHoraire(Horaire horaire);
	public Horaire chargerTout(Horaire horaire);
	public GregorianCalendar premierApresDebut(Calendar debut, Calendar jour);
	public GregorianCalendar premierLundiApresDebut(Calendar debut);
	public String getMessage();
	List<Horaire> getHorairesAnnee(Annee annee);
	void supprimerHoraireLogique(Horaire horaire);
	void modifierStatutHoraire(char statut,Horaire horaire);
}
