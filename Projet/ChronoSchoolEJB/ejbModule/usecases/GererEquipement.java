package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Equipement;

@Remote
public interface GererEquipement {
	public boolean existe(Equipement equipement);
	public Equipement ajouterEquipement(Equipement equipement);
	public Equipement modifierEquipement(Equipement equipement);
	public Equipement supprimerEquipement(Equipement equipement);
	public List<Equipement> listerEquipement();
	public Equipement rechercherEquipement(Equipement equipement);
	public Equipement rechercherIntitule(Equipement equipement);
	public Equipement chargerTout(Equipement equipement);
}
