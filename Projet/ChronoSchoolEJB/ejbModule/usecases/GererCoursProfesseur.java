package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.CoursProfesseur;

@Remote
public interface GererCoursProfesseur {
	CoursProfesseur ajouterCoursProfesseur(CoursProfesseur coursProfesseur);
	CoursProfesseur modifierCoursProfesseur(CoursProfesseur coursProfesseur);
	List<CoursProfesseur> listerTousCoursProfesseur();
	List<CoursProfesseur> listerCoursProfesseurParProfesseur(CoursProfesseur coursProfesseur);
	List<CoursProfesseur> listerCoursProfesseurParCours(CoursProfesseur coursProfesseur);
	void supprimerCoursProfesseur(CoursProfesseur coursProfesseur);
	CoursProfesseur rechercherCoursProfesseur(CoursProfesseur coursProfesseur);
	String getMessage();
}
