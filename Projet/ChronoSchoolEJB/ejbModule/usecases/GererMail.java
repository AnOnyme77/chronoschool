package usecases;

import java.util.List;

import javax.ejb.Remote;
import javax.mail.MessagingException;

import domaine.EtatMail;
import domaine.Utilisateur;

@Remote
public interface GererMail {
	
	public void envoyer(String titre, String contenu, List<Utilisateur> destinataires);

	public List<EtatMail> listerAEnvoyer();

	public void marqueEnvoye(EtatMail email);

	public void marqueErreur(EtatMail email);
	
	public List<EtatMail> listerDate();
	
	public void envoyerSync(String from, String to, String titre, String contenu) throws MessagingException;
}
