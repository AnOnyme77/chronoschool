package usecases;

import javax.ejb.Remote;

import domaine.Cours;
import domaine.Evenement;
import domaine.Local;

@Remote
public interface GererDisponibilite {
	boolean disponibiliteLocal(Evenement evenement);
	boolean disponibiliteAnnee(Evenement evenement);
	boolean disponibiliteProfesseur(Evenement evenement);
	boolean disponibiliteEvenement(Evenement evenement);
	boolean equipementLocal (Local local, Cours cours);
}
