package usecases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Utilisateur;

@Remote
public interface GererUtilisateur {
	
	Utilisateur ajouterUtilisateur(Utilisateur user);
	Utilisateur modifierUtilisateur(Utilisateur user);
	List<Utilisateur> listerTousUtilisateurs();
	List<Utilisateur> listerTousProfesseur();
	List<Utilisateur> listerTousEtudiant();
	void supprimerUtilisateur(int idUser);
	Utilisateur connecterUtilisateur(String email, String mdp);
	boolean mdpOublie(String email);
	boolean existe(String email);
	Utilisateur rechercherUtilisateur(String email);
	Utilisateur chercherUtilisateur(int idUtilisateur);
	String getMessage();
	Utilisateur chargerTout(Utilisateur user);
}
