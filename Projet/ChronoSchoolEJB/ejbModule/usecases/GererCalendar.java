package usecases;

import java.io.IOException;
import java.util.List;

import javax.ejb.Remote;

import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;

import domaine.Evenement;
import domaine.Horaire;

@Remote
public interface GererCalendar {
	public List<Event> convertirEvenements(List<Evenement> evenements);
	/**
	 * Cree un calendrier sur google agenda et renvoie l'id de ce calendrier
	 * @param horaire : l'horaire a transformer en calendrier
	 * @return l'id du calendrier
	 * @throws IOException
	 */
	public String creerCalendar(Horaire horaire) throws IOException;
	/**
	 * A appeler avant toute autre opération
	 * @throws IOException
	 */
	public void setUp() throws IOException;
	/**
	 * @param horaire : l'horaire à ajouter
	 * @param isComplet : true si l'horaire est un horaire complet<br>
	 * 					  false s'il s'agit d'un horaire type
	 * @return
	 */
	public Horaire ajouterCalendar(Horaire horaire, boolean isComplet);
	public Horaire mettreAJourCalendar(Horaire horaire);
	public boolean supprimerCalendar(Horaire horaire);
	/**
	 * ATTENTION, CETTE METHODE SUPPRIME L'INTEGRALITE DES EVENEMENTS ET CALENDRIERS ENREGISTRES
	 * ELLE PREND ENORMEMENT DE TEMPS A S'EFFECTUER
	 * @throws IOException
	 */
	public void clearAll() throws IOException;
	public String getMessage();
}
