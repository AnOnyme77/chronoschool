package domaine;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="chronoschool")
public class Mail implements  Serializable{
	
	private static final long serialVersionUID = 5186413198381949154L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idMail;
	
	@Column(nullable=false)
	private String titre;
	
	@Column(nullable=false)
	private String contenu;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Calendar dateAjout;	

	
	public Mail() {
		super();
	}

	
	
	public Mail(int idMail, String titre, String contenu, Calendar dateAjout) {
		super();
		this.idMail = idMail;
		this.titre = titre;
		this.contenu = contenu;
		this.dateAjout = dateAjout;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contenu == null) ? 0 : contenu.hashCode());
		result = prime * result
				+ ((dateAjout == null) ? 0 : dateAjout.hashCode());
		result = prime * result + idMail;
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mail other = (Mail) obj;
		if (contenu == null) {
			if (other.contenu != null)
				return false;
		} else if (!contenu.equals(other.contenu))
			return false;
		if (dateAjout == null) {
			if (other.dateAjout != null)
				return false;
		} else if (!dateAjout.equals(other.dateAjout))
			return false;
		if (idMail != other.idMail)
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}



	public int getIdMail() {
		return idMail;
	}

	public void setIdMail(int idMail) {
		this.idMail = idMail;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Calendar getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(Calendar dateAjout) {
		this.dateAjout = dateAjout;
	}

	
	
}
