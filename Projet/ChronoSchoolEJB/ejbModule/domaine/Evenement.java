package domaine;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="chronoschool")
public class Evenement implements Serializable{

	private static final long serialVersionUID = -4467835355914877543L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idEvenement;
	
	
	@Column(nullable=false)
	private String intitule;
	
	@Column()
	private String Description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Calendar heureDebut;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Calendar heureFin;
	
	@Column(nullable=false)
	private char statut;
	
	@ManyToOne()
	private Horaire horaire;
	
	@ManyToOne
	private Cours cours;
	
	@ManyToMany()
	@JoinTable(schema="chronoschool")
	private List<Local> locaux;

	public Evenement() {
		super();
	}

	public Evenement(int idEvenement, String intitule,
			String description, Calendar heureDebut, Calendar heureFin) {
		super();
		this.idEvenement = idEvenement;
		this.intitule = intitule;
		Description = description;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
	}

	
	
	public Evenement(int idEvenement,  String intitule,
			String description, Calendar heureDebut, Calendar heureFin,
			Horaire horaire, Cours cours, List<Local> locaux) {
		super();
		this.idEvenement = idEvenement;
		this.intitule = intitule;
		Description = description;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.horaire = horaire;
		this.cours = cours;
		this.locaux = locaux;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((Description == null) ? 0 : Description.hashCode());
		result = prime * result
				+ ((heureDebut == null) ? 0 : heureDebut.hashCode());
		result = prime * result
				+ ((heureFin == null) ? 0 : heureFin.hashCode());
		result = prime * result + idEvenement;
		result = prime * result
				+ ((intitule == null) ? 0 : intitule.hashCode());
		result = prime * result + statut;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evenement other = (Evenement) obj;
		if (Description == null) {
			if (other.Description != null)
				return false;
		} else if (!Description.equals(other.Description))
			return false;
		if (heureDebut == null) {
			if (other.heureDebut != null)
				return false;
		} else if (!(heureDebut.getTimeInMillis() == other.heureDebut.getTimeInMillis()))
			return false;
		if (heureFin == null) {
			if (other.heureFin != null)
				return false;
		} else if (!(heureFin.getTimeInMillis() == other.heureFin.getTimeInMillis()))
			return false;
		if (horaire == null) {
			if (other.horaire != null)
				return false;
		} else if (!horaire.equals(other.horaire))
			return false;
		if (idEvenement != other.idEvenement)
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		if (statut != other.statut)
			return false;
		return true;
	}
	
	/**
	 * Permet la comparaison sur base de l'intitulé, la description, les heures de debut et de fin
	 */
	public boolean equalsLow(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evenement other = (Evenement) obj;
		if (Description == null) {
			if (other.Description != null)
				return false;
		} else if (!Description.equals(other.Description))
			return false;
		if (heureDebut == null) {
			if (other.heureDebut != null)
				return false;
		} else if (!(heureDebut.getTimeInMillis() == other.heureDebut.getTimeInMillis()))
			return false;
		if (heureFin == null) {
			if (other.heureFin != null)
				return false;
		} else if (!(heureFin.getTimeInMillis() == other.heureFin.getTimeInMillis()))
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		return true;
	}

	public int getIdEvenement() {
		return idEvenement;
	}

	public void setIdEvenement(int idEvenement) {
		this.idEvenement = idEvenement;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Calendar getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(Calendar heureDebut) {
		this.heureDebut = heureDebut;
	}

	public Calendar getHeureFin() {
		return heureFin;
	}

	public void setHeureFin(Calendar heureFin) {
		this.heureFin = heureFin;
	}

	public Horaire getHoraire() {
		return horaire;
	}

	public void setHoraire(Horaire horaire) {
		this.horaire = horaire;
	}

	public Cours getCours() {
		return cours;
	}

	public void setCours(Cours cours) {
		this.cours = cours;
	}

	public List<Local> getLocaux() {
		return locaux;
	}

	public void setLocaux(List<Local> locaux) {
		this.locaux = locaux;
	}

	public char getStatut() {
		return statut;
	}

	public void setStatut(char statut) {
		this.statut = statut;
	}
	

	
}
