package domaine;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="chronoschool")
public class EtatMail implements  Serializable {

	private static final long serialVersionUID = -337205092442067699L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idEtatMail;
	
	@ManyToOne
	private Utilisateur destinataire;
	
	@ManyToOne
	private Mail email;
	
	@Column(nullable=false)
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Calendar dateEnvoi;

	public EtatMail() {
		super();
	}

	public EtatMail(int idEtatMail, Utilisateur destinataire, Mail email,
			char status, Calendar dateEnvoi) {
		super();
		this.idEtatMail = idEtatMail;
		this.destinataire = destinataire;
		this.email = email;
		this.status = status;
		this.dateEnvoi = dateEnvoi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateEnvoi == null) ? 0 : dateEnvoi.hashCode());
		result = prime * result
				+ ((destinataire == null) ? 0 : destinataire.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + idEtatMail;
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtatMail other = (EtatMail) obj;
		if (dateEnvoi == null) {
			if (other.dateEnvoi != null)
				return false;
		} else if (!dateEnvoi.equals(other.dateEnvoi))
			return false;
		if (destinataire == null) {
			if (other.destinataire != null)
				return false;
		} else if (!destinataire.equals(other.destinataire))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idEtatMail != other.idEtatMail)
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	public int getIdEtatMail() {
		return idEtatMail;
	}

	public void setIdEtatMail(int idEtatMail) {
		this.idEtatMail = idEtatMail;
	}

	public Utilisateur getDestinataire() {
		return destinataire;
	}

	public void setDestinataire(Utilisateur destinataire) {
		this.destinataire = destinataire;
	}

	public Mail getEmail() {
		return email;
	}

	public void setEmail(Mail email) {
		this.email = email;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public Calendar getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Calendar dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}
	
	
}
