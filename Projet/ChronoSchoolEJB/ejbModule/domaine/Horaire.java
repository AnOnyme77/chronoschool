package domaine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="chronoschool")
public class Horaire implements Serializable{

	private static final long serialVersionUID = -2541722360752681911L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idHoraire;
	
	@Column(nullable=true)
	private String idCalendar;
	
	@Column(nullable=true)
	private char statut;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Calendar dateDebut;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Calendar dateFin;
	
	@ManyToOne()
	private Annee annee;
	
	@OneToMany(mappedBy="horaire", cascade=CascadeType.ALL)
	private List<Evenement> evenements;
	
	public Horaire() {
		super();
		this.evenements = new ArrayList<Evenement>();
	}

	public Horaire(int idHoraire, Calendar dateDebut, Calendar dateFin,
			Annee annee) {
		super();
		this.idHoraire = idHoraire;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.annee = annee;
	}
	

	public Horaire(int idHoraire, Calendar dateDebut, Calendar dateFin,
			Annee annee, List<Evenement> evenements) {
		super();
		this.idHoraire = idHoraire;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.annee = annee;
		this.evenements = evenements;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((annee == null) ? 0 : annee.hashCode());
		result = prime * result
				+ ((dateDebut == null) ? 0 : dateDebut.hashCode());
		result = prime * result + ((dateFin == null) ? 0 : dateFin.hashCode());
		result = prime * result
				+ ((evenements == null) ? 0 : evenements.hashCode());
		result = prime * result + idHoraire;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Horaire other = (Horaire) obj;
		if (annee == null) {
			if (other.annee != null)
				return false;
		} else if (!annee.equals(other.annee))
			return false;
		if (dateDebut == null) {
			if (other.dateDebut != null)
				return false;
		} else if (!dateDebut.equals(other.dateDebut))
			return false;
		if (dateFin == null) {
			if (other.dateFin != null)
				return false;
		} else if (!dateFin.equals(other.dateFin))
			return false;
		if (idHoraire != other.idHoraire)
			return false;
		return true;
	}

	public int getIdHoraire() {
		return idHoraire;
	}

	public void setIdHoraire(int idHoraire) {
		this.idHoraire = idHoraire;
	}

	public Calendar getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Calendar dateDebut) {
		this.dateDebut = standardiserDate(dateDebut);
	}

	public static Calendar standardiserDate(Calendar date) {
		if(date != null){
			date.set(Calendar.HOUR_OF_DAY, 0);
			date.set(Calendar.MINUTE, 0);
			date.set(Calendar.SECOND, 0);
			date.set(Calendar.MILLISECOND, 0);
		}
		return date;
	}

	public Calendar getDateFin() {
		return dateFin;
	}

	public void setDateFin(Calendar dateFin) {
		this.dateFin = dateFin;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public List<Evenement> getEvenements() {
		return evenements;
	}

	public void setEvenements(List<Evenement> evenements) {
		this.evenements = evenements;
	}
	
	public String getIdCalendar() {
		return idCalendar;
	}

	public void setIdCalendar(String idCalendar) {
		this.idCalendar = idCalendar;
	}

	public char getStatut() {
		return statut;
	}

	public void setStatut(char statut) {
		this.statut = statut;
	}
	
	

}
