package domaine;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="chronoschool")
public class Cours implements Serializable{

	private static final long serialVersionUID = -6158378765589278809L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCours;
	
	@Column()
	private String description;
	
	@Column(nullable=false)
	private String intitule;
	
	@OneToMany(mappedBy="cours",cascade={CascadeType.ALL})
	private List<CoursProfesseur> professeurs;
	
	@OneToMany(mappedBy="cours")
	private List<Evenement> evenements;
	
	@ManyToMany
	@JoinTable(schema="chronoschool")
	private List<Equipement> equipements;
	
	@ManyToOne
	private Annee Annee;

	public Cours() {
		super();
	}
	
	
	
	public Cours(int idCours, String description, String intitule,
			List<CoursProfesseur> professeurs, List<Evenement> evenements,
			List<Equipement> equipements, domaine.Annee annee) {
		super();
		this.idCours = idCours;
		this.description = description;
		this.intitule = intitule;
		this.professeurs = professeurs;
		this.evenements = evenements;
		this.equipements = equipements;
		Annee = annee;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Annee == null) ? 0 : Annee.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + idCours;
		result = prime * result
				+ ((intitule == null) ? 0 : intitule.hashCode());
		
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cours other = (Cours) obj;
		if (Annee == null) {
			if (other.Annee != null)
				return false;
		} else if (!Annee.equals(other.Annee))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idCours != other.idCours)
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		return true;
	}



	public int getIdCours() {
		return idCours;
	}

	public void setIdCours(int idCours) {
		this.idCours = idCours;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public List<CoursProfesseur> getProfesseurs() {
		return professeurs;
	}

	public void setProfesseurs(List<CoursProfesseur> professeurs) {
		this.professeurs = professeurs;
	}

	public List<Evenement> getEvenements() {
		return evenements;
	}

	public void setEvenements(List<Evenement> evenements) {
		this.evenements = evenements;
	}

	public List<Equipement> getEquipements() {
		return equipements;
	}

	public void setEquipements(List<Equipement> equipements) {
		this.equipements = equipements;
	}


	public Annee getAnnee() {
		return Annee;
	}


	public void setAnnee(Annee annee) {
		Annee = annee;
	}


	
	
	
}
