package domaine;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema="chronoschool")
public class Local implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8138953568536196234L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idLocal;
	
	@Column(nullable=false)
	private int nbPlaces;
	
	@Column(nullable=false,unique=true)
	private String intitule;
	
	@ManyToMany
	@JoinTable(schema="chronoschool")
	private List<Equipement> equipements;

	
	public Local() {
		super();
	}

	public Local(int idLocal, int nbPlaces, String intitule,
			List<Equipement> equipements) {
		super();
		this.idLocal = idLocal;
		this.nbPlaces = nbPlaces;
		this.intitule = intitule;
		this.equipements = equipements;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((equipements == null) ? 0 : equipements.hashCode());
		result = prime * result + idLocal;
		result = prime * result + nbPlaces;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Local other = (Local) obj;
		if (idLocal != other.idLocal)
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		if (nbPlaces != other.nbPlaces)
			return false;
		return true;
	}

	public int getIdLocal() {
		return idLocal;
	}

	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public List<Equipement> getEquipements() {
		return equipements;
	}

	public void setEquipements(List<Equipement> equipements) {
		this.equipements = equipements;
	}
	
}
