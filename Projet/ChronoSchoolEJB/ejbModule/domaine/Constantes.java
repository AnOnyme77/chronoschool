package domaine;

public class Constantes {

	public static final String IHM_TEMPLATE = "Template";
	public static final String IHM_INFO_LOCAL = "InfosLocal";
	public static final String IHM_LISTE_LOCAUX = "ListeLocaux";
	public static final String IHM_INFO_COURS= "InfosCours";
	public static final String IHM_LISTE_COURS = "ListeCours";
	public static final String IHM_INFO_SECTION = "InfosSection";
	public static final String IHM_LISTE_SECTION = "ListeSection";
	public static final String IHM_COMPTE = "MonCompte";
	public static final String IHM_LISTE_HORAIRES = "ListeHoraires";
	public static final String IHM_INFO_UTILISATEUR = "InfosUtilisateur";
	public static final String IHM_LISTE_UTILISATEURS = "ListeUtilsateurs";
	public static final String IHM_AFFICHER_HORAIRE = "AfficherHoraire";
	public static final String IHM_ETATS_MAILS = "EtatMails"; 
	public static final String IHM_INFO_HORAIRE = "InfosHoraire";
	public static final String IHM_MDPOUBLIE = "MdpOublie";
	public static final String IHM_INDEX = "Index";
	public static final String IHM_INSCRIPTION = "Inscription";
	public static final String IHM_INFO_EQUIPEMENT = "InfosEquipement";
	public static final String IHM_LISTE_EQUIPEMENT = "ListeEquipements";
	public static final String IHM_INFO_EVENEMENT = "InfosEvenement";
	public static final String IHM_MODIFIER_EVENEMENT = "ModifierEvenement";
	
	
	public static final String ATT_MESSAGE = "message";
	public static final String ATT_SESSION_USER = "user";
	public static final String ATT_LISTE = "liste";
	public static final String ATT_OBJET = "objet";
	public static final String ATT_RESULTAT = "resultat";
	public static final String ATT_OPERATION = "operation";
	public static final String ATT_LISTE_ANNEE = "liste_annee";
	public static final String ATT_LISTE_PROFESSEUR = "liste_professeur";
	public static final String ATT_LISTE_EQUIPEMENT = "liste_equipement";
	
	
	public static final String MAIL_CRON_ENVOI_MINUTE = "*/15";
	public static final String MAIL_CRON_ENVOI_HEURE = "*";
	
	public static final String CAL_CRON_MINUTE = "*/2";
	public static final String CAL_CRON_HEURE = "*";
	
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_MDP = "mdp";
	public static final String PARAM_SUPPRIMER = "supprimer";
	public static final String PARAM_ID = "id";
	public static final String PARAM_IDLOCAL = "idLocal";
	public static final String PARAM_IDCOURS = "idCours";
	public static final String PARAM_OBJET = "objet";
	public static final String PARAM_TITULAIRE = "titulaire";
	public static final String PARAM_EQUIPEMENT = "equipement";
	public static final String PARAM_PROFESSEUR = "professeur";
	
}