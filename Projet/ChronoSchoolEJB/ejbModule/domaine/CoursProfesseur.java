package domaine;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="chronoschool")
public class CoursProfesseur implements Serializable{
	
	private static final long serialVersionUID = 6297305626946695524L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCoursProf;
	
	@Column(nullable=false)
	private boolean titulaire;
	
	@ManyToOne
	private Utilisateur professeur;
	
	@ManyToOne
	private Cours cours;

	
	public CoursProfesseur() {
		super();
	}

	public CoursProfesseur(int idCoursProf, boolean titulaire,
			Utilisateur professeur, Cours cours) {
		super();
		this.idCoursProf = idCoursProf;
		this.titulaire = titulaire;
		this.professeur = professeur;
		this.cours = cours;
	}
	
	public boolean contientUtilisateur(Utilisateur utilisateur)
	{
		return this.professeur.equals(utilisateur);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cours == null) ? 0 : cours.hashCode());
		result = prime * result + idCoursProf;
		result = prime * result
				+ ((professeur == null) ? 0 : professeur.hashCode());
		result = prime * result + (titulaire ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoursProfesseur other = (CoursProfesseur) obj;
		if (cours == null) {
			if (other.cours != null)
				return false;
		} else if (!cours.equals(other.cours))
			return false;
		if (idCoursProf != other.idCoursProf)
			return false;
		if (professeur == null) {
			if (other.professeur != null)
				return false;
		} else if (!professeur.equals(other.professeur))
			return false;
		if (titulaire != other.titulaire)
			return false;
		return true;
	}

	public int getIdCoursProf() {
		return idCoursProf;
	}

	public void setIdCoursProf(int idCoursProf) {
		this.idCoursProf = idCoursProf;
	}

	public boolean isTitulaire() {
		return titulaire;
	}

	public void setTitulaire(boolean titulaire) {
		this.titulaire = titulaire;
	}

	public Utilisateur getProfesseur() {
		return professeur;
	}

	public void setProfesseur(Utilisateur professeur) {
		this.professeur = professeur;
	}

	public Cours getCours() {
		return cours;
	}

	public void setCours(Cours cours) {
		this.cours = cours;
	}
	
	
}
