package domaine;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="chronoschool")
public class Annee implements Serializable{

	private static final long serialVersionUID = 1471181525409616053L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAnnee;
	
	@Column(nullable=false)
	private String section;
	
	@Column(nullable=false)
	private int numClasse;
	
	@OneToMany(mappedBy="annee",cascade={CascadeType.PERSIST,CascadeType.DETACH,CascadeType.MERGE})
	private List<Utilisateur> etudiants;

	public int getIdAnnee() {
		return idAnnee;
	}

	public Annee() {
		super();
	}

	public Annee(int idAnnee, String section, int numClasse) {
		super();
		this.idAnnee = idAnnee;
		this.section = section;
		this.numClasse = numClasse;
	}

	


	public Annee(int idAnnee, String section, int numClasse,
			List<Utilisateur> etudiants) {
		super();
		this.idAnnee = idAnnee;
		this.section = section;
		this.numClasse = numClasse;
		this.etudiants = etudiants;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idAnnee;
		result = prime * result + numClasse;
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Annee other = (Annee) obj;
		if (idAnnee != other.idAnnee)
			return false;
		if (numClasse != other.numClasse)
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		return true;
	}

	public void setIdAnnee(int idAnnee) {
		this.idAnnee = idAnnee;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public int getNumClasse() {
		return numClasse;
	}

	public void setNumClasse(int numClasse) {
		this.numClasse = numClasse;
	}

	public List<Utilisateur> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(List<Utilisateur> etudiants) {
		this.etudiants = etudiants;
	}
	
	//Methodes d'aisance
	public boolean ajouterEtudiant(Utilisateur etudiant){
		if(etudiant == null) return false;
		if(this.etudiants.contains(etudiant))
			return false;
		else{
			this.etudiants.add(etudiant);
			return true;
		}
	}
	
	public boolean modifierEtudiant(Utilisateur etudiant){
		if(etudiant == null) return false;
		if(!this.etudiants.contains(etudiant)){
			//Si la liste ne contient pas l'étudiant, on l'ajoute à la liste
			return ajouterEtudiant(etudiant);
		}else{
			return this.etudiants.add(etudiant);
		}
	}
	
	public boolean supprimerEtudiant(Utilisateur etudiant){
		if(etudiant == null) return false;
		if(!this.etudiants.contains(etudiant)){
			return false;
		}else{
			return this.etudiants.remove(etudiant);
		}
	}
}
