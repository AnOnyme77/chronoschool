package domaine;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(schema="chronoschool")
public class Utilisateur implements Serializable{
	
	private static final long serialVersionUID = 6185583177115540978L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUtilisateur;
	
	
	@Column(nullable=false)
	private String nom;
	
	@Column(nullable=false)
	private String prenom;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Calendar dateNaissance;
	
	@Column(nullable=false,unique=true)
	private String email;
	
	@Column(nullable=false)
	private String mdp;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(nullable=false)
	private Sexe sexe;
	
	@Column(nullable=false)
	private char status;
	
	@OneToMany(mappedBy="professeur", cascade=CascadeType.ALL)
	private List<CoursProfesseur> cours;
	
	@ManyToOne
	private Annee annee;

	public Utilisateur() {
		super();
	}

	public Utilisateur(int idUtilisateur, String nom, String prenom,
			Calendar dateNaissance, String email, Sexe sexe, char status) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.sexe = sexe;
		this.status = status;
	}

	

	public Utilisateur(int idUtilisateur, String nom, String prenom,
			Calendar dateNaissance, String email, Sexe sexe, char status,
			List<CoursProfesseur> cours, Annee annee) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.sexe = sexe;
		this.status = status;
		this.cours = cours;
		this.annee = annee;
	}

	

	public Utilisateur(int idUtilisateur, String nom, String prenom,
			Calendar dateNaissance, String email, String mdp, Sexe sexe,
			char status, List<CoursProfesseur> cours, Annee annee) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.mdp = mdp;
		this.sexe = sexe;
		this.status = status;
		this.cours = cours;
		this.annee = annee;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateNaissance == null) ? 0 : dateNaissance.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + idUtilisateur;
		result = prime * result + ((mdp == null) ? 0 : mdp.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + ((sexe == null) ? 0 : sexe.hashCode());
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utilisateur other = (Utilisateur) obj;
		if (dateNaissance == null) {
			if (other.dateNaissance != null)
				return false;
		} else if (!dateNaissance.equals(other.dateNaissance))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idUtilisateur != other.idUtilisateur)
			return false;
		if (mdp == null) {
			if (other.mdp != null)
				return false;
		} else if (!mdp.equals(other.mdp))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (sexe != other.sexe)
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Calendar getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Calendar dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Sexe getSexe() {
		return sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public List<CoursProfesseur> getCours() {
		return cours;
	}

	public void setCours(List<CoursProfesseur> cours) {
		this.cours = cours;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	
	public boolean isTitulaire(Cours cours){
		boolean retour = false;
		if(cours != null && this.getCours()!= null )
		{
			for(CoursProfesseur unCours : this.getCours()){
				if(cours.getIdCours() == unCours.getCours().getIdCours() && unCours.isTitulaire())
					retour = true;
			}
		}
		return retour;
	}
}
