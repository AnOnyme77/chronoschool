package domaine;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema="chronoschool")
public class Equipement implements Serializable{
	
	private static final long serialVersionUID = 1190783106368593248L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idEquipement;
	
	@Column(nullable=false,unique=true)
	private String intitule;
	
	@Column
	private String description;
	
	@ManyToMany(mappedBy="equipements")
	private List<Local> locaux;
	

	public Equipement() {
		super();
	}

	public Equipement(int idEquipement, String intitule, String description,
			List<Local> locaux) {
		super();
		this.idEquipement = idEquipement;
		this.intitule = intitule;
		this.description = description;
		this.locaux = locaux;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + idEquipement;
		result = prime * result
				+ ((intitule == null) ? 0 : intitule.hashCode());
		result = prime * result + ((locaux == null) ? 0 : locaux.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipement other = (Equipement) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idEquipement != other.idEquipement)
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		return true;
	}

	public int getIdEquipement() {
		return idEquipement;
	}

	public void setIdEquipement(int idEquipement) {
		this.idEquipement = idEquipement;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Local> getLocaux() {
		return locaux;
	}

	public void setLocaux(List<Local> locaux) {
		this.locaux = locaux;
	}
	
	
}
