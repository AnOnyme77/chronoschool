package domaine;

import java.util.ArrayList;

public class PageRight {
	private String pageName;
	private ArrayList<Role> roles;
	private boolean connexion;
	
	public PageRight() {
		super();
		
		this.roles = new ArrayList<Role>();
	}
	public PageRight(String pageName, ArrayList<Role> roles) {
		super();
		this.pageName = pageName;
		this.roles = roles;
	}
	
	public boolean containsRole(char idRole){
		boolean trouve=false;
		for(int i=0;i<this.roles.size() && !trouve;i++){
			if(this.roles.get(i).getIdRole()==idRole)trouve=true;
		}
		return trouve;
	}
	
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public ArrayList<Role> getRoles() {
		return roles;
	}
	public void setRoles(ArrayList<Role> roles) {
		this.roles = roles;
	}
	
	@Override
	public String toString() {
		return "PageRight [pageName=" + pageName + ", roles=" + roles
				+ ", connexion=" + connexion + "]";
	}
	public boolean isConnexion() {
		return connexion;
	}
	public void setConnexion(boolean connexion) {
		this.connexion = connexion;
	}
	
	
}
