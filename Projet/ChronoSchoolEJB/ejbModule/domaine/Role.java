package domaine;

public class Role {
	private String nomRole;
	private char idRole;
	private int roleNumber;
	
	public Role() {
		super();
	}

	public Role(String nomRole, char idRole) {
		super();
		this.nomRole = nomRole;
		this.idRole = idRole;
	}
	
	public String getNomRole() {
		return nomRole;
	}
	public void setNomRole(String nomRole) {
		this.nomRole = nomRole;
	}
	public char getIdRole() {
		return idRole;
	}
	public void setIdRole(char idRole) {
		this.idRole = idRole;
	}

	@Override
	public String toString() {
		return "Role [nomRole=" + nomRole + ", idRole=" + idRole
				+ ", roleNumber=" + roleNumber + "]";
	}

	public int getRoleNumber() {
		return roleNumber;
	}

	public void setRoleNumber(int roleNumber) {
		this.roleNumber = roleNumber;
	}
	
	
	
}
