package outils;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ParserHeure implements Parser<Calendar> {

	@Override
	public Calendar parser(String str) {
		GregorianCalendar retour = new GregorianCalendar();
		String [] heureTab = str.split(":");
		int heure, minute;
		
		try{
			heure = Integer.parseInt(heureTab[0]);
			minute = Integer.parseInt(heureTab[1]);
			retour.set(Calendar.HOUR_OF_DAY, heure);
			retour.set(Calendar.MINUTE, minute);
			
			retour.clear(Calendar.SECOND);
			retour.clear(Calendar.MILLISECOND);
		}
		catch(Exception e){
			retour=null;
		}
		
		
		return retour;
	}

}
