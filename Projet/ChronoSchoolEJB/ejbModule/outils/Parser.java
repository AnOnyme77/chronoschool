package outils;

public interface Parser<T> {
	T parser(String str);
}
