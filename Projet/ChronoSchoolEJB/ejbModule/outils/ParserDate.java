package outils;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ParserDate implements Parser<Calendar> {

	@Override
	public Calendar parser(String str) {
		Calendar date = null;
		int annee, mois, jour;
		
		String[] infos = str.split("-");
		
		annee = Integer.parseInt(infos[0]);
		mois = Integer.parseInt(infos[1]);
		jour = Integer.parseInt(infos[2]);
		
		date = new GregorianCalendar(annee, mois-1, jour);
		date.clear(Calendar.SECOND);
		date.clear(Calendar.MILLISECOND);
		return date;
	}

}
