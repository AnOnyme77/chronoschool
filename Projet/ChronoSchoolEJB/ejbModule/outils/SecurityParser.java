package outils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import domaine.PageRight;
import domaine.Role;

public class SecurityParser{
	
	private Map<String, Role> roles;
	
	public SecurityParser(){
		this.roles = new HashMap<String,Role>();
	}

	public Map<String,PageRight> parser(InputStream nomFichier) {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		HashMap<String,PageRight> pages = new HashMap<String,PageRight>();
		
		try {
			 builder = factory.newDocumentBuilder();
			 document = builder.parse(nomFichier);
			 
			 this.getRoles(document);
			 pages = this.getDroits(document);
			 
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
		
		
		return pages;
	}
	
	private HashMap<String, PageRight> getDroits(Document doc) {
		NodeList liste = doc.getElementsByTagName("page");
		PageRight pageRight = null;
		HashMap<String, PageRight> droits = new HashMap<String, PageRight>();
		for(int i=0; i<liste.getLength();i++){
			Node node = liste.item(i);
			
			if(node.getNodeType() == Node.ELEMENT_NODE){
				pageRight = new PageRight();
				
				Element element = (Element) node;
				
				String pageName = element.getElementsByTagName("page-name").item(0).getChildNodes().item(0).getNodeValue();
				pageRight.setPageName(pageName);
				
				String connexion = element.getElementsByTagName("connexion").item(0).getChildNodes().item(0).getNodeValue();
				pageRight.setConnexion((connexion.equals("true")?true:false));
				
				NodeList roles = element.getElementsByTagName("role-id");
				
				for(int j=0; j<roles.getLength();j++){
					Node node2 = roles.item(j).getChildNodes().item(0);
					
					if(node.getNodeType() == Node.ELEMENT_NODE){
						if(this.roles.containsKey(node2.getNodeValue()))
							pageRight.getRoles().add(this.roles.get(node2.getNodeValue()));
					}
				}
				droits.put(pageName, pageRight);
			}
		}
		return droits;
	}

	private void getRoles(Document doc){
		NodeList liste = doc.getElementsByTagName("role");
		Role role = null;
		int numeroRole;
		for(int i=0; i<liste.getLength();i++){
			Node node = liste.item(i);
			
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element) node;
				
				String roleName = element.getElementsByTagName("role-name").item(0).getChildNodes().item(0).getNodeValue();
				String roleId = element.getElementsByTagName("role-id").item(0).getChildNodes().item(0).getNodeValue();
				String roleNumber = element.getElementsByTagName("role-number").item(0).getChildNodes().item(0).getNodeValue();
				
				
				if(roleName!=null && roleId!=null && roleNumber!=null){
					role = new Role(roleName,roleId.charAt(0));
					
					try{
						numeroRole = Integer.parseInt(roleNumber);
						role.setRoleNumber(numeroRole);
					}
					catch(NumberFormatException e){
						role.setRoleNumber(0);
					}
					
					this.roles.put(""+roleId.charAt(0), role);
				}
			}
		}
	}
	
	public Collection<Role> getRolesSecurite(InputStream nomFichier){
		this.parser(nomFichier);
		return this.roles.values();
	}
	
	public Role getSmallestRole(InputStream nomFichier){
		this.parser(nomFichier);
		Role plusPetit = null;
		Role actuel = null;
		Iterator<Role> it = this.roles.values().iterator();
		if(it.hasNext())plusPetit = it.next();
		
		while(it.hasNext()){
			actuel = it.next();
			if(actuel.getRoleNumber()<plusPetit.getRoleNumber())plusPetit=actuel;
		}
		
		return plusPetit;
	}

}
