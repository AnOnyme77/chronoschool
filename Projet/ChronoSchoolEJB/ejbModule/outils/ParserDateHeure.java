package outils;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ParserDateHeure implements Parser<Calendar> {

	/**
	 * @param str format - yyyy-MM-dd-HH-mm
	 */
	@Override
	public Calendar parser(String str) {
		Calendar dateH = null;
		int annee, mois, jour;
		int heure, minute;
		String[] infos = str.split("-");
		
		try{
			annee = Integer.parseInt(infos[0]);
			mois = Integer.parseInt(infos[1]);
			jour = Integer.parseInt(infos[2]);
			heure = Integer.parseInt(infos[3]);
			minute = Integer.parseInt(infos[4]);
			
			dateH = new GregorianCalendar(annee, mois-1, jour, heure, minute);
			return dateH;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
