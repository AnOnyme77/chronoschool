package outils;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Remote;

import usecases.GererAnnee;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.calendar.model.AclRule;

public class GoogleCalendar {
	private HttpTransport httpTransport;
	private JacksonFactory jsonFactory;
	private String scope = "https://www.googleapis.com/auth/calendar";
	private ArrayList<String> scopes = new ArrayList<String>();
	private String emailAddress = "705530083170-150opbslftn2ofpg4c0jecs4ptgh14sc@developer.gserviceaccount.com";
	private GoogleCredential credential;
	private static String applicationName = "ChronoSchool";
	private AclRule rule;

	public GoogleCalendar() {
		super();
		
	}
	
	public GoogleCredential getCredential(HttpTransport httpTransport, JacksonFactory jsonFactory, String emailAddress, ArrayList<String> scopes){
		try {
			credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory).setServiceAccountId(emailAddress)
					.setServiceAccountPrivateKeyFromP12File(new File("../standalone/deployments/ChronoSchool.ear/ChronoSchoolEJB.jar/META-INF/ChronoSchool-3aff40882c1c.p12")).setServiceAccountScopes(scopes).build();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return credential;
	}
}
