package outils;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import usecases.GererMail;
import domaine.Constantes;
import domaine.EtatMail;


@Singleton
public class TimerMail {

	@EJB
	private GererMail uc;
	
	// cette m�thode est appel�e toutes les minutes
    @Schedule(minute=Constantes.MAIL_CRON_ENVOI_MINUTE, hour=Constantes.MAIL_CRON_ENVOI_HEURE, persistent=false)
    public void automaticTimeout1() {
    	
    	List<EtatMail> liste = this.uc.listerAEnvoyer();
    	if(liste!=null){
	    	
	    	for (EtatMail email : liste) {
		        try {
		            this.uc.envoyerSync("evrardlaurent77@gmail.com", email.getDestinataire().getEmail(), 
		            		email.getEmail().getTitre(), email.getEmail().getContenu());
		            this.uc.marqueEnvoye(email);
		
		        } catch (javax.mail.MessagingException e) {
		        	this.uc.marqueErreur(email);
		            //e.printStackTrace();
		        }
		    	
			}
    	}
    	
    }

}
