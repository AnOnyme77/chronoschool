package outils;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Timer;

import usecases.GererCalendar;
import usecases.GererHoraire;
import domaine.Constantes;
import domaine.Horaire;

@Singleton
public class TimerCalendar {
	
	@EJB
	private GererHoraire ucHoraire;
	
	@EJB
	private GererCalendar ucCalendar;


	@Schedule(minute=Constantes.CAL_CRON_MINUTE, hour=Constantes.CAL_CRON_HEURE,persistent=false)
    private void scheduledTimeout(final Timer t) {
        List<Horaire> horaires = this.ucHoraire.listerHoraire();
        for(Horaire horaire : horaires){
        	horaire = this.ucHoraire.chargerTout(horaire);
        	switch(horaire.getStatut()){
        		case 'A':
        			this.ucCalendar.ajouterCalendar(horaire, false);
        			break;
        			
        		case 'M':
        			if(horaire.getIdCalendar()!=null && !horaire.getIdCalendar().isEmpty()){
        				this.ucCalendar.supprimerCalendar(horaire);
        			}
        			this.ucCalendar.ajouterCalendar(horaire, false);
        			break;
        			
        		case 'E':
        			if(horaire.getIdCalendar()!=null && !horaire.getIdCalendar().isEmpty()){
        				this.ucCalendar.supprimerCalendar(horaire);
        			}
        			this.ucCalendar.ajouterCalendar(horaire, true);
        			break;
        			
        		case 'S':
        			if(horaire.getIdCalendar()!=null && !horaire.getIdCalendar().isEmpty())
        				this.ucCalendar.supprimerCalendar(horaire);
        			this.ucHoraire.supprimerHoraire(horaire);
        			break;
        	}
        }
    }
}