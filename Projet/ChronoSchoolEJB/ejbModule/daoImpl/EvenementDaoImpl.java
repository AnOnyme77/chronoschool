package daoImpl;

import java.util.List;

import javax.ejb.Stateless;

import dao.EvenementDao;
import domaine.Evenement;
@Stateless
public class EvenementDaoImpl extends DaoImpl<Integer, Evenement> implements EvenementDao {


	private static final long serialVersionUID = -6831239613431258253L;

	@Override
	public List<Evenement> listerAvecParams(String queryString, Object[] params) {
		if(queryString == null) return null;
		return liste(queryString, params);
	}

	@Override
	public List<Evenement> listerParProfesseur(int idUtilisateur) {		
		String queryString = "select e from Evenement e "
				+ "inner join e.cours c "
				+ "inner join c.professeurs cp "
				+ "where cp.professeur.idUtilisateur = ? ";

		Object[] params = new Object[1];
		params[0]=idUtilisateur;
		return super.liste(queryString, params);
	}

	@Override
	public List<Evenement> listerParAnnee(int idAnnee) {
		String queryString="select e from Evenement e "
				+ "inner join e.horaire h "
				+ "where h.annee.idAnnee = ?";

		Object[] params = new Object[1];
		params[0]=idAnnee;
		return super.liste(queryString, params);
	}

	@Override
	public List<Evenement> listerParLocal(int idLocal) {
		String queryString="select e from Evenement e "
				+ "inner join e.locaux l "
				+ "where l.idLocal = ?";

		Object[] params = new Object[1];
		params[0]=idLocal;
		return super.liste(queryString, params);
	}
	
	@Override
	public Evenement chargerTout(Evenement evenement) {
		Evenement retour = this.rechercher(evenement.getIdEvenement());
		if(retour==null)return null;
		retour.getLocaux().size();
		
		return retour; 
	}

}
