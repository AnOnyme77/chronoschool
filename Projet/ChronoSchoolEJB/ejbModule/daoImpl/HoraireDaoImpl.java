package daoImpl;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import usecases.GererAnnee;
import usecases.GererEvenement;
import usecases.GererUtilisateur;
import dao.EvenementDao;
import dao.HoraireDao;
import domaine.Annee;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Utilisateur;
@Stateless
public class HoraireDaoImpl extends DaoImpl<Integer, Horaire> implements HoraireDao {

	private static final long serialVersionUID = 2005666560064592412L;
	@EJB
	GererEvenement gestionnaireEvenement;
	@EJB
	GererAnnee gestionnaireannee;
	@EJB
	GererUtilisateur gestionnaireUtilisateur;
	@EJB
	EvenementDao daoEvenement;
	
	@Override
	public Horaire getSemaineAnnee(Annee annee, Calendar dateDebutSemaine) {
		if(annee == null) return null;
		annee = this.gestionnaireannee.rechercherAnnee(annee.getIdAnnee());
		if(annee == null) return null;
		
		if(dateDebutSemaine == null) return null;
		//on s'assure de la standardisation de la date
		dateDebutSemaine = Horaire.standardiserDate(dateDebutSemaine);
		
		Calendar debutHoraire = new GregorianCalendar();
		debutHoraire.setTimeInMillis(dateDebutSemaine.getTimeInMillis());
		
		Calendar finHoraire = new GregorianCalendar();
		finHoraire.setTimeInMillis(dateDebutSemaine.getTimeInMillis());
		
		Calendar debutEvenement = new GregorianCalendar();
		debutEvenement.setTimeInMillis(dateDebutSemaine.getTimeInMillis());
		
		Calendar dateFin = new GregorianCalendar();
		dateFin.setTimeInMillis(dateDebutSemaine.getTimeInMillis());
		dateFin.add(Calendar.DAY_OF_MONTH, 7);
		
		String queryNative = "select e from "
				+ "Evenement e inner join e.horaire h "
				+ "where h.annee = :annee and "
				+ "h.dateDebut <= :dhoraire and "
				+ "h.dateFin > :fhoraire and "
				+ "h.statut != 'S' and "
				+ "e.heureDebut between :debutEvenement and :dateFin and "
				+ "e.statut = :statut";

		TypedQuery<Evenement> query = this.entityManager.createQuery(queryNative,Evenement.class);
		query.setParameter("annee", annee);
		query.setParameter("dhoraire", debutHoraire, TemporalType.DATE);
		query.setParameter("fhoraire", finHoraire, TemporalType.DATE);
		query.setParameter("debutEvenement", debutEvenement, TemporalType.TIMESTAMP);
		query.setParameter("dateFin", dateFin, TemporalType.TIMESTAMP);
		query.setParameter("statut", 'A');
		
		List<Evenement> liste = (List<Evenement>)query.getResultList();
		
		Horaire horaire = new Horaire();
		horaire.setEvenements(liste);

		return horaire;
	}

	@Override
	public Horaire getSemaineProf(Utilisateur utilisateur, Calendar dateDebutSemaine) {
		if(utilisateur == null) return null;
		utilisateur = this.gestionnaireUtilisateur.rechercherUtilisateur(utilisateur.getEmail());
		if(utilisateur == null) return null;
		
		
		dateDebutSemaine = Horaire.standardiserDate(dateDebutSemaine);
		
		GregorianCalendar dateFin = new GregorianCalendar();
		dateFin.setTimeInMillis(dateDebutSemaine.getTimeInMillis());
		dateFin.add(Calendar.DAY_OF_MONTH, 7);
		
		
		String queryString = "select e "+
				"from Evenement e "+
				     "inner join e.cours c "+
				     "inner join c.professeurs cp "+
				"where cp.professeur = ? and "+
				      "e.heureDebut between ? and ?";
		
		
		TypedQuery<Evenement> query = this.entityManager.createQuery(queryString, Evenement.class);
		query.setParameter(1, utilisateur);
		query.setParameter(2, dateDebutSemaine,TemporalType.TIMESTAMP);
		query.setParameter(3, dateFin,TemporalType.TIMESTAMP);
		
		Horaire horaire = new Horaire();
		horaire.setEvenements(query.getResultList());
		
		return horaire;
	}

	@Override
	public List<Evenement> getEvenementType(Horaire horaire) {
		if(horaire == null) return null;
		horaire = rechercher(horaire.getIdHoraire());
		if(horaire == null) return null;
		
		//On prend les dates de début et de fin de la première semaine
		//afin de récupérer les évènements d'une semaine
		Calendar dateDebut = horaire.getDateDebut();
		Calendar dateFin = new GregorianCalendar();
		dateFin.setTime(dateDebut.getTime());
		dateFin.add(Calendar.DAY_OF_MONTH, 7);
		
		//On recherche maintenant les évènements associés à cet horaire et à ces dates
		String queryString = "select e "
							+ "from Horaire h, Evenement e "
							+ "where h.idHoraire = ? "
							+ "and e.HeureDebut <= ? "
							+ "and e.HeureFin <= ?";
		
		Object[] params = new Object[5];
		params[0] = horaire.getIdHoraire();
		params[1] = dateDebut;
		params[2] = TemporalType.DATE;
		params[3] = dateFin;
		params[4] = TemporalType.DATE;
	
		return daoEvenement.listerAvecParams(queryString, params);
	}
	
	@Override
	public Horaire chargerTout(Horaire horaire) {
		if(horaire == null)		return null;
		horaire = this.rechercher(horaire.getIdHoraire());
		horaire.getEvenements().size();
		return horaire;
	}

	@Override
	public List<Horaire> getHorairesAnnee(Annee annee) {
		String queryString = "select h from Horaire h where h.annee = ? and h.statut!='S'";
		Object[] params = new Object[1];
		params[0] = annee;
		return this.liste(queryString, params);
	}

	@Override
	public List<Horaire> listerHorairesActif() {
		String queryString = "select h from Horaire h where h.statut != 'S' ";
		Object[] params = new Object[0];
		return this.liste(queryString, params);
	}	
}
