package daoImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererCoursProfesseur;
import dao.CoursDao;
import domaine.Cours;
@Stateless
public class CoursDaoImpl extends DaoImpl<Integer, Cours> implements CoursDao {

	private static final long serialVersionUID = 7056210253979478896L;

	@EJB
	GererCoursProfesseur gererCoursProfesseur;
	
	@Override
	public Cours rechercher(int idCours) {
		return super.rechercher(idCours);
	} 

	@Override
	public void supprimer(int idCours) {
		super.supprimer(idCours);
	}

	@Override
	public List<Cours> listerP(int idProfesseur){
		String queryString="select c from CoursProfesseur cp, Cours c "
				+ "where c = cp.cours "
				+ "AND cp.professeur.idUtilisateur = ? "
				+ "group by c";

		Object[] params = new Object[1];
		params[0]=idProfesseur;
		return super.liste(queryString, params);
	}

	@Override
	public List<Cours> listerA(int idAnnee) {
		String queryString="select c from Cours c "
				+ "where c.Annee.idAnnee = ?";
		Object[] params = new Object[1];
		params[0]=idAnnee;
		return super.liste(queryString, params);
	}
	
	@Override
	public Cours chargerTout(int idCours)
	{
		Cours cours = this.rechercher(idCours);
		if(cours==null)return null;
		
		cours.getEquipements().size();
	
		cours.getProfesseurs().size();
	
		cours.getEvenements().size();
	
		return cours;
	}
	
	
}
