package daoImpl;

import javax.ejb.Stateless;
import dao.EquipementDao;
import domaine.Equipement;

@Stateless
public class EquipementDaoImpl extends DaoImpl<Integer, Equipement> implements EquipementDao {

	private static final long serialVersionUID = -8846347311986481536L;

	public Equipement rechercherIntitule(String intitule){
		Equipement retour = null;
		Object[] params = new Object[1];
		params[0]=intitule;
		retour=this.recherche("select e from Equipement e where e.intitule=?", params);
		return retour;
	}

	@Override
	public Equipement chargerTout(int id) {
		Equipement equipement = this.rechercher(id);
		if(equipement==null)return null;
		
		equipement.getLocaux().size();
		
		return equipement;
	}
	
}
