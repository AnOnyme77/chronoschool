package daoImpl;

import java.util.List;

import javax.ejb.Stateless;

import dao.EtatMailDao;
import domaine.EtatMail;

@Stateless
public class EtatMailDaoImpl extends DaoImpl<Integer, EtatMail> implements EtatMailDao {

	private static final long serialVersionUID = 646603115254981593L;

	@Override
	public List<EtatMail> listeAEnvoyer() {
		String query = "select e from EtatMail e where e.status='U'";
		Object[] params = new Object[0];
		return this.liste(query, params);
	}

	@Override
	public List<EtatMail> listerDate() {
		String query = "select e from EtatMail e order by e.email.dateAjout desc";
		Object[] params = new Object[0];
		return this.liste(query, params);
	}

}
