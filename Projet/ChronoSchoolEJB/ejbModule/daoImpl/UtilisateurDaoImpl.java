package daoImpl;

import java.util.List;

import javax.ejb.Stateless;

import dao.UtilisateurDao;
import domaine.Utilisateur;
@Stateless
public class UtilisateurDaoImpl extends DaoImpl<Integer, Utilisateur> implements UtilisateurDao{

	private static final long serialVersionUID = 6496705780307058131L;


	@Override
	public void supprimer(int idUtilisateur) {
		super.supprimer(idUtilisateur);
	}

	@Override
	public Utilisateur rechercher(String email) {
		String queryString="select u from Utilisateur u where u.email = ?";
		Object[] params = new Object[1];
		params[0]=email;
		return recherche(queryString, params);
	}


	@Override
	public Utilisateur rechercher(int idUtilisateur) {
		return super.rechercher(idUtilisateur);
	}

	@Override
	public List<Utilisateur> listerProfesseur() {
		String queryString="select u from Utilisateur u where u.status = ?";
		Object[] params = new Object[1];
		params[0]='P';
		return super.liste(queryString, params);
	}

	@Override
	public List<Utilisateur> listerEtudiant() {
		String queryString="select u from Utilisateur u where u.status = ?";
		Object[] params = new Object[1];
		params[0]='U';
		return super.liste(queryString, params);
	}

	@Override
	public Utilisateur chargerTout(Utilisateur user) {
		Utilisateur db = null;
		
		db = this.rechercher(user.getIdUtilisateur());
		if(db!=null){
			db.getCours().size();
			db.getAnnee();
		}
		
		return db;
	}

}
