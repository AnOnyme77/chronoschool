package daoImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererUtilisateur;
import dao.AnneeDao;
import domaine.Annee;
import domaine.Utilisateur;

@Stateless
public class AnneeDaoImpl extends DaoImpl<Integer, Annee> implements AnneeDao {

	private static final long serialVersionUID = -1592434766287558153L;
	@EJB
	private GererUtilisateur gestionnaireUtilisateur;
	
	@Override
	public void supprimer(Annee annee) {
		if(annee == null) return;
		annee = rechercher(annee.getIdAnnee());
		annee = chargerTout(annee);
		for(Utilisateur u : annee.getEtudiants()){
			u.setAnnee(null);
			this.gestionnaireUtilisateur.modifierUtilisateur(u);
		}
		annee.setEtudiants(null);
		annee = mettreAJour(annee);
		super.supprimer(annee.getIdAnnee());
	}

	@Override
	public Annee ajouterEtudiant(Annee annee, Utilisateur etudiant) {
		annee = rechercher(annee.getIdAnnee());
		annee = chargerTout(annee);
		if(annee == null) return null;
		etudiant = gestionnaireUtilisateur.rechercherUtilisateur(etudiant.getEmail());
		if(etudiant == null) return null;
		
		List<Utilisateur> liste = annee.getEtudiants();
		if(liste.contains(etudiant)) return null;
		etudiant.setAnnee(annee);
		etudiant = gestionnaireUtilisateur.modifierUtilisateur(etudiant);
		liste.add(etudiant);		
		annee.setEtudiants(liste);
		
		return mettreAJour(annee);
	}

	@Override
	public Annee modifierEtudiant(Annee annee, Utilisateur etudiant) {
		annee = rechercher(annee.getIdAnnee());
		annee = chargerTout(annee);
		if(annee == null) return null;
		etudiant = gestionnaireUtilisateur.rechercherUtilisateur(etudiant.getEmail());
		if(etudiant == null) return null;
		
		List<Utilisateur> liste = annee.getEtudiants();
		boolean contains = false;
		int index = 0;
		for(int i = 0; i < liste.size(); i++){
			Utilisateur e = liste.get(i);
			if(e.getIdUtilisateur() == etudiant.getIdUtilisateur()){
				contains = true;
				index = i;
			}
		}
		if(!contains) return ajouterEtudiant(annee, etudiant);
		liste.remove(index);
		etudiant.setAnnee(annee);
		etudiant = gestionnaireUtilisateur.modifierUtilisateur(etudiant);
		liste.add(index, etudiant);
		annee.setEtudiants(liste);
		
		return mettreAJour(annee);
	}

	@Override
	public void supprimerEtudiant(Annee annee, Utilisateur etudiant) {
		annee = rechercher(annee.getIdAnnee());
		annee = chargerTout(annee);
		if(annee == null) return;
		etudiant = gestionnaireUtilisateur.rechercherUtilisateur(etudiant.getEmail());
		if(etudiant == null) return;
		List<Utilisateur> liste = annee.getEtudiants();
		if(liste.contains(etudiant)){
			liste.remove(etudiant);
			etudiant.setAnnee(null);
			etudiant = gestionnaireUtilisateur.modifierUtilisateur(etudiant);
			annee.setEtudiants(liste);
			mettreAJour(annee);
		}
	}

	@Override
	public List<Utilisateur> listerEtudiants(Annee annee) {
		annee = chargerTout(annee);
		return annee.getEtudiants();
	}

	@Override
	public Annee chargerTout(Annee annee) {
		annee = this.rechercher(annee.getIdAnnee());
		annee.getEtudiants().size();
		return annee;
	}

}
