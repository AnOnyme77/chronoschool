package daoImpl;

import java.util.List;

import javax.ejb.Stateless;

import dao.CoursProfesseurDao;
import domaine.CoursProfesseur;
@Stateless
public class CoursProfesseurDaoImpl extends DaoImpl<Integer, CoursProfesseur> implements
		CoursProfesseurDao {

	private static final long serialVersionUID = -7528713707543660434L;

	@Override
	public CoursProfesseur rechercher(int idCoursProfesseur) {
		return this.rechercher(idCoursProfesseur);
	}

	@Override
	public void supprimerCP(int idCoursProfesseur) {
		this.supprimer(idCoursProfesseur);
	}

	@Override
	public List<CoursProfesseur> listerParCours(int idCours) {
		String queryString="select cp "
				+ "from CoursProfesseur cp, Cours c "
				+ "where cp.cours = c and c.idCours = ?";
		Object[] params = new Object[1];
		params[0]= idCours;
		return super.liste(queryString, params);
	}

	@Override
	public List<CoursProfesseur> listerParProfesseur(int idProfesseur) {
		String queryString="select cp "
							+ "from CoursUtilisateur cp, Utilisateur u "
							+ "where cp.professeur = u and u.idUtilisateur = ?";
		Object[] params = new Object[1];
		params[0]= idProfesseur;
		return super.liste(queryString, params);
	}
	
}
