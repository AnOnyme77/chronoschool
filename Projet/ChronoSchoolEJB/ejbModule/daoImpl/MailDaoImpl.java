package daoImpl;

import javax.ejb.Stateless;

import dao.MailDao;
import domaine.Mail;

@Stateless
public class MailDaoImpl extends DaoImpl<Integer, Mail> implements MailDao {

	private static final long serialVersionUID = 3373064748248881817L;

}
