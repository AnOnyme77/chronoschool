package daoImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererEquipement;
import dao.LocalDao;
import domaine.Equipement;
import domaine.Local;
@Stateless
public class LocalDaoImpl extends DaoImpl<Integer, Local> implements LocalDao {

	private static final long serialVersionUID = -7280213663729209967L;
	@EJB
	private GererEquipement gestionnaireEquipement;

	@Override
	public Local rechercher(String intitule) {
		String queryString = "SELECT l from Local l where intitule like ?";
		Object[] params = new Object[1];
		params[0] = "%" + intitule + "%";
		return recherche(queryString, params);
	}

	@Override
	public List<Equipement> listerEquipement(Local local) {
		local = chargerTout(local);
		
		return local.getEquipements();
	}

	@Override
	public Local ajouterEquipement(Local local, Equipement equipement) {
		local = rechercher(local.getIdLocal());
		local = chargerTout(local);
		if(local == null) return null;
		equipement = gestionnaireEquipement.rechercherEquipement(equipement);
		if(equipement == null) return null;
		
		List<Equipement> liste = local.getEquipements();
		if(liste.contains(equipement)) return null;
		
		liste.add(equipement);		
		local.setEquipements(liste);
		
		return mettreAJour(local);
	}

	@Override
	public Local modifierEquipement(Local local, Equipement equipement) {
		local = rechercher(local.getIdLocal());
		local = chargerTout(local);
		if(local == null) return null;
		equipement = gestionnaireEquipement.rechercherEquipement(equipement);
		if(equipement == null) return null;
		
		List<Equipement> liste = local.getEquipements();
		boolean contains = false;
		int index = 0;
		for(int i = 0; i < liste.size(); i++){
			Equipement e = liste.get(i);
			if(e.getIdEquipement() == equipement.getIdEquipement()){
				contains = true;
				index = i;
			}
		}
		if(!contains) return ajouterEquipement(local, equipement);
		liste.remove(index);
		liste.add(index, equipement);
		local.setEquipements(liste);
		
		return mettreAJour(local);
	}

	@Override
	public void supprimerEquipement(Local local, Equipement equipement) {
		local = rechercher(local.getIdLocal());
		local = chargerTout(local);
		if(local == null) return;
		equipement = gestionnaireEquipement.rechercherEquipement(equipement);
		if(equipement == null) return;
		List<Equipement> liste = local.getEquipements();
		if(liste.contains(equipement)){
			liste.remove(equipement);
			local.setEquipements(liste);
			mettreAJour(local);
		}
	}

	@Override
	public Local chargerTout(Local local) {
		local = rechercher(local.getIdLocal());
		if(local == null) return null;
		local.getEquipements().size();
		return local;
	}


}
