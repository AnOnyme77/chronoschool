package usescasesImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererCours;
import usecases.GererCoursProfesseur;
import usecases.GererUtilisateur;
import dao.CoursDao;
import domaine.Cours;
import domaine.CoursProfesseur;

@Stateless
public class GererCoursImpl implements GererCours {

	private String message = "";

	@EJB
	private CoursDao coursDao;
	
	@EJB
	private GererUtilisateur gererUtilisateur;
	
	@EJB
	private GererCoursProfesseur ucCP;

	@Override
	public Cours ajouterCours(Cours cours) {

		boolean existeProf = true;

		// verification de l'existance des professeur
		if (cours.getProfesseurs() != null && cours.getProfesseurs().size() > 0) {
			for (CoursProfesseur temp : cours.getProfesseurs()) {
				if (gererUtilisateur.chercherUtilisateur(temp.getProfesseur()
						.getIdUtilisateur()) == null) {
					existeProf = false;
					this.message = "Professeur inexistant.";
				}
			}
		}

		// ajout du cours
		if (existeProf == true) {
			cours = coursDao.enregistrer(cours);
			if (cours != null)
				this.message = "ajout effectuée avec succes";
			else
				this.message = "Erreur lors de l'ajout";
		} else {
			cours = null;
		}

		return cours;
	}

	@Override
	public Cours modifierCours(Cours cours) {


		// on recupere le cours et on ajoute/supprime les coursProfesseur
		Cours coursDB = this.coursDao.rechercher(cours.getIdCours());
		coursDB = this.coursDao.chargerTout(coursDB.getIdCours());

		if(coursDB!=null){
			coursDB.setAnnee(cours.getAnnee());
			coursDB.setDescription(cours.getDescription());
			coursDB.setEquipements(cours.getEquipements());
			coursDB.setIntitule(cours.getIntitule());
			
			this.supprimerCoursProfesseur(coursDB);
			
			coursDB.setProfesseurs(cours.getProfesseurs());
			
			cours = coursDao.mettreAJour(coursDB);

		}

		return cours;
	}
	
	private void supprimerCoursProfesseur(Cours cours){
		for(CoursProfesseur cp : cours.getProfesseurs()){
			this.ucCP.supprimerCoursProfesseur(cp);
		}
	}

	@Override
	public void supprimerCours(Cours cours) {

		coursDao.supprimer(cours.getIdCours());
		message = "suppression effectuée.";
	}

	@Override
	public Cours rechercherCours(Cours cours) {

		return coursDao.rechercher(cours.getIdCours());

	}

	@Override
	public List<Cours> listerTousCours() {

		return coursDao.lister();
	}

	@Override
	public List<Cours> listerCoursParProfesseur(int idProfesseur) {

		return coursDao.listerP(idProfesseur);
	}

	@Override
	public List<Cours> listerCoursParAnnee(int idAnnee) {

		return coursDao.listerA(idAnnee);
	}

	@Override
	public Cours chargerTout(Cours cours) {
		return this.coursDao.chargerTout(cours.getIdCours());
	}

	@Override
	public String getMessage() {
		return message;
	}

}
