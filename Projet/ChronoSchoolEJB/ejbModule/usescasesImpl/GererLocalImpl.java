package usescasesImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererLocal;
import dao.LocalDao;
import domaine.Equipement;
import domaine.Local;
@Stateless
public class GererLocalImpl implements GererLocal {

	@EJB
	private LocalDao dao;
	
	@Override
	public Local enregistrer(Local local) {
		if(local == null) return null;
		return this.dao.enregistrer(local);
	}

	@Override
	public Local rechercher(String intitule) {
		if(intitule == null) return null;
		return this.dao.rechercher(intitule);
	}

	@Override
	public Local rechercher(int id) {
		return this.dao.rechercher(id);
	}
	@Override
	public List<Local> lister() {
		return this.dao.lister();
	}

	@Override
	public Local mettreAJour(Local local) {
		if(local == null) return null;
		return this.dao.mettreAJour(local);
	}

	@Override
	public List<Equipement> listerEquipement(Local local) {
		if(local == null) return null;
		return this.dao.listerEquipement(local);
	}

	@Override
	public Local ajouterEquipement(Local local, Equipement equipement) {
		if(local == null || equipement == null) return null;
		return this.dao.ajouterEquipement(local, equipement);
	}

	@Override
	public Local modifierEquipement(Local local, Equipement equipement) {
		if(local == null || equipement == null) return null;
		return this.dao.modifierEquipement(local, equipement);
	}

	@Override
	public void supprimerEquipement(Local local, Equipement equipement) {
		if(local == null || equipement == null) return;
		this.dao.supprimerEquipement(local, equipement);
	}

	@Override
	public Local chargerTout(Local local) {
		if(local == null) return null;
		return this.dao.chargerTout(local);
	}

	@Override
	public void supprimer(Local local) {
		if(local == null) return;
		this.dao.supprimer(local.getIdLocal());
	}

	@Override
	public void supprimer(int id) {
		this.dao.supprimer(id);
	}

}
