package usescasesImpl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import outils.TimerCalendar;
import usecases.GererAnnee;
import usecases.GererEvenement;
import usecases.GererHoraire;
import usecases.GererMail;
import dao.EvenementDao;
import domaine.Annee;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Local;
import domaine.Utilisateur;

@Stateless
public class GererEvenementImpl implements GererEvenement {

	@EJB
	EvenementDao dao;

	@EJB
	GererHoraire ucHoraire;

	TimerCalendar tc;
	@EJB
	GererAnnee ucAnnee;

	@EJB
	GererMail ucMail;

	@Override
	public List<Evenement> listerAvecParams(String queryString, Object[] params) {
		if (queryString == null)
			return null;
		return this.dao.listerAvecParams(queryString, params);
	}

	@Override
	public List<Evenement> listerEvenementParLocal(Local local) {
		if (local == null)
			return null;
		return this.dao.listerParLocal(local.getIdLocal());
	}

	@Override
	public List<Evenement> listerEvenementParAnnee(Annee annee) {
		if (annee == null)
			return null;
		return this.dao.listerParAnnee(annee.getIdAnnee());
	}

	@Override
	public Evenement mettreAJourEvenement(Evenement evenement) {
		Evenement db = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		if (evenement == null)
			return null;

		db = this.dao.rechercher(evenement.getIdEvenement());
		db = this.dao.chargerTout(evenement);
		String titre = "Modification d'un cours";
		String message = "L'un de vos cours du " + sdf.format(evenement.getHeureDebut().getTime())
				+ " vient d'être modifié.\n" + "Intitulé : " + evenement.getIntitule() + "\n" + "Description : "
				+ evenement.getDescription();
		Annee annee = db.getCours().getAnnee();
		annee = this.ucAnnee.chargerTout(annee);
		this.ucMail.envoyer(titre, message, annee.getEtudiants());


		Evenement eventMaj = this.dao.mettreAJour(evenement);

		// On met à jour l'horaire afin de permettre la synchronisation avec
		// google calendar
		Horaire horaireAMaj = this.ucHoraire.rechercherHoraire(db.getHoraire());
		horaireAMaj = this.ucHoraire.chargerTout(horaireAMaj);
		horaireAMaj.setStatut('E');
		
		int indiceEvent = getIndiceEvent(horaireAMaj.getEvenements(), db);
		// int indiceEvent = horaireAMaj.getEvenements().indexOf(db);

		horaireAMaj.getEvenements().set(indiceEvent, eventMaj);
		ucHoraire.modifierHoraire(horaireAMaj, true);

		return eventMaj;
	}
	
	private int getIndiceEvent(List<Evenement> liste, Evenement event){
		int indice = -1;
		if(liste != null){
			for(int i = 0; i < liste.size() && indice == -1; i++){
				if(liste.get(i).getIdEvenement() == event.getIdEvenement())
					indice = i;					
			}
		}
		return indice;
	}
	@Override
	public List<Evenement> listerEvenementParProfesseur(Utilisateur utilisateur) {
		if (utilisateur == null)
			return null;
		return this.dao.listerParProfesseur(utilisateur.getIdUtilisateur());
	}

	@Override
	public Evenement rechercherEvenement(int idEvenement) {
		if (idEvenement == 0)
			return null;
		return this.dao.rechercher(idEvenement);
	}

	@Override
	public Evenement chargerTout(Evenement evenement) {
		return this.dao.chargerTout(evenement);
	}

	@Override
	public Evenement enregistrerEvenement(Evenement evenement) {
		if (evenement == null || evenement.getIntitule() == null || evenement.getIntitule() == ""
				|| evenement.getHeureDebut() == null || evenement.getHeureFin() == null || evenement.getStatut() == ' '
				|| evenement.getHoraire() == null || evenement.getCours() == null || evenement.getLocaux() == null
				|| evenement.getLocaux().size() == 0)
			return null;

		return this.dao.enregistrer(evenement);
	}

	@Override
	public void supprimerEvenement(Evenement evenement) {
		if (evenement == null || evenement.getIdEvenement() == 0)
			return;
		this.dao.supprimer(evenement.getIdEvenement());
	}

	/**
	 * Verifie si un evenement en chevauche un autre du point de vue temporel
	 * 
	 * @param horaire
	 * @param evenement
	 * @return true si l'evenement ne peut etre enregistré dans son etat actuel<br>
	 *         false si l'evenement peut etre enregistré
	 */
	public boolean chevaucheTemps(Horaire horaire, Evenement evenement) {
		if (horaire == null)
			return true;
		if (evenement == null)
			return true;

		Horaire type = ucHoraire.getSemaineAnnee(horaire.getAnnee(), horaire.getDateDebut());
		if (type == null)
			return true;

		List<Evenement> liste = type.getEvenements();

		boolean chevauche = false;
		for (int i = 0; i < liste.size() - 1 && !chevauche; i++) {
			// On vérifie s'il s'agit du meme jour de la semaine, étant donné
			// que la vérification se fait sur un horaire type et non un vrai
			if (evenement.getHeureDebut().get(Calendar.DAY_OF_WEEK) == liste.get(i).getHeureFin()
					.get(Calendar.DAY_OF_WEEK)) {
				if (heureToInt(evenement.getHeureDebut()) < heureToInt(liste.get(i).getHeureFin()))
					chevauche = true;
				if (heureToInt(evenement.getHeureFin()) > heureToInt(liste.get(i + 1).getHeureDebut()))
					chevauche = true;
			}
		}
		return chevauche;
	}

	private int heureToInt(Calendar heure) {
		int h = heure.get(Calendar.HOUR_OF_DAY) * 100;
		h += heure.get(Calendar.MINUTE);
		return h;
	}
}
