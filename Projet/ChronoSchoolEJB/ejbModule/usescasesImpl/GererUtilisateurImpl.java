package usescasesImpl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;

import outils.PasswordHash;
import usecases.GererMail;
import usecases.GererUtilisateur;
import dao.UtilisateurDao;
import domaine.Utilisateur;
@Stateless
public class GererUtilisateurImpl implements GererUtilisateur {

	@EJB
	private UtilisateurDao utilisateurDao;
	
	@EJB
	private GererMail gererMail;
	
	private String message="";
	
	// ajoute l'utilisateur en DB
	@Override
	public Utilisateur ajouterUtilisateur(Utilisateur user) {
		if(this.existe(user.getEmail()))
			return null;
		else//Vérification de la validité de la date de naissance et de l'adresse email
		{
			//SI sa date de naissance se trouve dans le futur
			if(user.getDateNaissance().after(Calendar.getInstance())){
				message = "La date renseignée est invalide";
				return null;
			}else{
				//Si l'email est invalide (vérification bancale à améliorer)
				Pattern rfc2822 = Pattern.compile(
				        "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
				);

				if (!rfc2822.matcher(user.getEmail()).matches()) {
					message = "L'adresse email renseignée est invalide";
					return null;
				}
				else{
					//Hashage du mot de passe
					try {
						user.setMdp(PasswordHash.createHash(user.getMdp()));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return (this.utilisateurDao.enregistrer(user));
				}
			}
		}	
	}
	
	public Utilisateur rechercherUtilisateur(String email){
		return this.utilisateurDao.rechercher(email);
	}
	
	public Utilisateur chercherUtilisateur(int idUtilisateur){
		return this.utilisateurDao.rechercher(idUtilisateur);
	}

	// modifie l'utilisateur avec l'id indiqué
	// si l'utilisateur n'est pas en DB, il l'ajoute
	@Override
	public Utilisateur modifierUtilisateur(Utilisateur user) {
		//On vérifie si le mdp est hashé ou pas
		if(!user.getMdp().startsWith("1000:")){
			//On hash le mdp avant de sauvegarder les modifications
			try {
				user.setMdp(PasswordHash.createHash(user.getMdp()));
			} catch (Exception e) {
				e.printStackTrace();
			}Pattern rfc2822 = Pattern.compile(
			        "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
			);

			if (!rfc2822.matcher(user.getEmail()).matches()) {
				message = "L'adresse email renseignée est invalide";
				return null;
			}
		}
		return this.utilisateurDao.mettreAJour(user);
	}

	// renvoi la liste de tous les utilisateurs
	@Override
	public List<Utilisateur> listerTousUtilisateurs() {
		return this.utilisateurDao.lister();
	}

	@Override
	public List<Utilisateur> listerTousProfesseur() {
		return this.utilisateurDao.listerProfesseur();
	}
	
	@Override
	public List<Utilisateur> listerTousEtudiant() {
		return this.utilisateurDao.listerEtudiant();
	}
	
	// supprime l'utilisateur selectionne
	@Override
	public void supprimerUtilisateur(int idUser) {
		Utilisateur utilisateur = this.utilisateurDao.rechercher(idUser);
		if(utilisateur!=null)
			this.utilisateurDao.supprimer(idUser);
	}

	// verifie si le mot de passe correspond a l'email. Dans ce cas, l'utilisateur est renvoy�, sinon il renvoi null;
	@Override
	public Utilisateur connecterUtilisateur(String email, String mdp) {
		
		Utilisateur user = this.utilisateurDao.rechercher(email);
		if(user!=null)
			try {
				if(PasswordHash.validatePassword(mdp, user.getMdp()))
					return user;
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}
		
		return null;
	}

	// change le mot de passe de l'utilisateur et lui envoi par mail
	@Override
	public boolean mdpOublie(String email) {
		String nouveauPassword = null;
		Utilisateur user = this.utilisateurDao.rechercher(email);
		if(user==null)
			return false;
		
		try {
			nouveauPassword = createRandom(10).toString();
			user.setMdp(PasswordHash.createHash(nouveauPassword));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(this.utilisateurDao.mettreAJour(user)==null)
			return false;
		
		// envoi du mail
		String titre = "Récupération de mail";
		String contenu = "Voici votre nouveau mot de passe : \""+ nouveauPassword +"\". Pour plus de sécurité, il vous est conseillé de changer de mot de passe à votre prochaine connexion.";
		List<Utilisateur> destinataires = new ArrayList<Utilisateur>();
		destinataires.add(user);
		
		try {
			this.gererMail.envoyerSync("evrardlaurent77@gmail.com", user.getEmail(), titre, contenu);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
		
	}

	// verifie si l'email n'est pas deja utilise 
	@Override
	public boolean existe(String email) {
		if(this.utilisateurDao.rechercher(email)==null)
			return false;
		
		return true;
	}
	
	final private static String CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDREFGHIJKLMNOPQRSTUVWXYZ";

	static public char[] createRandom(int length) {
		Random rand = new Random(System.currentTimeMillis());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i <= length; i++) {
			int pos = rand.nextInt(CHARS.length());
			sb.append(CHARS.charAt(pos));
		}
		return sb.toString().toCharArray();
	}
	
	public String getMessage(){
		return this.message;
	}

	@Override
	public Utilisateur chargerTout(Utilisateur user) {
		return this.utilisateurDao.chargerTout(user);
	}

}
