package usescasesImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererEvenement;
import usecases.GererHoraire;
import dao.HoraireDao;
import domaine.Annee;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Utilisateur;

@Stateless
public class GererHoraireImpl implements GererHoraire {

	@EJB
	private HoraireDao dao;

	@EJB
	private GererEvenement ucEvenement;

	private static String message = "";

	@Override
	public Horaire ajouterHoraire(Horaire horaire) {
		if (horaire == null)
			return null;

		Horaire complet = this.getHoraireComplet(horaire);
		if (complet == null)
			return null;

		complet.setStatut('A');
		if (chevauchementHoraire(complet)) {
			message = "Deux horaires ne peuvent pas se superposer pour une même classe";
			return null;
		}

		return this.dao.enregistrer(complet);
	}
	
	public List<Horaire> getHorairesAnnee(Annee annee){
		return(this.dao.getHorairesAnnee(annee));
	}

	private Horaire getHoraireComplet(Horaire partiel) {
		List<Evenement> complet = new ArrayList<Evenement>();
		List<Evenement> type = partiel.getEvenements();
		int i;
		boolean chevauche = false;
		for (Evenement evenement : type) {
			i = 0;
			GregorianCalendar debut = this.premierApresDebut(partiel.getDateDebut(), evenement.getHeureDebut());
			debut.set(Calendar.HOUR_OF_DAY, evenement.getHeureDebut().get(Calendar.HOUR_OF_DAY));
			debut.set(Calendar.MINUTE, evenement.getHeureDebut().get(Calendar.MINUTE));

			GregorianCalendar fin = this.premierApresDebut(partiel.getDateDebut(), evenement.getHeureDebut());
			fin.set(Calendar.HOUR_OF_DAY, evenement.getHeureFin().get(Calendar.HOUR_OF_DAY));
			fin.set(Calendar.MINUTE, evenement.getHeureFin().get(Calendar.MINUTE));
			while (partiel.getDateFin().after(debut) && chevauche == false) {
				Evenement event = new Evenement();

				event.setCours(evenement.getCours());
				event.setDescription(evenement.getDescription());
				event.setHeureDebut(debut);
				event.setHeureFin(fin);
				event.setHoraire(partiel);
				event.setIntitule(evenement.getIntitule());
				event.setLocaux(evenement.getLocaux());
				event.setStatut(evenement.getStatut());

				chevauche = ucEvenement.chevaucheTemps(partiel, event);
				complet.add(event);

				debut = this.premierApresDebut(partiel.getDateDebut(), evenement.getHeureDebut());
				debut.set(Calendar.HOUR_OF_DAY, evenement.getHeureDebut().get(Calendar.HOUR_OF_DAY));
				debut.set(Calendar.MINUTE, evenement.getHeureDebut().get(Calendar.MINUTE));

				fin = this.premierApresDebut(partiel.getDateDebut(), evenement.getHeureDebut());
				fin.set(Calendar.HOUR_OF_DAY, evenement.getHeureFin().get(Calendar.HOUR_OF_DAY));
				fin.set(Calendar.MINUTE, evenement.getHeureFin().get(Calendar.MINUTE));

				i++;
				debut.add(Calendar.DAY_OF_MONTH, i * 7);
				fin.add(Calendar.DAY_OF_MONTH, i * 7);
			}
		}

		if (chevauche) {
			message = "Deux évènements ne peuvent pas se chevaucher";
			return null;
		}

		partiel.setEvenements(complet);
		return (partiel);
	}

	public GregorianCalendar premierApresDebut(Calendar debut, Calendar jour) {
		GregorianCalendar sortie = new GregorianCalendar(debut.get(Calendar.YEAR), debut.get(Calendar.MONTH),
				debut.get(Calendar.DAY_OF_MONTH));

		switch (debut.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.TUESDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 6);
			break;
		case Calendar.WEDNESDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 5);
			break;
		case Calendar.THURSDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 4);
			break;
		case Calendar.FRIDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 3);
			break;
		case Calendar.SATURDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 2);
			break;
		case Calendar.SUNDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 1);
			break;
		}

		switch (jour.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.TUESDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 1);
			break;
		case Calendar.WEDNESDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 2);
			break;
		case Calendar.THURSDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 3);
			break;
		case Calendar.FRIDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 4);
			break;
		case Calendar.SATURDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 5);
			break;
		case Calendar.SUNDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 6);
			break;
		}
		return sortie;
	}

	public GregorianCalendar premierLundiApresDebut(Calendar debut) {
		GregorianCalendar sortie = new GregorianCalendar(debut.get(Calendar.YEAR), debut.get(Calendar.MONTH),
				debut.get(Calendar.DAY_OF_MONTH));

		switch (debut.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.TUESDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 6);
			break;
		case Calendar.WEDNESDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 5);
			break;
		case Calendar.THURSDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 4);
			break;
		case Calendar.FRIDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 3);
			break;
		case Calendar.SATURDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 2);
			break;
		case Calendar.SUNDAY:
			sortie.add(Calendar.DAY_OF_MONTH, 1);
			break;
		}

		return sortie;
	}

	@Override
	public Horaire modifierHoraire(Horaire horaire, boolean estComplet) {
		if (horaire == null)
			return null;
		if (chevauchementHoraire(horaire)) {
			message = "Deux horaires ne peuvent pas se supperposer pour une même classe";
			return null;
		}

		Horaire db = this.dao.rechercher(horaire.getIdHoraire());
		if (db == null)
			return null;
		
		db = this.supprimerEvenement(db);

		db.setAnnee(horaire.getAnnee());
		db.setDateDebut(horaire.getDateDebut());
		db.setDateFin(horaire.getDateFin());
		
		if(horaire.getStatut()=='\u0000'){
			if(estComplet)
				db.setStatut('E');
			else
				db.setStatut('M');
		}
		else
			db.setStatut(horaire.getStatut());
		
		if(!estComplet)
			horaire = this.getHoraireComplet(horaire);
		
		db.setEvenements(horaire.getEvenements());
		db.setIdCalendar(horaire.getIdCalendar());

		return this.dao.mettreAJour(db);
	}
	public void modifierStatutHoraire(char statut,Horaire horaire){
		Horaire db = this.dao.rechercher(horaire.getIdHoraire());
		
		db.setStatut(statut);
	}
	private Horaire supprimerEvenement(Horaire horaire) {
		horaire = this.rechercherHoraire(horaire);
		if (horaire == null)
			return null;

		horaire = this.chargerTout(horaire);
		if (horaire == null)
			return null;

		for (Evenement event : horaire.getEvenements()) {
			this.ucEvenement.supprimerEvenement(event);
		}
		horaire.setEvenements(null);
		return this.dao.mettreAJour(horaire);
	}

	@Override
	public void supprimerHoraire(Horaire horaire) {
		if (horaire == null)
			return;
		
		this.dao.supprimer(horaire.getIdHoraire());
	}
	
	@Override
	public void supprimerHoraireLogique(Horaire horaire) {
		if (horaire == null)
			return;
		
		Horaire db = this.dao.rechercher(horaire.getIdHoraire());
		if (db == null)
			return ;
		
		db.setStatut('S');
		this.dao.mettreAJour(db);
	}

	@Override
	public List<Horaire> listerHoraire() {
		return this.dao.lister();
	}

	@Override
	public Horaire getSemaineProf(Utilisateur utilisateur, Calendar dateDebut) {
		if (utilisateur == null)
			return null;
		if (dateDebut == null)
			return null;
		return this.dao.getSemaineProf(utilisateur, dateDebut);
	}

	@Override
	public Horaire getSemaineAnnee(Annee annee, Calendar dateDebut) {
		if (annee == null)
			return null;
		if (dateDebut == null)
			return null;
		return this.dao.getSemaineAnnee(annee, dateDebut);
	}

	@Override
	public Horaire rechercherHoraire(Horaire horaire) {
		if (horaire == null)
			return null;
		return this.dao.rechercher(horaire.getIdHoraire());
	}

	@Override
	public Horaire chargerTout(Horaire horaire) {
		if (horaire == null)
			return null;
		return this.dao.chargerTout(horaire);
	}

	/**
	 * verifie si un horaire n'en chevauche pas un autre, basé sur l'année
	 * 
	 * @param horaire
	 * @return true si chevauchement<br>
	 *         false si ok pour enreg
	 */
	private boolean chevauchementHoraire(Horaire horaire) {
		if (horaire == null)
			return true;

		List<Horaire> listeA = dao.listerHorairesActif();
		List<Horaire> liste = new ArrayList<Horaire>();

		// Permet de faire le tri pour n'obtenir que les horaires à ne pas
		// "chevaucher"
		for (Horaire h : listeA) {
			if (horaire.getIdHoraire() != h.getIdHoraire()) {
				if (h.getAnnee().equals(horaire.getAnnee())) {
					liste.add(h);
				}
			}
		}

		boolean chevauche = false;
		for (int i = 0; i < liste.size() && !chevauche; i++) {
			if (horaire.getDateDebut().before(liste.get(i).getDateFin()))
				if (horaire.getDateDebut().after(liste.get(i).getDateDebut()))
					chevauche = true;
			if (horaire.getDateFin().before(liste.get(i).getDateFin()))
				if (horaire.getDateFin().after(liste.get(i).getDateDebut()))
					chevauche = true;
		}

		return chevauche;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public List<Horaire> listerHorairesActifs() {
		return this.dao.listerHorairesActif();
	}
}
