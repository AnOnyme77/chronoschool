package usescasesImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererEquipement;
import dao.EquipementDao;
import domaine.Equipement;
@Stateless
public class GererEquipementImpl implements GererEquipement {
	
	@EJB
	private EquipementDao dao;

	@Override
	public boolean existe(Equipement equipement) {
		return(this.dao.existe(equipement.getIdEquipement()));
	}

	@Override
	public Equipement ajouterEquipement(Equipement equipement) {
		
		if(this.existe(equipement) || this.dao.rechercherIntitule(equipement.getIntitule())!=null)return null;
		
		return this.dao.enregistrer(equipement);
	}

	@Override
	public Equipement modifierEquipement(Equipement equipement) {
		Equipement db = this.dao.rechercher(equipement.getIdEquipement());
		if(db==null)return null;
		
		Equipement dbNom = this.dao.rechercherIntitule(equipement.getIntitule());
		if(dbNom!=null && dbNom.getIdEquipement()!=db.getIdEquipement())return null;
		
		db = this.dao.rechercher(equipement.getIdEquipement());
		db.setDescription(equipement.getDescription());
		db.setIntitule(equipement.getIntitule());
		db.setLocaux(equipement.getLocaux());
		
		
		return this.dao.mettreAJour(db);
	}

	@Override
	public Equipement supprimerEquipement(Equipement equipement) {
		if(!this.existe(equipement))return null;
		
		this.dao.supprimer(equipement.getIdEquipement());
		
		return equipement;
	}

	@Override
	public List<Equipement> listerEquipement() {
		return this.dao.lister();
	}

	@Override
	public Equipement rechercherEquipement(Equipement equipement) {
		
		return this.dao.rechercher(equipement.getIdEquipement());
	}

	@Override
	public Equipement rechercherIntitule(Equipement equipement) {
		return this.dao.rechercherIntitule(equipement.getIntitule());
	}

	@Override
	public Equipement chargerTout(Equipement equipement) {
		return this.dao.chargerTout(equipement.getIdEquipement());
	}

}
