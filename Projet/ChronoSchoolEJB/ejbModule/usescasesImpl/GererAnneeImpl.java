package usescasesImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererAnnee;
import usecases.GererCours;
import usecases.GererHoraire;
import usecases.GererUtilisateur;
import dao.AnneeDao;
import domaine.Annee;
import domaine.Cours;
import domaine.Horaire;
import domaine.Utilisateur;
@Stateless
public class GererAnneeImpl implements GererAnnee {

	@EJB
	private AnneeDao anneeDao;
	@EJB
	private GererCours coursImpl;
	@EJB
	private GererUtilisateur utilisateurImpl;
	@EJB
	private GererHoraire ucHoraire;
	
	@Override
	public List<Annee> listerAnnee() {
		return this.anneeDao.lister();
	}

	@Override
	public Annee rechercherAnnee(Integer annee) {
		return this.anneeDao.rechercher(annee);
	}

	@Override
	public Annee enregistrerAnnee(Annee annee) {
		boolean existeTous=true;
		
		List<Utilisateur> etudiant = annee.getEtudiants();
		if(etudiant != null)
		{			
			for (Utilisateur temp : etudiant) {
			if(!utilisateurImpl.existe(temp.getEmail()))
				existeTous=false;
			}
		
			if(existeTous==true)
			{
				annee.setEtudiants(null);
				annee = this.anneeDao.enregistrer(annee);
				for (Utilisateur temp : etudiant)
				{
					temp.setAnnee(annee);
					if(utilisateurImpl.modifierUtilisateur(temp)==null)
						return null;
				}
				return annee;
			}
			else
				return null;
		}
		else
			return (this.anneeDao.enregistrer(annee));
	}

	@Override
	public Annee mettreAJourAnnee(Annee annee) {

		boolean existeTous=true;
		
		List<Utilisateur> etudiant = annee.getEtudiants();
		List<Utilisateur> etudiantAncien = this.anneeDao.listerEtudiants(annee);
		
		if(etudiant != null)
		{
			for (Utilisateur temp : etudiant) {
				if(!utilisateurImpl.existe(temp.getEmail()))
					existeTous=false;
			}
			
			if(existeTous==true)
			{
				for(Utilisateur tempAncien : etudiantAncien){
					if(!etudiant.contains(tempAncien)){
						tempAncien.setAnnee(null);
						if(utilisateurImpl.modifierUtilisateur(tempAncien)==null)
							return null;
					}
				}
				
				for (Utilisateur temp : etudiant)
				{
					temp.setAnnee(annee);
					if(utilisateurImpl.modifierUtilisateur(temp)==null)
						return null;
				}
				return (this.anneeDao.mettreAJour(annee));
			}
			else
				return null;
		}
		else
			return (this.anneeDao.mettreAJour(annee));
	}

	@Override
	public void supprimerAnnee(Integer idAnnee) {
		Annee annee = this.anneeDao.rechercher(idAnnee);
		annee = this.anneeDao.chargerTout(annee);
		if(annee!=null)
		{
			List<Utilisateur> etudiant = annee.getEtudiants();
			
			if(etudiant != null)
			{
				for (Utilisateur temp : etudiant)
				{
					temp.setAnnee(null);
					utilisateurImpl.modifierUtilisateur(temp);
				}
			}
			
			List<Horaire> horaires = this.ucHoraire.getHorairesAnnee(annee);
			for(Horaire h:horaires){
				this.ucHoraire.supprimerHoraire(h);
			}
			
			this.anneeDao.supprimer(idAnnee);
			
			List<Cours> cours = this.coursImpl.listerCoursParAnnee(idAnnee);
			for (Cours temp : cours) {
				this.coursImpl.supprimerCours(temp);
			}
		}
	}

	@Override
	public void supprimerAnnee(Annee annee) {
		if(annee == null) return;
		this.anneeDao.supprimer(annee);
	}

	@Override
	public Annee ajouterEtudtiant(Annee annee, Utilisateur etudiant) {
		if(annee == null || etudiant == null) return null;
		return this.anneeDao.ajouterEtudiant(annee, etudiant);
	}

	@Override
	public Annee modifierEtudiant(Annee annee, Utilisateur etudiant) {
		if(annee == null || etudiant == null) return null;
		return this.anneeDao.modifierEtudiant(annee, etudiant);
	}

	@Override
	public void supprimerEtudiant(Annee annee, Utilisateur etudiant) {
		if(annee == null || etudiant == null) return;
		this.anneeDao.supprimerEtudiant(annee, etudiant);
	}

	@Override
	public List<Utilisateur> listerEtudiants(Annee annee) {
		if(annee == null) return null;
		return this.anneeDao.listerEtudiants(annee);
	}

	@Override
	public Annee chargerTout(Annee annee) {
		if(annee == null) return null;
		return this.anneeDao.chargerTout(annee);
	}
}

