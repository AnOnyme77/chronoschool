package usescasesImpl;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererAnnee;
import usecases.GererCalendar;
import usecases.GererEvenement;
import usecases.GererHoraire;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Event.Creator;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

import domaine.Annee;
import domaine.Evenement;
import domaine.Horaire;
import domaine.Local;
import domaine.Utilisateur;

@Stateless
public class GererCalendarImpl implements GererCalendar {
	@EJB
	private GererAnnee gererAnnee;
	@EJB
	private GererHoraire gererHoraire;
	@EJB
	private GererEvenement gererEvenement;

	private HttpTransport httpTransport;
	private JacksonFactory jsonFactory;
	private String scope = "https://www.googleapis.com/auth/calendar";
	private ArrayList<String> scopes = new ArrayList<String>();
	private String emailAddress = "705530083170-150opbslftn2ofpg4c0jecs4ptgh14sc@developer.gserviceaccount.com";
	// private String emailAddress = "chronoschoolplatform@gmail.com";
	private GoogleCredential credential;
	private static String applicationName = "ChronoSchool";

	private boolean isSetup = false;

	private String message = "";

	public void setUp() throws IOException {
		httpTransport = new NetHttpTransport();
		jsonFactory = new JacksonFactory();

		scopes.add(scope);

		credential = null;
		try {
			credential = new GoogleCredential.Builder()
					.setTransport(httpTransport)
					.setJsonFactory(jsonFactory)
					.setServiceAccountId(emailAddress)
					.setServiceAccountPrivateKeyFromP12File(
							new File(
									"../standalone/deployments/ChronoSchool.ear/ChronoSchoolEJB.jar/META-INF/ChronoSchool-3aff40882c1c.p12"))
					.setServiceAccountScopes(scopes).build();
			isSetup = true;
		} catch (GeneralSecurityException e) {
			message = "Une erreur est survenue lors de l'authentification Google Calendar";
		}

	}

	/**
	 * Convertit une liste d'événements ChronoSchool en événements
	 * GoogleCalendar
	 * 
	 * @param evenements
	 *            : la liste d'événements ChronoSchool
	 * @return une liste d'événements GoogleCalendar
	 */
	public List<Event> convertirEvenements(List<Evenement> evenements) {
		List<Event> events = new ArrayList<Event>();
		for (Evenement e : evenements) {
			e = gererEvenement.chargerTout(e);
			// Création d'un event
			Event event = new Event();

			// Attribution de l'intitulé
			event.setSummary(e.getIntitule() + " - " + e.getCours().getIntitule());
			// Attribution d'une description
			event.setDescription(e.getDescription() + " - " + e.getCours().getDescription());
			String location = "";
			// Si l'évènement possède des locaux, on les attribue en tant que
			// location
			if (e.getLocaux() != null) {
				for (Local l : e.getLocaux()) {
					location += l.getIntitule() + ", ";
				}
				event.setLocation(location);
			}

			// Attribution des dates de début et de fin
			Date startDate = e.getHeureDebut().getTime();
			DateTime start = new DateTime(startDate, TimeZone.getTimeZone("CET"));
			event.setStart(new EventDateTime().setDateTime(start));

			Date endDate = e.getHeureFin().getTime();
			DateTime end = new DateTime(endDate, TimeZone.getTimeZone("CET"));
			event.setEnd(new EventDateTime().setDateTime(end));

			Annee a = gererAnnee.chargerTout(e.getHoraire().getAnnee());
			List<EventAttendee> attendees = new ArrayList<EventAttendee>();
			// Attribution des utilisateurs
			for (Utilisateur u : a.getEtudiants()) {
				attendees.add(new EventAttendee().setEmail(u.getEmail()));
			}

			event.setAttendees(attendees);
			if (e.getStatut() == 'S')
				event.setStatus("cancelled");
			Creator c = new Creator();
			c.setEmail("chronoschool@gmail.com");
			c.setDisplayName("ChronoSchool");
			event.setCreator(c);

			events.add(event);
		}
		return events;
	}

	@Override
	public Horaire ajouterCalendar(Horaire horaire, boolean isComplet) {
		Horaire tmp = null;
		if (horaire == null)
			return null;

		if (!isSetup) {
			try {
				setUp();
			} catch (IOException e1) {
				message = "Une erreur est survenue lors de l'authentification Google Calendar";
				return null;
			}
		}

		// On charge toutes les informations de l'horaire
		horaire = gererHoraire.chargerTout(horaire);

		try {
			String id = creerCalendar(horaire);
			horaire.setIdCalendar(id);
			horaire.setStatut('O');
			if (!isComplet) {
				tmp = gererHoraire.getSemaineAnnee(horaire.getAnnee(), horaire.getDateDebut());
				System.out.println(tmp.getEvenements().size());

				horaire.setEvenements(tmp.getEvenements());
			}
			horaire = gererHoraire.modifierHoraire(horaire, isComplet);
			return horaire;
		} catch (IOException e1) {
			message = "Une erreur est survenue lors de la création du calendrier";
			return null;
		}
	}

	public String creerCalendar(Horaire horaire) throws IOException {
		Calendar service = new Calendar.Builder(httpTransport, jsonFactory, credential).setApplicationName(
				applicationName).build();
		com.google.api.services.calendar.model.Calendar c = new com.google.api.services.calendar.model.Calendar();
		c.setSummary(horaire.getAnnee().getNumClasse() + " " + horaire.getAnnee().getSection());
		c.setTimeZone("CET");

		com.google.api.services.calendar.model.Calendar createdCalendar = service.calendars().insert(c).execute();

		String id = createdCalendar.getId();

		List<Event> liste = convertirEvenements(horaire.getEvenements());
		for (Event e : liste) {
			service.events().insert(id, e).execute();
		}

		return id;
	}

	@Override
	public Horaire mettreAJourCalendar(Horaire horaire) {
		if (horaire == null)
			return null;

		if (!isSetup) {
			try {
				setUp();
			} catch (IOException e1) {
				message = "Une erreur est survenue lors de l'authentification Google Calendar";
				return null;
			}
		}

		// On charge toutes les informations de l'horaire
		horaire = gererHoraire.chargerTout(horaire);

		try {
			String id = patchCalendar(horaire);
			horaire.setIdCalendar(id);
			return gererHoraire.modifierHoraire(horaire, true);
		} catch (IOException e1) {
			message = "Une erreur est survenue lors de la modification du calendrier";
			return null;
		}
	}

	private String patchCalendar(Horaire horaire) throws IOException {
		String id = horaire.getIdCalendar();

		// Suppression de l'ancien calendar
		try {
			deleteCalendar(id);
			id = creerCalendar(horaire);
			return id;
		} catch (IOException e) {
			message = "Une erreur est survenue lors de la modification du calendrier";
			return null;
		}
	}

	@Override
	public boolean supprimerCalendar(Horaire horaire) {
		if (horaire == null)
			return false;

		if (!isSetup) {
			try {
				setUp();
			} catch (IOException e1) {
				message = "Une erreur est survenue lors de l'authentification Google Calendar";
				return false;
			}
		}
		String idCal = horaire.getIdCalendar();
		if (idCal == null)
			return false;
		try {
			deleteCalendar(idCal);
			return true;
		} catch (IOException e) {
			message = "Une erreur est survenue lors de la suppression du calendrier";
			return false;
		}
	}

	private void deleteCalendar(String id) throws IOException {
		Calendar service = new Calendar.Builder(httpTransport, jsonFactory, credential).setApplicationName(
				applicationName).build();
		String pageToken = null;
		// Suppression des events du calendrier
		do {
			Events events = service.events().list(id).setPageToken(pageToken).setShowDeleted(true).execute();
			List<Event> items = events.getItems();
			for (Event e : items) {
				service.events().delete(id, e.getId());
			}
		} while (pageToken != null);

		// Suppression du calendrier
		service.calendars().delete(id).execute();
	}

	@Override
	public void clearAll() throws IOException {
		if (!isSetup) {
			try {
				setUp();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		Calendar service = new Calendar.Builder(httpTransport, jsonFactory, credential).setApplicationName(
				applicationName).build();
		String pageToken = null;
		List<String> ids = new ArrayList<String>();

		do {
			CalendarList calendarList = service.calendarList().list().setPageToken(pageToken).setShowDeleted(true)
					.execute();
			List<CalendarListEntry> items = calendarList.getItems();

			for (CalendarListEntry calendarListEntry : items) {
				ids.add(calendarListEntry.getId());
				String pt = null;
				do {
					Events events = service.events().list(calendarListEntry.getId()).setPageToken(pt).execute();
					List<Event> itemsE = events.getItems();
					for (Event e : itemsE) {
						service.events().delete(calendarListEntry.getId(), e.getId()).execute();
					}
				} while (pt != null);
			}
			pageToken = calendarList.getNextPageToken();
		} while (pageToken != null);
	}

	@Override
	public String getMessage() {
		return message;
	}
}
