package usescasesImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererCoursProfesseur;
import dao.CoursProfesseurDao;
import domaine.CoursProfesseur;
@Stateless
public class GererCoursProfesseurImpl implements GererCoursProfesseur {

	private String message="";

	@EJB
	CoursProfesseurDao coursProfesseurDao;
	
	@Override
	public CoursProfesseur ajouterCoursProfesseur(CoursProfesseur coursProfesseur) {
		
		boolean valide=true;
		
		if(coursProfesseur.getCours()==null)
		{
			valide=false;
			message="Cours invalide";
		}
		
		if(coursProfesseur.getProfesseur()==null)
		{
			valide=false;
			message="professeur invalide";
		}
		
		if(valide)
		coursProfesseur = this.coursProfesseurDao.enregistrer(coursProfesseur);
		
		if(valide && coursProfesseur==null)
			message="echec lors de l'enregistrement";
		else
			message="ajout effectué";
		
		return coursProfesseur;
	}

	@Override
	public CoursProfesseur modifierCoursProfesseur(
			CoursProfesseur coursProfesseur) {
		
		boolean valide=true;
		
		if(coursProfesseur.getCours()==null)
		{
			valide=false;
			message="Cours invalide";
		}
		
		if(coursProfesseur.getProfesseur()==null)
		{
			valide=false;
			message="professeur invalide";
		}
		
		if(valide)
		coursProfesseur = this.coursProfesseurDao.mettreAJour(coursProfesseur);
		
		if(valide && coursProfesseur==null)
			message="echec lors de la modification";
		else
			message="Modification effectué";
		
		return coursProfesseur;
	}

	@Override
	public List<CoursProfesseur> listerTousCoursProfesseur() {
		return this.coursProfesseurDao.lister();
	}

	@Override
	public List<CoursProfesseur> listerCoursProfesseurParProfesseur(
			CoursProfesseur coursProfesseur) {
		List<CoursProfesseur> retour = null;
		if(coursProfesseur.getProfesseur()==null)
		{
			this.message="professeur invalide";
		}
		else
		{
			retour = this.coursProfesseurDao.listerParProfesseur(coursProfesseur.getProfesseur().getIdUtilisateur());
		}
		
		return retour;
	}

	@Override
	public List<CoursProfesseur> listerCoursProfesseurParCours(
			CoursProfesseur coursProfesseur) {
		
		List<CoursProfesseur> retour = null;
		if(coursProfesseur.getCours()==null)
		{
			this.message="Cours invalide";
		}
		else
		{
			retour = this.coursProfesseurDao.listerParCours(coursProfesseur.getCours().getIdCours());
		}
		
		return retour;
	}

	@Override
	public void supprimerCoursProfesseur(CoursProfesseur coursProfesseur) {
		if(coursProfesseur==null)
			this.message="Cours professeur invalide";
		else
		{
			this.message="Suppression effectuée avec succes";
			this.coursProfesseurDao.supprimerCP(coursProfesseur.getIdCoursProf());
		}
		
	}

	@Override
	public CoursProfesseur rechercherCoursProfesseur(
			CoursProfesseur coursProfesseur) {
		
		CoursProfesseur retour = null;
		if(coursProfesseur==null)
			this.message="Cours professeur invalide";
		else
		{
			this.message="Suppression effectuée avec succes";
			retour = this.coursProfesseurDao.rechercher(coursProfesseur.getIdCoursProf());
		}
		
		return retour;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	

}
