package usescasesImpl;

import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import usecases.GererMail;
import dao.EtatMailDao;
import dao.MailDao;
import domaine.EtatMail;
import domaine.Mail;
import domaine.Utilisateur;
@Stateless
public class GererMailImpl implements GererMail {
	
	@EJB
	EtatMailDao etatmail;
	
	@EJB
	MailDao mail;
	
	@Resource(lookup = "java:jboss/mail/Default")
    private Session mailSession;

	@Override
	public void envoyer(String titre, String contenu,
			List<Utilisateur> destinataires) {
		Mail mailA = new Mail();
		
		mailA.setTitre(titre);
		mailA.setDateAjout(new GregorianCalendar());
		mailA.setContenu(contenu);
		
		mailA = this.mail.enregistrer(mailA);
		
		EtatMail etat = null;
		for(Utilisateur utili : destinataires){
			etat = new EtatMail();
			etat.setDestinataire(utili);
			etat.setEmail(mailA);
			etat.setStatus('U');
			etat.setDateEnvoi(new GregorianCalendar());
			
			etatmail.enregistrer(etat);
			
		}
		
	}

	@Override
	public List<EtatMail> listerAEnvoyer() {
		List<EtatMail> mails = this.etatmail.listeAEnvoyer();
		for(EtatMail mail : mails){
			Mail email = mail.getEmail();
			email.setContenu(email.getContenu());
		}
		return(mails);
	}

	@Override
	public void marqueEnvoye(EtatMail email) {
		this.changerStatut(email, 'S'); 
	}

	@Override
	public void marqueErreur(EtatMail email) {
		this.changerStatut(email, 'E');
	}
	
	private void changerStatut(EtatMail mail, char statut){
		EtatMail db = this.etatmail.rechercher(mail.getIdEtatMail());
		if(db!=null){
			db.setStatus(statut);
			this.etatmail.mettreAJour(db);  
		}
	}

	@Override
	public List<EtatMail> listerDate() {
		List<EtatMail> mails =this.etatmail.listerDate();
		for(EtatMail mail : mails){
			Mail email = mail.getEmail();
			email.setContenu(email.getContenu());
		}
		return(mails);
	}
	
	public void envoyerSync(String from, String to, String titre, String contenu) throws MessagingException{
        MimeMessage m = new MimeMessage(mailSession);
        Address fromM = new InternetAddress(from);
        Address[] toM = new InternetAddress[] { new InternetAddress(
            to) };
        m.setFrom(fromM);
        m.setRecipients(Message.RecipientType.TO, toM);
        m.setSubject(titre);
        m.setContent(contenu, "text/plain");
        Transport.send(m);
	}

}
