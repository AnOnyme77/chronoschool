package usescasesImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import usecases.GererCours;
import usecases.GererDisponibilite;
import usecases.GererEvenement;
import usecases.GererLocal;
import domaine.Annee;
import domaine.Cours;
import domaine.CoursProfesseur;
import domaine.Equipement;
import domaine.Evenement;
import domaine.Local;
@Stateless
public class GererDisponibiliteImpl implements GererDisponibilite {

	@EJB
	private GererEvenement gererEvenement;
	
	@EJB
	private GererCours gererCours;
	
	@EJB
	private GererLocal gererLocal;
	
	@Override
	public boolean disponibiliteLocal(Evenement evenement) {
		if(evenement == null) return false;
		List<Local> listeLocal = evenement.getLocaux();
		boolean disponible=true;
		
		if(listeLocal!=null)
		{
			List<Evenement> listeEvenement = new ArrayList<Evenement>();
			for (Local local : listeLocal) {
				listeEvenement = this.gererEvenement.listerEvenementParLocal(local);
				if(listeEvenement!=null)
				{
					for (Evenement evenementDB : listeEvenement) {
						// on verifie si l'evenement a lieu en meme temps (horaire, jour, heure, minute)
						if(		evenementDB.getHoraire().equals(evenement.getHoraire())
								&& evenementDB.getHeureDebut().get(Calendar.DAY_OF_WEEK) == evenement.getHeureDebut().get(Calendar.DAY_OF_WEEK)
								&& verificationHeureEvenementChevauche(evenement, evenementDB))
							disponible=false;
					}
				}
			}
		}
		
		return disponible;
	}

	@Override
	public boolean disponibiliteAnnee(Evenement evenement) {
		if(evenement == null) return false;
		Annee annee = evenement.getHoraire().getAnnee();
		boolean disponible = true;
		
		if(annee!=null)
		{
			List<Evenement> listeEvenement = this.gererEvenement.listerEvenementParAnnee(annee);
			if(listeEvenement!=null)
			{
				for (Evenement evenementDB : listeEvenement) {
					// on verifie si l'evenement a lieu en meme temps (horaire, jour, heure, minute)
					if(		evenementDB.getHoraire().equals(evenement.getHoraire())
							&& evenementDB.getHeureDebut().get(Calendar.DAY_OF_WEEK) == evenement.getHeureDebut().get(Calendar.DAY_OF_WEEK)
							&& verificationHeureEvenementChevauche(evenement, evenementDB))
						disponible=false;
				}
			}
		}
		
		return disponible;
	}

	@Override
	public boolean disponibiliteProfesseur(Evenement evenement) {
		if(evenement == null) return false;
		if(evenement.getCours() == null) return true; // Si l'evenement n'est lié a aucun cours, il ne dépend pas de la présence ou non d'un professeur
		Cours cours = this.gererCours.rechercherCours(evenement.getCours());
		cours = this.gererCours.chargerTout(cours);
		List<CoursProfesseur> listeCoursProfesseur = cours.getProfesseurs();
		boolean disponible=true;
		
		if(listeCoursProfesseur!=null)
		{
			List<Evenement> listeEvenement = new ArrayList<Evenement>();
			for (CoursProfesseur coursProfesseur : listeCoursProfesseur) {
				listeEvenement = this.gererEvenement.listerEvenementParProfesseur(coursProfesseur.getProfesseur());
				if(listeEvenement!=null)
				{
					for (Evenement evenementDB : listeEvenement) {
						// on verifie si l'evenement a lieu en meme temps (horaire, jour, heure, minute)
						if(		evenementDB.getHoraire().equals(evenement.getHoraire())
								&& evenementDB.getHeureDebut().get(Calendar.DAY_OF_WEEK) == evenement.getHeureDebut().get(Calendar.DAY_OF_WEEK)
								&& verificationHeureEvenementChevauche(evenement, evenementDB)) 
							disponible=false;
					}
				}
			}
		}
		
		return disponible;
	}

	@Override
	public boolean disponibiliteEvenement(Evenement evenement) {
		if(evenement!=null && this.disponibiliteAnnee(evenement) && this.disponibiliteLocal(evenement) && disponibiliteProfesseur(evenement))
			return true;
		else
			return false;
	}

	// verifie que les equipement dans le cours sont disponible dans le local
	@Override
	public boolean equipementLocal(Local local, Cours cours) {
		boolean retour = true;
		
		cours = this.gererCours.chargerTout(cours);
		local = this.gererLocal.chargerTout(local);
		
		List<Equipement> listeEquipementCours = cours.getEquipements();
		List<Equipement> listeEquipementLocal = local.getEquipements();
		if(listeEquipementCours!=null)
		{
			if(listeEquipementLocal==null)
				retour=false;
			else
				for (Equipement equipement : listeEquipementCours) {
					if(!listeEquipementLocal.contains(equipement))
						retour=false;
				}
		}
		return retour;
	}
	
	private boolean verificationHeureEvenementChevauche(Evenement evenement,
			Evenement evenementDB) {
		
		if(evenement.getHeureDebut().get(Calendar.HOUR_OF_DAY)>evenementDB.getHeureFin().get(Calendar.HOUR_OF_DAY)) return false;
		
		if(evenement.getHeureDebut().get(Calendar.HOUR_OF_DAY)==evenementDB.getHeureFin().get(Calendar.HOUR_OF_DAY) 
				&& evenement.getHeureDebut().get(Calendar.MINUTE)>=evenementDB.getHeureFin().get(Calendar.MINUTE))return false;
		
		if(evenement.getHeureFin().get(Calendar.HOUR_OF_DAY)<evenementDB.getHeureDebut().get(Calendar.HOUR_OF_DAY)) return false;
		
		if(evenement.getHeureFin().get(Calendar.HOUR_OF_DAY)==evenementDB.getHeureDebut().get(Calendar.HOUR_OF_DAY) 
				&& evenement.getHeureFin().get(Calendar.MINUTE)<=evenementDB.getHeureDebut().get(Calendar.MINUTE))return false;
		
		return true;
	}
	
}

