package be.inertDevelopers.main;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;


public class LeCalendar {
	//notasecret
	public LeCalendar(){}

	public void setUp() throws IOException {
		HttpTransport httpTransport = new NetHttpTransport();
		JacksonFactory jsonFactory = new JacksonFactory();

		String scope = "https://www.googleapis.com/auth/calendar";
		ArrayList<String> scopes = new ArrayList<String>();
		scopes.add(scope);

		String emailAddress = "331220576530-4qgn3hk6q3d05qruqm4pmer8i4qv6s5l@developer.gserviceaccount.com";
		
		GoogleCredential credential = null;
		try {
			credential = new GoogleCredential.Builder()
				.setTransport(httpTransport)
				.setJsonFactory(jsonFactory)
				.setServiceAccountId(emailAddress)
				.setServiceAccountPrivateKeyFromP12File(new File("TestCalendar-6d039e431aeb.p12"))
				.setServiceAccountScopes(scopes)
				.build();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}

		// Create a new authorized API client
		Calendar service = new Calendar.Builder(httpTransport, jsonFactory,
				credential).build();
		
		Event event = new Event();

		event.setSummary("Appointment");
		event.setLocation("Somewhere");

		ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
		attendees.add(new EventAttendee().setEmail("carlysebastien@gmail.com"));
		attendees.add(new EventAttendee().setEmail("cipolla.fredo@gmail.com"));
		// ...
		event.setAttendees(attendees);

		Date startDate = new Date();
		Date endDate = new Date(startDate.getTime() + 3600000);
		DateTime start = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
		event.setStart(new EventDateTime().setDateTime(start));
		DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
		event.setEnd(new EventDateTime().setDateTime(end));
		
		Event createdEvent = service.events().insert("primary", event).execute();

		System.out.println(createdEvent.getId());
	}
}
