package useCases;

import java.util.List;

import javax.ejb.Remote;

import domaine.Email;
@Remote
public interface GestionEmail {
	public Email ajouterEmail(Email email);
	public List<Email> listerEmails();
	public List<Email> listerAEnvoyer();
	public void marqueEnvoye(Email email);
}
