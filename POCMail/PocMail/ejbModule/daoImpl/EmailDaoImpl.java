package daoImpl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import dao.DaoEmail;
import domaine.Email;

@Stateless
public class EmailDaoImpl extends DaoImpl<Integer, Email> implements DaoEmail {

	@Override
	public List<Email> listeAEnvoyer() {
		TypedQuery<Email> query=this.entityManager.createQuery("select e from Email e where e.status='U'",Email.class);
		return query.getResultList();
	}

	@Override
	public void marquerEnvoye(Email email) {
		Email db = this.rechercher(email.getIdMail());
		db.setStatus('S');
	}

	
}
