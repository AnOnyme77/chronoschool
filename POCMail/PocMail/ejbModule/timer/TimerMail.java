package timer;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import useCases.GestionEmail;
import domaine.Email;


@Singleton
public class TimerMail {
	
	@Resource(lookup = "java:jboss/mail/Default")
    private Session mailSession;

	@EJB
	private GestionEmail uc;
	
	// cette m�thode est appel�e toutes les minutes
    @Schedule(minute="*/1", hour="*")
    public void automaticTimeout1() {
    	
    	List<Email> liste = this.uc.listerAEnvoyer();
    	System.out.println("Il y a "+liste.size()+" email(s) a envoyer");
    	
    	for (Email email : liste) {
	        try {
	            MimeMessage m = new MimeMessage(mailSession);
	            Address from = new InternetAddress("bibhas@monkeyman.com");
	            Address[] to = new InternetAddress[] { new InternetAddress(
	                email.getDestinataire()) };
	            m.setFrom(from);
	            m.setRecipients(Message.RecipientType.TO, to);
	            m.setSubject("Google JavaMail Test");
	            m.setContent(email.getMessage(), "text/plain");
	            Transport.send(m);
	            this.uc.marqueEnvoye(email);
	            System.out.println("Mail sended");
	
	        } catch (javax.mail.MessagingException e) {
	            e.printStackTrace();
	        }
	    	
		}
    	
    	
    }

}