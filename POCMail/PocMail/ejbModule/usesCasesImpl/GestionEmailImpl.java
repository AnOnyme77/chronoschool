package usesCasesImpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import useCases.GestionEmail;
import dao.DaoEmail;
import domaine.Email;

@Stateless
public class GestionEmailImpl implements GestionEmail{
	
	@EJB
	private DaoEmail dao;

	@Override
	public Email ajouterEmail(Email email) {
		return this.dao.enregistrer(email);
	}

	@Override
	public List<Email> listerEmails() {
		return this.dao.lister();
	}

	@Override
	public List<Email> listerAEnvoyer() {
		return this.dao.listeAEnvoyer();
	}

	@Override
	public void marqueEnvoye(Email email) {
		this.dao.marquerEnvoye(email);
		
	}
	
}
