package dao;

import java.util.List;

import javax.ejb.Local;

import domaine.Email;

@Local
public interface DaoEmail extends Dao<Integer, Email>{
	List<Email> listeAEnvoyer();
	void marquerEnvoye(Email email);
}
